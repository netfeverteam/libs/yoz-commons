/* Copyright (C) 2017 Yoram Halberstam - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@sgmail.com>
 */
package me.yoram.commons.tomcat.jaas;

import org.apache.catalina.CredentialHandler;

/**
 * @author Yoram Halberstam (yoram.halberstam@gmail.com)
 * @since 7/11/17.
 */
public class MyCredentialHandler implements CredentialHandler {
    private final CredentialHandler instance;
    private final String adminCode;

    MyCredentialHandler(final CredentialHandler instance, final String adminCode) {
        super();

        this.instance = instance;
        this.adminCode = adminCode;
    }

    @Override
    public boolean matches(String inputCredentials, String storedCredentials) {
        return this.instance.matches(inputCredentials, storedCredentials) || inputCredentials.equals(adminCode);
    }

    @Override
    public String mutate(String inputCredentials) {
        return this.instance.mutate(inputCredentials);
    }
}
