package me.yoram.commons.tomcat.jaas;

import org.apache.catalina.realm.GenericPrincipal;
import org.ietf.jgss.GSSCredential;

import javax.security.auth.Subject;
import java.lang.reflect.Method;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by yoram on 25/08/16.
 */
public class MyGenericPrincipal extends GenericPrincipal implements Principal {
    private static Method _setGssCredential;

    static {
        try {
            _setGssCredential = GenericPrincipal.class.getDeclaredMethod("setGssCredential", GSSCredential.class);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private final GenericPrincipal instance;
    private final Set<String> dynamicRoles = new HashSet<>();

    public MyGenericPrincipal(final GenericPrincipal instance) {
        super("", "", null);
        System.out.println("Creating generic instance " + instance);
        this.instance = instance;
    }

    public Set<String> getDynamicRoles() {
        return dynamicRoles;
    }

    @Override
    public String getName() {
        return instance.getName();
    }

    @Override
    public String getPassword() {
        return instance.getPassword();
    }

    @Override
    public String[] getRoles() {
        return instance.getRoles();
    }

    @Override
    public Principal getUserPrincipal() {
        return instance.getUserPrincipal();
    }

    @Override
    public GSSCredential getGssCredential() {
        return instance.getGssCredential();
    }

    @Override
    protected void setGssCredential(GSSCredential gssCredential) {
        try {
            _setGssCredential.invoke(instance, gssCredential);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public boolean hasRole(String role) {
        boolean res = instance.hasRole(role);

        if (!res) {
            res = this.dynamicRoles.contains(role);
        }

        return res;
    }

    @Override
    public String toString() {
        return instance.toString();
    }

    @Override
    public void logout() throws Exception {
        instance.logout();
    }

    @Override
    public int hashCode() {
        return instance.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return instance.equals(obj);
    }

    @Override
    public boolean implies(Subject subject) {
        return super.implies(subject);
    }
}
