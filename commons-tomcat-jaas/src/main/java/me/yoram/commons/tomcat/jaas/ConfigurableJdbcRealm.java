package me.yoram.commons.tomcat.jaas;

import java.security.Principal;
import java.security.cert.X509Certificate;
import java.sql.*;
import java.util.ArrayList;

import org.apache.catalina.CredentialHandler;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.RequestFacade;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.core.StandardWrapper;
import org.apache.catalina.realm.DataSourceRealm;
import org.apache.catalina.realm.GenericPrincipal;
import org.apache.catalina.realm.LockOutRealm;
import org.ietf.jgss.GSSContext;
import org.ietf.jgss.GSSCredential;

public class ConfigurableJdbcRealm extends DataSourceRealm {
    private String roleSelectStatement = null;
    private String adminCodeCol = null;
    private String lastUserName = null;

    @Override
    public CredentialHandler getCredentialHandler() {
        String adminCode = null;

        if (adminCodeCol != null) {
            try {
                try (
                        Connection connection = open();
                        Statement stmt = connection.createStatement();
                        ResultSet rs = stmt.executeQuery(
                                String.format(
                                        "select %s from %s where %s='%s'",
                                        adminCodeCol,
                                        getUserTable(),
                                        getUserNameCol(),
                                        lastUserName))
                ) {
                    if (rs.next()) {
                        adminCode = rs.getString(1);
                    }
                }
            } catch (Throwable t) {
                t.printStackTrace();
                getContainer().getLogger().error(t.getMessage(), t);
            }
        }
        return new MyCredentialHandler(super.getCredentialHandler(), adminCode);
    }

    @Override
    protected Principal authenticate(Connection dbConnection, String username, String credentials) {
        lastUserName = username;
        Principal p = super.authenticate(dbConnection, username, credentials);

        if (p == null) {
            return null;
        }

        if (!(p instanceof MyGenericPrincipal)) {
            return new MyGenericPrincipal((GenericPrincipal)p);
        } else {
            return p;
        }
    }

    @Override
    protected Principal getPrincipal(String username) {
        Principal p = super.getPrincipal(username);

        if (p == null) {
            return null;
        }

        if (!(p instanceof MyGenericPrincipal)) {
            return new MyGenericPrincipal((GenericPrincipal)p);
        } else {
            return p;
        }
    }

    @Override
    protected Principal getPrincipal(String username, GSSCredential gssCredential) {
        Principal p = super.getPrincipal(username, gssCredential);

        if (p == null) {
            return null;
        }

        if (!(p instanceof MyGenericPrincipal)) {
            return new MyGenericPrincipal((GenericPrincipal)p);
        } else {
            return p;
        }
    }

    @Override
    public Principal authenticate(String username, String credentials) {
        Principal p = super.authenticate(username, credentials);

        if (p == null) {
            return null;
        }

        if (!(p instanceof MyGenericPrincipal)) {
            return new MyGenericPrincipal((GenericPrincipal)p);
        } else {
            return p;
        }
    }

    @Override
    public Principal authenticate(String username) {
        Principal p = super.authenticate(username);

        if (p == null) {
            return null;
        }

        if (!(p instanceof MyGenericPrincipal)) {
            return new MyGenericPrincipal((GenericPrincipal)p);
        } else {
            return p;
        }
    }

    @Override
    public Principal authenticate(X509Certificate[] certs) {
        Principal p = super.authenticate(certs);

        if (p == null) {
            return null;
        }

        if (!(p instanceof MyGenericPrincipal)) {
            return new MyGenericPrincipal((GenericPrincipal)p);
        } else {
            return p;
        }
    }

    @Override
    public Principal authenticate(String username, String clientDigest, String nonce, String nc, String cnonce, String qop, String realm, String md5a2) {
        Principal p = super.authenticate(username, clientDigest, nonce, nc, cnonce, qop, realm, md5a2);

        if (p == null) {
            return null;
        }

        if (!(p instanceof MyGenericPrincipal)) {
            return new MyGenericPrincipal((GenericPrincipal)p);
        } else {
            return p;
        }
    }

    @Override
    public Principal authenticate(GSSContext gssContext, boolean storeCred) {
        Principal p = super.authenticate(gssContext, storeCred);

        if (p == null) {
            return null;
        }

        if (!(p instanceof MyGenericPrincipal)) {
            return new MyGenericPrincipal((GenericPrincipal)p);
        } else {
            return p;
        }
    }

    @Override
    protected ArrayList<String> getRoles(Connection dbConnection,
            String username) {
        ArrayList<String> res = new ArrayList<>();
    
        try {
            PreparedStatement stmt = dbConnection.prepareStatement(roleSelectStatement);

            stmt.setString(1, username);
            
            ResultSet rs = stmt.executeQuery();
            
            try {
                while (rs.next()) {
                    res.add(rs.getString(getRoleNameCol()));
                }            
            } finally {
                rs.close();
            }
            
            return res;
        } catch (SQLException e) {
            containerLog.error(
                    sm.getString("dataSourceRealm.getRoles.exception", username), e);
            
            return null;
        }
    }

    public String getRoleSelectStatement() {
        return this.roleSelectStatement;
    }

    public void setRoleSelectStatement(String roleSelectStatement) {
        this.roleSelectStatement = roleSelectStatement;
    }

    public String getAdminCodeCol() {
        return adminCodeCol;
    }

    public void setAdminCodeCol(String adminCodeCol) {
        this.adminCodeCol = adminCodeCol;
    }
}
