/*
 * Copyright 2016 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2ee.jersey.models;

import java.util.Arrays;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 08/04/16
 */
public class JsonErrorObject extends AbstractJsonObject {
    public static class MessageDetail extends AbstractJsonObject {
        private String bundleBaseName;
        private String messageId;
        private String message;

        public String getBundleBaseName() {
            return bundleBaseName;
        }

        public void setBundleBaseName(final String bundleBaseName) {
            this.bundleBaseName = bundleBaseName;
        }

        public MessageDetail bundleBaseName(String bundleBaseName) {
            setBundleBaseName(bundleBaseName);

            return this;
        }

        public String getMessageId() {
            return messageId;
        }

        public void setMessageId(final String messageId) {
            this.messageId = messageId;
        }

        public MessageDetail messageId(final String messageId) {
            setMessageId(messageId);

            return this;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(final String message) {
            this.message = message;
        }

        public MessageDetail message(final String message) {
            setMessage(message);

            return this;
        }

        public MessageDetail copy() {
            return new MessageDetail().bundleBaseName(bundleBaseName).message(message).messageId(messageId);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof MessageDetail) {
                final MessageDetail o = (MessageDetail)obj;

                return super.equals(obj) &&
                        Objects.equals(bundleBaseName, o.bundleBaseName) &&
                        Objects.equals(message, o.message) &&
                        Objects.equals(messageId, o.messageId);
            }

            return false;
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(new Object[] {super.hashCode(), bundleBaseName, message, messageId});
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    public interface StandardMessageIds {
        String ERR_HEADER = "ERR.GLOBAL";

        String ERR_REQUEST_NULL = ERR_HEADER + ".ERR_REQUEST_NULL";
        String ERR_PROPERTY_EMPTY = ERR_HEADER + ".ERR_PROPERTY_EMPTY";
        String ERR_OPERATION_NOT_ALLOWED = ERR_HEADER + ".ERR_OPERATION_NOT_ALLOWED";
        String ERR_BUSINESS_ERROR = ERR_HEADER + ".ERR_BUSINESS_ERROR";
        String ERR_SYSTEM_ERROR = ERR_HEADER + ".ERR_SYSTEM_ERROR";
    }

    public interface StandardMessages {
        String GLOBAL_ERRORS_BUNDLE = "me.yoram.commons.messages.GlobalErrors";

        MessageDetail ERR_REQUEST_NULL  = new MessageDetail()
                .message("No request sent to server")
                .messageId(StandardMessageIds.ERR_REQUEST_NULL)
                .bundleBaseName(GLOBAL_ERRORS_BUNDLE);

        MessageDetail ERR_PROPERTY_EMPTY = new MessageDetail()
                .message("Property is required")
                .messageId(StandardMessageIds.ERR_PROPERTY_EMPTY)
                .bundleBaseName(GLOBAL_ERRORS_BUNDLE);

        MessageDetail ERR_OPERATION_NOT_ALLOWED = new MessageDetail()
                .message("Operation Not Allowed")
                .messageId(StandardMessageIds.ERR_OPERATION_NOT_ALLOWED)
                .bundleBaseName(GLOBAL_ERRORS_BUNDLE);

        MessageDetail ERR_SYSTEM_ERROR = new MessageDetail()
                .message("An error has occurred, we are working on the problem {0}.")
                .messageId(StandardMessageIds.ERR_SYSTEM_ERROR)
                .bundleBaseName(GLOBAL_ERRORS_BUNDLE);
    }

    private String language;
	private String propertyName;
	private MessageDetail detail;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(final String language) {
		this.language = language;
	}

	public JsonErrorObject language(final String language) {
		setLanguage(language);

		return this;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(final String propertyName) {
		this.propertyName = propertyName;
	}

	public JsonErrorObject propertyName(final String propertyName) {
		setPropertyName(propertyName);

		return this;
	}

    public MessageDetail getDetail() {
        return detail;
    }

    public void setDetail(final MessageDetail detail) {
        this.detail = detail;
    }

    public JsonErrorObject detail(final MessageDetail detail) {
        setDetail(detail);

        return this;
    }

    public JsonErrorObject copy() {
        return new JsonErrorObject()
                .language(language)
                .propertyName(propertyName)
                .detail(detail == null ? null : detail.copy());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JsonErrorObject) {
            final JsonErrorObject o = (JsonErrorObject)obj;

            return super.equals(obj) &&
                    Objects.equals(detail, o.detail) &&
                    Objects.equals(language, o.language) &&
                    Objects.equals(propertyName, o.propertyName);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] {super.hashCode(), detail, language, propertyName});
    }
}
