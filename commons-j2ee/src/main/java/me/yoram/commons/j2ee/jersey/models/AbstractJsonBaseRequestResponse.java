/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2ee.jersey.models;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 15/09/18
 */
public abstract class AbstractJsonBaseRequestResponse extends AbstractJsonObject {
    private String callerData;

    public String getCallerData() {
        return callerData;
    }

    public void setCallerData(final String callerData) {
        this.callerData = callerData;
    }

    public AbstractJsonBaseRequestResponse callerData(final String callerData) {
        setCallerData(callerData);

        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AbstractJsonBaseRequestResponse) {
            final AbstractJsonBaseRequestResponse o = (AbstractJsonBaseRequestResponse)obj;

            return super.equals(obj) && Objects.equals(callerData, o.callerData);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] {super.hashCode(), callerData});
    }
}
