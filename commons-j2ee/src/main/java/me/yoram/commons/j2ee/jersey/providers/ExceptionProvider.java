/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2ee.jersey.providers;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import me.yoram.commons.j2ee.jersey.exceptions.JsonException;
import me.yoram.commons.j2ee.jersey.exceptions.ThreatDetectedException;
import me.yoram.commons.j2ee.jersey.models.JsonErrorResponse;
import me.yoram.commons.j2ee.jersey.providers.jsonmodel.ProcessObjectEntryInterceptor;
import me.yoram.commons.j2ee.jersey.utils.JsonUtils;
import me.yoram.commons.j2se.exceptions.BusinessException;
import me.yoram.commons.j2se.utils.I18;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import me.yoram.commons.j2ee.jersey.models.JsonErrorObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 14/09/18
 */
@Provider
public class ExceptionProvider implements ExceptionMapper<Throwable> {
    private static final Logger LOG = LogManager.getLogger(ExceptionProvider.class);
    private static final Logger AUDIT_LOG = LogManager.getLogger("audit");

    private static final JsonErrorObject JSONERR_OPERATION_NOT_ALLOWED = new JsonErrorObject()
            .language("en")
            .detail(JsonErrorObject.StandardMessages.ERR_OPERATION_NOT_ALLOWED);

    private static final JsonErrorObject JSONERR_SYSTEM_ERROR = new JsonErrorObject()
            .language("en")
            .detail(JsonErrorObject.StandardMessages.ERR_SYSTEM_ERROR);

    @SuppressWarnings("unchecked")
    static <X> X findException(@Nullable Throwable bottomLevelException, @Nullable Class<X> clazz) {
        if (bottomLevelException != null && clazz != null) {
            Throwable current = bottomLevelException;

            do {
                if (clazz.isAssignableFrom(current.getClass())) {
                    return (X)current;
                }

                current = current.getCause();
            } while (current != null);
        }


    	return null;
    }

    //@Context
    //private ContainerRequestContext context;

    @Context
    ResourceContext resourceContext;

	@Override
	public Response toResponse(Throwable exception) {
	    final ContainerRequestContext context = resourceContext.getResource(ContainerRequestContext.class);

        Response.ResponseBuilder builder = null;

        final WebApplicationException wae = findException(exception, WebApplicationException.class);

        JsonErrorObject[] entity = null;

        if (wae instanceof NotAuthorizedException || wae instanceof ForbiddenException) {
            entity = new JsonErrorObject[] {JSONERR_OPERATION_NOT_ALLOWED.copy()};

            builder = Response.status(Response.Status.FORBIDDEN);
        }

        if (builder == null) {
            JsonException e = findException(exception, JsonException.class);

            if (e != null) {
                entity = e.getErrors();

                builder = Response.status(Response.Status.BAD_REQUEST);
            }
        }

        if (builder == null) {
            ThreatDetectedException e = findException(exception, ThreatDetectedException.class);

            if (e != null) {
                AUDIT_LOG.info(String.format("THREAT - %s with data %s" ,e.getMessage(), e.getInfo().toString()));

                entity = new JsonErrorObject[] {JSONERR_OPERATION_NOT_ALLOWED.copy()};

                builder = Response.status(Response.Status.FORBIDDEN);
            }
        }

        if (builder == null) {
            BusinessException e = findException(exception, BusinessException.class);

            if (e != null) {
                LOG.error(e.getMessage(), e);

                entity = new JsonErrorObject[] {
                        new JsonErrorObject().detail(
                                new JsonErrorObject.MessageDetail()
                                        .message(e.getUserMessage())
                                        .messageId(JsonErrorObject.StandardMessageIds.ERR_BUSINESS_ERROR))
                };

                builder = Response.status(Response.Status.BAD_REQUEST);
            }
        }

        if (builder == null) {
            entity = new JsonErrorObject[] {JSONERR_SYSTEM_ERROR.copy()};

            builder = Response.status(Response.Status.BAD_REQUEST);

            LOG.error(exception.getMessage(), exception);
        }

        final String locale = JsonUtils.readStringProperty(context, ProcessObjectEntryInterceptor.CTX_LOCALE);

        for (final JsonErrorObject o: entity) {
            final JsonErrorObject.MessageDetail detail = o.getDetail();

            if (detail != null) {
                detail.setMessage(
                        I18.getMessage(detail.getBundleBaseName(), detail.getMessageId(), detail.getMessage(), locale));

                o.getDetail().setBundleBaseName(null);
            }
        }

        return builder.header("X-POWERED-BY", "me.yoram").entity(new JsonErrorResponse().errors(entity)).build();
    }
}
