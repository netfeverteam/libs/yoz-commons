package me.yoram.commons.j2ee.smtp.impl;

import me.yoram.commons.j2ee.smtp.api.IEmailAddress;

public class DefaultEmailAddress implements IEmailAddress {
    private String name;
    private String email;
    public DefaultEmailAddress(final String email) {
        this(email ,email);        
    }
    
    public DefaultEmailAddress(final String name, final String email) {
        super();
        
        this.name = name;
        this.email = email;
    }
    
    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    @Override
    public void setEmail(final String email) {
        this.email = email;
    }

}
