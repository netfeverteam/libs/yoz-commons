/* Copyright (C) 2016 Yoram Halberstam - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@sgmail.com>
 */
package me.yoram.commons.j2ee.jersey.models;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author Yoram Halberstam (yoram.halberstam@gmail.com)
 * @since 01/01/16.
 */
public abstract class AbstractJsonObject{
    private transient Gson gson;

	@Override
	public String toString() {
	    if (gson == null) {
            gson= new Gson();
        }
		return gson.toJson(this);
	}

    @Override
    public boolean equals(Object obj) {
        return obj instanceof AbstractJsonObject;
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
