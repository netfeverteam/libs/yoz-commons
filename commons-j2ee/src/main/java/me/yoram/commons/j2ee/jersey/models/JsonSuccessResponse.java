/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2ee.jersey.models;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 16/09/18
 */
public class JsonSuccessResponse extends AbstractJsonBaseResponse {
    private static final Logger LOG = LogManager.getLogger(JsonSuccessResponse.class);

    public JsonSuccessResponse() {
        super();

        setStatus(EStatus.SUCCESS);
    }

    @Override
    public JsonSuccessResponse callerData(String callerData) {
        super.callerData(callerData);

        return this;
    }

    @Override
    public JsonSuccessResponse serverLogId(String serverLogId) {
        super.serverLogId(serverLogId);

        return this;
    }

    @Override
    public void setStatus(EStatus status) {
        if (status != EStatus.SUCCESS) {
            LOG.warn(
                    String.format(
                            "Status set to %s, it can only be EStatus.SUCCESS and this is what it will be set as",
                            status));
        }

        super.setStatus(EStatus.SUCCESS);
    }

    @Override
    public JsonSuccessResponse status(EStatus status) {
        super.status(status);

        return this;
    }
}
