package me.yoram.commons.j2ee.smtp.api;

import me.yoram.commons.j2se.exceptions.BusinessException;

public interface IEmailSender {
    String getSmtpHost();
    void setSmtpHost(String host);
    int getSmtpPort();
    void setSmtpPort(int port);
    String getJndiSession();
    void setJndiSession(String jndiSession);
    void send(IEmail email) throws BusinessException;
    boolean isInlineImage();
    boolean supportAttachments();
}
