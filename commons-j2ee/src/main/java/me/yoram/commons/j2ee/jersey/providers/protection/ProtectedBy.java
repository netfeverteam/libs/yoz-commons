package me.yoram.commons.j2ee.jersey.providers.protection;

import me.yoram.commons.j2ee.jersey.providers.protection.ProtectionApi;

import javax.ws.rs.NameBinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 21/05/18
 */
@NameBinding
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ProtectedBy {
    Class<? extends ProtectionApi>[] value();
}
