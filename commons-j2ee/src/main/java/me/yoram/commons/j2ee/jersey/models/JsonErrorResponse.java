/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2ee.jersey.models;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 16/09/18
 */
public class JsonErrorResponse extends AbstractJsonBaseResponse {
    private static final Logger LOG = LogManager.getLogger(JsonErrorResponse .class);

    private JsonErrorObject[] errors;

    public JsonErrorResponse() {
        super();

        setStatus(AbstractJsonBaseResponse.EStatus.ERROR);
    }

    @Override
    public JsonErrorResponse callerData(String callerData) {
        super.callerData(callerData);

        return this;
    }

    @Override
    public JsonErrorResponse serverLogId(String serverLogId) {
        super.serverLogId(serverLogId);

        return this;
    }

    @Override
    public void setStatus(AbstractJsonBaseResponse.EStatus status) {
        if (status != EStatus.ERROR) {
            LOG.warn(
                    String.format(
                            "Status set to %s, it can only be EStatus.ERROR and this is what it will be set as",
                            status));
        }

        super.setStatus(EStatus.ERROR);
    }

    @Override
    public JsonErrorResponse status(AbstractJsonBaseResponse.EStatus status) {
        super.status(status);

        return this;
    }

    public JsonErrorObject[] getErrors() {
        return errors;
    }

    public void setErrors(final JsonErrorObject[] errors) {
        this.errors = errors;
    }

    public JsonErrorResponse errors(final JsonErrorObject... errors) {
        setErrors(errors);

        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JsonErrorResponse) {
            final JsonErrorResponse o = (JsonErrorResponse)obj;

            return super.equals(obj) && Objects.equals(errors, o.errors);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] {super.hashCode(), this.errors});
    }
}
