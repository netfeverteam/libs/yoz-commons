/* Copyright (C) 2017 Yoram Halberstam - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@sgmail.com>
 */
package me.yoram.commons.j2ee.jersey.models;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author Yoram Halberstam (yoram.halberstam@gmail.com)
 * @since 15/05/16.
 */
public class JsonRequest extends AbstractJsonBaseRequestResponse {
    private String locale;

    @Override
    public JsonRequest callerData(final String callerData) {
        super.callerData(callerData);

        return this;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(final String locale) {
        this.locale = locale;
    }

    public JsonRequest locale(final String locale) {
        setLocale(locale);

        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JsonRequest) {
            final JsonRequest o = (JsonRequest)obj;

            return super.equals(obj) && Objects.equals(locale, o.locale);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] {super.hashCode(), locale});
    }
}
