package me.yoram.commons.j2ee.json;

public class FieldErrorMessage {
	private int code;
	private String defaultMessage;

	public FieldErrorMessage() {
		super();
	}

	public FieldErrorMessage(final int code, final String defaultMessage) {
		super();
		
		this.code = code;
		this.defaultMessage = defaultMessage;
	}

	public FieldErrorMessage(final String message) {
		super();
		
		int pos = message.indexOf('.');

		if (pos == -1) {
			throw new IllegalArgumentException("Cannot find 2 part error message in " + message);
		}
		this.code = Integer.parseInt(message.substring(0, pos).trim());
		this.defaultMessage = message.substring(pos + 1).trim();
	}
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDefaultMessage() {
		return defaultMessage;
	}

	public void setDefaultMessage(String defaultMessage) {
		this.defaultMessage = defaultMessage;
	}
	
	@Override
	public String toString() {
		return code + "." + defaultMessage;
	}
	
	@Override
	public boolean equals(Object obj) {
		return toString().equals(obj.toString());
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
}
