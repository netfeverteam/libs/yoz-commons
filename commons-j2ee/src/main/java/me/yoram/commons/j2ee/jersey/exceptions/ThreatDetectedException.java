package me.yoram.commons.j2ee.jersey.exceptions;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import me.yoram.commons.j2se.utils.KeyValuePair;
import me.yoram.commons.j2se.exceptions.BusinessException;

public class ThreatDetectedException extends BusinessException {
	private static final long serialVersionUID = 3218576493021957506L;
	
	private final Map<String, Object> info;

	public ThreatDetectedException(
			final String message, 
			final Object sender, 
			final Object request, 
			final HttpServletRequest servletRequest,
			KeyValuePair... kvps) {
		super(message);
		
		this.info = new HashMap<>();
		this.info.put("wt-class", sender.getClass().getName());
		info.put("wt-remote-ip", servletRequest.getRemoteAddr());
		
		if (servletRequest.getRemoteUser() != null) {
			this.info.put("wt-remote-user", servletRequest.getRemoteUser());
		}

		info.put("wt-request", request);

		if (kvps != null) {
			for (final KeyValuePair kvp: kvps) {
				this.info.put("o-" + kvp.getKey(), kvp.getValue());
			}
		}		
	}
	
	public Map<String, Object> getInfo() {
		return info;
	}
}
