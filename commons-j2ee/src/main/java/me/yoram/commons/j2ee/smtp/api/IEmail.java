package me.yoram.commons.j2ee.smtp.api;

import java.io.File;

public interface IEmail {
    String getReturnPath();
    void setReturnPath(String returnPath);
    
    IEmailAddress getFrom();
    void setFrom(IEmailAddress from);

    IEmailAddress getTo();
    void setTo(IEmailAddress to);

    IEmailAddress getCC();
    void setCC(IEmailAddress cc);
    
    String getSubject();
    void setSubject(String subject);

    String getText();
    void setText(String text);
    
    String getHtml();
    void setHtml(String html);
        
    File[] getAttachments();
    void addAttachment(File attachment);
    void removeAttachment(File attachment);
}
