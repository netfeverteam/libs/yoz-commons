/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2ee.jersey.providers.jsonmodel;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.util.Map;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 16/09/18
 */
@Provider
@PreMatching
public class ProcessObjectEntryRequestFilter implements ContainerRequestFilter {
    @Context
    private HttpServletRequest request;

    @Override
    public void filter(ContainerRequestContext context) {
        final Map<String, String[]> map = request.getParameterMap();

        for (final String prop: ProcessObjectEntryInterceptor.CTX_PROPS) {
            if (map.containsKey(prop)) {
                context.setProperty(prop, map.get(prop));
            }
        }
    }
}
