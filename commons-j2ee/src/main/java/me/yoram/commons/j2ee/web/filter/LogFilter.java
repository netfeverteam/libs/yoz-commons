package me.yoram.commons.j2ee.web.filter;

import java.io.IOException;
import java.security.SecureRandom;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.logging.log4j.ThreadContext;

public class LogFilter implements Filter {
    private static final SecureRandom RND = new SecureRandom();
    private static final String CTX_SERVER_LOG_ID = "serverLogId";

    public static String getServerLogId() {
        return ThreadContext.get(CTX_SERVER_LOG_ID);
    }

    @Override
    public void init(FilterConfig filterConfig) {
        // DO NOTHING
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        ThreadContext.put(
                CTX_SERVER_LOG_ID,
                Long
                        .toString(System.currentTimeMillis() + RND.nextLong(), 36)
                        .replace("-", "O")
                        .replace("0", "O"));

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // DO NOTHING
    }

}