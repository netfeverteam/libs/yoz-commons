package me.yoram.commons.j2ee.jersey.models;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by yoram on 15/05/16.
 */
public class AbstractJsonBaseResponse extends AbstractJsonBaseRequestResponse {
    public enum EStatus {
        SUCCESS,
        ERROR
    }

    private static final Logger LOG = LogManager.getLogger(AbstractJsonBaseResponse.class);

    private String serverLogId;
    private EStatus status = EStatus.SUCCESS;

    @Override
    public AbstractJsonBaseResponse callerData(final String callerData) {
        super.callerData(callerData);

        return this;
    }

    public String getServerLogId() {
        return serverLogId;
    }

    public void setServerLogId(final String serverLogId) {
        this.serverLogId = serverLogId;
    }

    public AbstractJsonBaseResponse serverLogId(final String serverLogId) {
        setServerLogId(serverLogId);

        return this;
    }

    public EStatus getStatus() {
        return status;
    }

    public void setStatus(final EStatus status) {
        if (status == null) {
            LOG.warn("status is set to null, this is not allowed, it will be set to EStatus.SUCCESS");

            this.status = EStatus.SUCCESS;
        } else {
            this.status = status;
        }
    }

    public AbstractJsonBaseResponse status(final EStatus status) {
        setStatus(status);

        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AbstractJsonBaseResponse) {
            final AbstractJsonBaseResponse o = (AbstractJsonBaseResponse)obj;

            return super.equals(o) && Objects.equals(serverLogId, o.serverLogId) && status == o.status;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] {super.hashCode(), serverLogId, status});
    }
}
