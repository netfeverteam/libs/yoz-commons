/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2ee.jersey.utils;

import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Nullable;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.ext.InterceptorContext;
import java.io.InputStream;
import java.io.Reader;
import java.util.Map;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 15/09/18
 */
public class JsonUtils {
    private static final Logger LOG = LogManager.getLogger(JsonUtils.class);

    public static Map getJsonMap(final Reader reader) {
        try {
            return new Gson().fromJson(reader, Map.class);
        } catch (Throwable t) {
            return null;
        }
    }

    @Nullable
    public static String readStringProperty(
            @Nullable final Object value, @Nullable final String name) {
        if (value instanceof String) {
            return (String)value;
        } else if (value instanceof String[]) {
            String[] array = (String[])value;

            if (array.length == 0) {
                LOG.warn(
                        String.format(
                                "Context property %s found with array zero length. As such, returning null.", name));

                return null;
            } else if (array.length > 1) {
                LOG.warn(
                        String.format(
                                "Context property %s found with array of size %d length. As such, returning the 1st one.",
                                name,
                                array.length));
            }

            return array[0];
        } else if (value != null) {
            LOG.warn(
                    String.format(
                            "Context property %s expected a string or string array but object of type %s returned. As such, returning toString().",
                            name,
                            value.getClass().getName()));

            return value.toString();
        }

        return null;
    }

    @Nullable
    public static String readStringProperty(
            @Nullable final ContainerRequestContext context, @Nullable final String name) {
        if (context == null || name == null) {
            return null;
        }

        return readStringProperty(context.getProperty(name), name);
    }

    @Nullable
    public static String readStringProperty(
            @Nullable final InterceptorContext context, @Nullable final String name) {
        if (context == null || name == null) {
            return null;
        }

        return readStringProperty(context.getProperty(name), name);
    }
}
