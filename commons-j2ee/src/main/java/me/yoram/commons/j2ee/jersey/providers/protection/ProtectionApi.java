package me.yoram.commons.j2ee.jersey.providers.protection;

import me.yoram.commons.j2se.exceptions.BusinessException;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 24/05/18
 */
public interface ProtectionApi {
    void access() throws BusinessException;

    default Class<?>[] needs() {
        return new Class[0];
    }
}
