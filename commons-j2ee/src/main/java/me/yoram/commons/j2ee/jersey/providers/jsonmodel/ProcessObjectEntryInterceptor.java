/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2ee.jersey.providers.jsonmodel;

import me.yoram.commons.j2ee.jersey.utils.JsonUtils;
import org.apache.commons.io.IOUtils;

import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;
import java.io.*;
import java.util.Map;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 15/09/18
 */
@Provider
@PreMatching
public class ProcessObjectEntryInterceptor implements ReaderInterceptor {
    public static final String CTX_CALLER_DATA = "callerData";
    public static final String CTX_LOCALE = "locale";
    public static final String[] CTX_PROPS = {CTX_CALLER_DATA, CTX_LOCALE};

    @Override
    public Object aroundReadFrom(final ReaderInterceptorContext context) throws IOException {
        final InputStream in = context.getInputStream();

        if (in != null) {
            final ByteArrayOutputStream out  = new ByteArrayOutputStream();
            IOUtils.copy(in, out);

            try {
                Map m = JsonUtils.getJsonMap(new InputStreamReader(new ByteArrayInputStream(out.toByteArray())));

                if (m != null) {
                    for (final String prop: CTX_PROPS) {
                        if (m.containsKey(prop)) {
                            context.setProperty(prop, m.get(prop));
                        }
                    }
                }
            } finally {
                context.setInputStream(new ByteArrayInputStream(out.toByteArray()));
            }
        }

        return context.proceed();
    }
}
