package me.yoram.commons.j2ee.smtp;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.activation.DataSource;
import javax.activation.URLDataSource;
import javax.mail.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import me.yoram.commons.j2ee.smtp.api.IEmailAddress;
import me.yoram.commons.j2se.exceptions.BusinessException;
import me.yoram.commons.j2se.exceptions.NotForUserException;
import org.apache.commons.mail.DataSourceResolver;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.ImageHtmlEmail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import me.yoram.commons.j2ee.smtp.api.IEmail;
import me.yoram.commons.j2ee.smtp.impl.AbstractEmailSender;

public class CommonsEmailSender extends AbstractEmailSender {
    private static final Logger LOG = LogManager.getLogger(CommonsEmailSender.class);
    public CommonsEmailSender(boolean inlineImage, boolean supportAttachments) {
        super(inlineImage, supportAttachments);
    }

    @Override
    public void send(final IEmail email) throws BusinessException {
        try {
           HtmlEmail htmlEmail;

            if (isInlineImage()) {
                htmlEmail = new ImageHtmlEmail();
            } else {
                htmlEmail = new HtmlEmail();

            }
            htmlEmail.addHeader("Return-Path", email.getReturnPath());
            htmlEmail.addReplyTo(email.getReturnPath(), email.getFrom()
                    .getName());
            htmlEmail.setFrom(email.getFrom().getEmail(), email.getFrom()
                    .getName());
            htmlEmail.addTo(email.getTo().getEmail(), email.getTo().getName());
            htmlEmail.setSubject(email.getSubject());
            htmlEmail.setHtmlMsg(email.getHtml());
            htmlEmail.setTextMsg(email.getText());

            IEmailAddress cc = email.getCC();
            if (cc != null) {
                htmlEmail.addCc(cc.getEmail(), cc.getName());
            }

            for (final File f : email.getAttachments()) {
                final EmailAttachment attachment = new EmailAttachment();
                attachment.setDisposition(EmailAttachment.ATTACHMENT);
                attachment.setPath(f.getAbsolutePath());
                attachment.setName(f.getName());
                htmlEmail.attach(attachment);
            }

            if (isInlineImage()) {
                ((ImageHtmlEmail) htmlEmail)
                        .setDataSourceResolver(new DataSourceResolver() {

                            @Override
                            public DataSource resolve(String resourceLocation,
                                    boolean isLenient) throws IOException {
                                return new URLDataSource(new URL(
                                        resourceLocation));
                            }

                            @Override
                            public DataSource resolve(String resourceLocation)
                                    throws IOException {
                                return new URLDataSource(new URL(
                                        resourceLocation));
                            }
                        });
            }

            if (getJndiSession() != null) {
                try {
                    Context initCtx = new InitialContext();
                    Session session = (Session) initCtx.lookup(getJndiSession());
                    htmlEmail.setMailSession(session);
                } catch (NamingException e) {
                    throw new NotForUserException("trying to get mail jndi " + getJndiSession() + " but error " + e.getMessage() + " with " + e.getExplanation() + " occured.");
                }                
            } else {
                htmlEmail.setHostName(getSmtpHost());
                htmlEmail.setSmtpPort(getSmtpPort());
            }

            htmlEmail.send();
        } catch (EmailException e) {
            throw new NotForUserException(e.getMessage(), e);
        }
    }
}
