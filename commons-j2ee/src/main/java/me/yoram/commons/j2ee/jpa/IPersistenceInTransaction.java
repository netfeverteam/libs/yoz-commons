package me.yoram.commons.j2ee.jpa;

import me.yoram.commons.j2se.exceptions.BusinessException;

import javax.persistence.EntityManager;

public interface IPersistenceInTransaction {
    Object onPersistance(PersistenceUtils utils, EntityManager em) throws BusinessException;
}
