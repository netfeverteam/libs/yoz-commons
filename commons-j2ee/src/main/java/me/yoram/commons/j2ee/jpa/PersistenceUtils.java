package me.yoram.commons.j2ee.jpa;

import me.yoram.commons.j2se.exceptions.BusinessException;
import me.yoram.commons.j2se.exceptions.NotForUserException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Map;

/**
 * Created by yoram on 15/05/16.
 */
public class PersistenceUtils implements IPersistenceUtils {
    private static final Logger LOG = LogManager.getLogger(PersistenceUtils.class);
    private static IPersistenceUtils instance; //= new PersistenceUtils("db", Config.getJpa().getProperties());

    public static IPersistenceUtils getInstance() {
        return instance;
    }

    public static void setInstance(IPersistenceUtils instance) {
        PersistenceUtils.instance = instance;
    }

    private final EntityManagerFactory factory;

    public PersistenceUtils(final String unitName) {
        this(unitName, null);
    }

    public PersistenceUtils(final String unitName, Map<String, String> jpaOverride) {
        super();

        if (jpaOverride == null) {
            this.factory = Persistence.createEntityManagerFactory(unitName);
        } else {
            this.factory = Persistence.createEntityManagerFactory(unitName, jpaOverride);
        }
    }

    @Override
    public synchronized EntityManager createEntityManager() {
        return factory.createEntityManager();
    }

    public void silentClose(EntityManager em) {
        try {
            em.close();
        } catch (Throwable t) {
            LOG.error(String.format("Error closing EntityManager: %s", t.getMessage()), t);
        }
    }

    @Override
    public Object doInTransaction(IPersistenceInTransaction event) throws BusinessException {
        EntityManager em = createEntityManager();

        try {
            EntityTransaction tx = em.getTransaction();
            tx.begin();

            try {
                Object o = event.onPersistance(this, em);
                tx.commit();
                return o;
            } catch (BusinessException e) {
                tx.rollback();
                throw new NotForUserException("Error in trasaction: " + e.getMessage(), e);
            } catch (Throwable t) {
                //tx.rollback();
                t.printStackTrace();
                throw new NotForUserException("Error in trasaction: " + t.getMessage(), t);
            }
        } finally {
            silentClose(em);
        }
    }
}
