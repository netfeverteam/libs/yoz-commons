/*
 * Copyright 2016 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2ee.jersey.exceptions;

import java.util.List;

import me.yoram.commons.j2ee.jersey.models.JsonErrorObject;
import me.yoram.commons.j2se.exceptions.BusinessException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 08/04/16
 */
public class JsonException extends BusinessException {
	private static final long serialVersionUID = 1463709564890776918L;

	private static final Logger LOG = LogManager.getLogger(JsonException.class);

	private JsonErrorObject[] errors;

	static String concatenateErrorMessages(final JsonErrorObject... errors) {
	    final StringBuilder sb = new StringBuilder();

	    if (errors != null) {
	        for (final JsonErrorObject o: errors) {
	            if (o.getDetail() != null && o.getDetail().getMessage() != null) {
	                if (sb.length() > 0) {
	                    sb.append(" | ");
                    }

                    sb.append(o.getDetail().getMessage());
                }
            }
        }

        return sb.toString();
    }

	public JsonException(JsonErrorObject... errors) {
		super(concatenateErrorMessages(errors));
		
		this.errors = errors;
	}

	public JsonErrorObject[] getErrors() {
		return this.errors;
	}

	public static void checkBoolean(final boolean bool, final JsonErrorObject error) throws JsonException {
		if (!bool) {
			throw new JsonException(error);
		}
	}	

	public static void checkBoolean(final boolean bool, final JsonErrorObject error, final List<JsonErrorObject> list) {
		if (!bool) {
			list.add(error);
		}
	}	
	
	public static void checkBooleans(final List<JsonErrorObject> list, final boolean[] bools, final JsonErrorObject[] errors) {
		for (int i = 0; i < bools.length; i++) {
			if (!bools[i]) {
				list.add(errors[i]);
				
				return;
				
			}
		}
	}
}
