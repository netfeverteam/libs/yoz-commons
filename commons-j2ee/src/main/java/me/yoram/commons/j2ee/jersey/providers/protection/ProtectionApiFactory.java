/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2ee.jersey.providers.protection;

import me.yoram.commons.j2ee.jersey.providers.protection.ProtectionApi;
import me.yoram.commons.j2se.exceptions.BusinessException;

import javax.ws.rs.core.Context;
import java.lang.reflect.Field;
import java.util.Collection;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 22/05/18
 */
public class ProtectionApiFactory {
    static ProtectionApi newInstance(final Class<?> clazz, final Collection<Object> context) throws BusinessException {
        try {
            final ProtectionApi res = (ProtectionApi)clazz.newInstance();

            for (final Field field: clazz.getDeclaredFields()) {
                if (field.getAnnotation(Context.class) != null) {
                    for (final Object c: context) {
                        if (field.getType().isAssignableFrom(c.getClass())) {
                            field.setAccessible(true);
                            field.set(res, c);
                        }
                    }
                }
            }

            return res;
        } catch (Throwable t) {
            throw new BusinessException(t.getMessage(), t);
        }
    }
}
