package me.yoram.commons.j2ee.jersey.providers.protection;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 29/05/18
 */
public class PathParamList {
    private final Map<String, String> map = new HashMap<>();

    public String getPathParam(String name) {
        return map.get(name);
    }

    void setPathParam(String name, String value) {
        map.put(name, value);
    }

    public int size() {
        return map.size();
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public String toString() {
        return map.toString();
    }

    @Override
    public int hashCode() {
        return map.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof PathParamList && Objects.equals(map, ((PathParamList) obj).map);
    }
}
