package me.yoram.commons.j2ee.jersey.providers.jsonmodel;

import me.yoram.commons.j2ee.jersey.models.AbstractJsonBaseResponse;
import me.yoram.commons.j2ee.jersey.utils.JsonUtils;
import me.yoram.commons.j2ee.web.filter.LogFilter;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.IOException;

/**
 * This writer ensure that the JSON object returning is properly decorated. For example, that the serverId (which is a
 * key in every lines in the log) is returned to the caller for methods that return {@link AbstractJsonBaseResponse}
 *
 * @author theyoz
 * @since 30/05/18
 */
@Provider
public class ProcessObjectExitInterceptor implements WriterInterceptor {
    @Override
    public void aroundWriteTo(final WriterInterceptorContext context) throws WebApplicationException, IOException {
        final Object entity = context.getEntity();

        if (entity instanceof AbstractJsonBaseResponse) {
            final AbstractJsonBaseResponse res = (AbstractJsonBaseResponse) context.getEntity();
            res.setServerLogId(LogFilter.getServerLogId());
            res.setCallerData(JsonUtils.readStringProperty(context, ProcessObjectEntryInterceptor.CTX_CALLER_DATA));
        }

        context.setEntity(entity);

        context.proceed();
    }
}
