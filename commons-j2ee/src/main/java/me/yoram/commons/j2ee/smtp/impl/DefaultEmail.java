package me.yoram.commons.j2ee.smtp.impl;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import me.yoram.commons.j2ee.smtp.api.IEmail;
import me.yoram.commons.j2ee.smtp.api.IEmailAddress;

public class DefaultEmail implements IEmail {
    private String returnPath;
    private IEmailAddress from = new DefaultEmailAddress("default@localhost");
    private IEmailAddress to = new DefaultEmailAddress("default@localhost");
    private IEmailAddress cc;
    private String subject;
    private String text;
    private String html;
    private final Set<File> attachments = new HashSet<File>();
    
    public DefaultEmail() {
        super();
    }
    
    @Override
    public String getReturnPath() {
        return this.returnPath == null ? getFrom().getEmail() : this.returnPath;
    }

    @Override
    public void setReturnPath(final String returnPath) {
        this.returnPath = returnPath;
    }

    @Override
    public IEmailAddress getFrom() {
        return this.from;
    }

    @Override
    public void setFrom(final IEmailAddress from) {
        this.from = from;
    }

    @Override
    public IEmailAddress getTo() {
        return this.to;
    }

    @Override
    public void setTo(final IEmailAddress to) {
        this.to = to;
    }

    @Override
    public IEmailAddress getCC() {
        return this.cc;
    }

    @Override
    public void setCC(final IEmailAddress cc) {
        this.cc = cc;
    }

    @Override
    public String getSubject() {
        return this.subject == null ? "" : this.subject;
    }

    @Override
    public void setSubject(final String subject) {
        this.subject = subject;
    }

    @Override
    public String getText() {
        return this.text == null ? "" : this.text;
    }

    @Override
    public void setText(final String text) {
        this.text = text;        
    }

    @Override
    public String getHtml() {
        return this.html == null ? "" : this.html;
    }

    @Override
    public void setHtml(final String html) {
        this.html = html;
    }

    @Override
    public File[] getAttachments() {
        return this.attachments.toArray(new File[0]);
    }

    @Override
    public void addAttachment(final File attachment) {
        this.attachments.add(attachment);
    }

    @Override
    public void removeAttachment(final File attachment) {
        this.attachments.remove(attachment);
    }
}
