package me.yoram.commons.j2ee.jersey.providers.protection;

import me.yoram.commons.j2se.exceptions.BusinessException;
import me.yoram.commons.j2se.utils.ClassUtils;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

/**
 * @author Yoram Halberstam (yoram.halberstam@gmail.com)
 * @since 4/4/17.
 */
@Provider
public class ProtectionFilter implements ContainerRequestFilter {
    @Context
    private ResourceInfo info;

    @Context
    private HttpServletRequest request;

    public ProtectionFilter(ResourceInfo info) {
        super();

        this.info = info;
    }

    public ProtectionFilter() {
        super();
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {
        if (ClassUtils.hasAnnotation(ProtectedBy.class, info.getResourceClass(), info.getResourceMethod())) {
            final boolean isGet = info != null && ClassUtils.hasAnnotation(GET.class, info.getResourceMethod());
            final boolean isDelete = info != null && ClassUtils.hasAnnotation(DELETE.class, info.getResourceMethod());
            if (isGet || isDelete) {
                /*
                final String token = request.getHeader(Constants.Http.TOKEN_HTTP_HEADER);
                final UserInfo user = token == null ? null : Config.getInstance().getGateKeeper().getUserInfo(token);

                if (user != null) {
                    ProtectionInterceptor.runProtection(
                            info,
                            request,
                            ProtectionInterceptor.createPathParamList(info, request),
                            user);
                } else {
                }*/

                try {
                    ProtectionDynamicFeature.runProtection(
                            info,
                            request,
                            ProtectionDynamicFeature.createPathParamList(info, request));
                } catch (Throwable t) {
                    throw new RuntimeException(t.getMessage(), t);
                }
            }
        }
    }
}