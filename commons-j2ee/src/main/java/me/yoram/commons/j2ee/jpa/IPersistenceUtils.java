package me.yoram.commons.j2ee.jpa;

import me.yoram.commons.j2se.exceptions.BusinessException;

import javax.persistence.EntityManager;

/**
 * Created by yoram on 15/05/16.
 */
public interface IPersistenceUtils {
    EntityManager createEntityManager();
    Object doInTransaction(IPersistenceInTransaction event) throws BusinessException;
}
