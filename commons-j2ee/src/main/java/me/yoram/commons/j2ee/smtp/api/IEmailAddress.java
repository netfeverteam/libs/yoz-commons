package me.yoram.commons.j2ee.smtp.api;

public interface IEmailAddress {
    String getName();
    void setName(String name);
    String getEmail();
    void setEmail(String email);
}
