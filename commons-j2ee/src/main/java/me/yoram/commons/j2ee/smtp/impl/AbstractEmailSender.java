package me.yoram.commons.j2ee.smtp.impl;

import me.yoram.commons.j2ee.smtp.api.IEmailSender;

public abstract class AbstractEmailSender implements IEmailSender {
    private String smtpHost = "127.0.0.1";
    private int smtpPort = 25;
    private String jndiSession;
    private final boolean inlineImage;
    private final boolean supportAttachments;

    protected AbstractEmailSender(final boolean inlineImage,
            final boolean supportAttachments) {
        super();

        this.inlineImage = inlineImage;
        this.supportAttachments = supportAttachments;
    }

    @Override
    public String getSmtpHost() {
        return this.smtpHost;
    }

    @Override
    public void setSmtpHost(final String smtpHost) {
        this.smtpHost = smtpHost;
    }

    @Override
    public int getSmtpPort() {
        return this.smtpPort;
    }

    @Override
    public void setSmtpPort(final int smtpPort) {
        this.smtpPort = smtpPort;
    }

    @Override
    public boolean isInlineImage() {
        return this.inlineImage;
    }

    @Override
    public boolean supportAttachments() {
        return this.supportAttachments;
    }

    @Override
    public String getJndiSession() {
        return this.jndiSession;
    }

    @Override
    public void setJndiSession(String jndiSession) {
        this.jndiSession = jndiSession;
    }
}
