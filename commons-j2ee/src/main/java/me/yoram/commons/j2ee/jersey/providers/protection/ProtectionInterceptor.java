package me.yoram.commons.j2ee.jersey.providers.protection;

import com.google.gson.Gson;
import me.yoram.commons.j2se.utils.ClassUtils;
import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;
import java.io.ByteArrayInputStream;
import java.lang.reflect.Parameter;
import java.util.*;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 21/05/18
 */
public class ProtectionInterceptor implements ReaderInterceptor {
    @Context
    private ResourceInfo info;

    @Context
    private HttpServletRequest request;

    @Override
    public Object aroundReadFrom(ReaderInterceptorContext context) throws WebApplicationException {
        try {
            if (ClassUtils.hasAnnotation(ProtectedBy.class, info.getResourceClass(), info.getResourceMethod())) {
                final String contentType = context.getHeaders().containsKey("Content-Type") ?
                        context.getHeaders().get("Content-Type").get(0) :
                        null;

                final Object param;

                if (contentType != null && contentType.contains("json")) {
                    final String payload = IOUtils.toString(context.getInputStream(), "UTF-8");
                    context.setInputStream(new ByteArrayInputStream(payload.getBytes("UTF-8")));

                    final Class<?> paramClazz = findPayloadClass();
                    param = paramClazz == null ? null : new Gson().fromJson(payload, paramClazz);
                } else {
                    param = null;
                }


                Set<Object> ctx = new HashSet<>();
                ctx.add(request);

                if (param != null) {
                    ctx.add(param);
                }

                /*
                String token = request.getHeader(Constants.Http.TOKEN_HTTP_HEADER);
                if (token != null) {
                    final UserInfo user = Config.getInstance().getGateKeeper().getUserInfo(token);
                    if (user != null) {
                        ctx.add(user);
                    }
                }
                */

                ctx.add(ProtectionDynamicFeature.createPathParamList(info, request));

                ProtectionDynamicFeature.runProtection(info, ctx.toArray(new Object[0]));
            }

            return context.proceed();
        } catch (WebApplicationException we) {
            throw we;
        } catch (Throwable t) {
            throw new WebApplicationException(t.getMessage(), t);
        }
    }

    private Class<?> findPayloadClass() {
        if (info.getResourceMethod() != null) {
            for (final Parameter p: info.getResourceMethod().getParameters()) {
                if (p.getAnnotations().length == 0) {
                    return p.getType();
                }
            }
        }

        return null;
    }
}
