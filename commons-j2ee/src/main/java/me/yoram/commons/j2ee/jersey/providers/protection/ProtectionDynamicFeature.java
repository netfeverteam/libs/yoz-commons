package me.yoram.commons.j2ee.jersey.providers.protection;

import me.yoram.commons.j2se.exceptions.BusinessException;
import me.yoram.commons.j2se.utils.ClassUtils;
import me.yoram.commons.j2se.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;
import java.util.*;

/**
 * {@link DynamicFeature} to add the {@link ProtectionFilter} for Jersey services with {@link ProtectedBy} annotations
 * that are either {@link GET} or {@link DELETE}.
 *
 * @author Yoram Halberstam (yoram.halberstam@gmail.com)
 * @since 04/04/17.
 */
@Provider
public class ProtectionDynamicFeature implements DynamicFeature {
    private static final Set<Class> unique = new HashSet<>();

    private static synchronized boolean canAdd(Class<?> c) {
        if (c == null || unique.contains(c)) {
            return false;
        }

        unique.add(c);

        return true;
    }

    public static void runProtection(ResourceInfo info, Object... contextObjects) throws BusinessException {
        final Collection<ProtectedBy> protections =
                ClassUtils.getAnnotations(ProtectedBy.class, info.getResourceClass(), info.getResourceMethod());

        if (!protections.isEmpty()) {
            final Set<Class<?>> allProtectorSet = new HashSet<>();
            for (final ProtectedBy p : protections) {
                allProtectorSet.addAll(Arrays.asList(p.value()));
            }
            final List<Class<?>> allProtectors = new ArrayList<>(allProtectorSet);

            Collection<Object> ctx = new ArrayList<>(Arrays.asList(contextObjects));
            ctx.add(info);

            Set<Class<?>> executedProtectors = new HashSet<>();
            for (final Class<?> clazz: allProtectors) {
                executeProtector(clazz, ctx, executedProtectors);
            }
        }
    }

    static void executeProtector(
            final Class<?> clazz,
            final Collection<Object> context,
            final Set<Class<?>> executedProtectors) throws BusinessException {
        final ProtectionApi protectionApi = ProtectionApiFactory.newInstance(clazz, context);

        final Class<?>[] needs = protectionApi.needs();
        if (needs != null && needs.length > 0) {
            for (final Class<?> c: protectionApi.needs()) {
                executeProtector(c, context, executedProtectors);
            }
        }

        if (!executedProtectors.contains(clazz)) {
            protectionApi.access();
            executedProtectors.add(clazz);
        }
    }

    /**
     * Get the full jersey (REST) path to the method as defined by the {@link Path} annotation. The path is the
     * annotation on the class + the annotation on the method
     *
     * @param info the {@link ResourceInfo} linked to the call
     * @return the full jersey (REST) path to the method
     */
    static String getJerseyPath(final ResourceInfo info) {
        final StringBuilder res = new StringBuilder();
        if (info != null) {
            if (info.getResourceClass() != null) {
                final Path p = info.getResourceClass().getAnnotation(Path.class);

                if (p != null) {
                    res.append(p.value());
                }
            }

            if (info.getResourceMethod() != null) {
                final Path p = info.getResourceMethod().getAnnotation(Path.class);

                if (p != null) {
                    res.append(p.value());
                }
            }
        }

        return res.toString();
    }

    /**
     * Take a jersey method (<code>info</code>) and url called and map the path parameter.
     *
     * For example:
     *   1. Jersey Path is /a/b/{ID}
     *   2. URI is /a/b/123
     *
     * In the case above, the return will have one path parameter named ID with value 123
     *
     * @param info info the {@link ResourceInfo} linked to the call
     * @param request the {@link HttpServletRequest} linked to the call
     * @return list of path parameter
     */
    public static PathParamList createPathParamList(final ResourceInfo info, final HttpServletRequest request) {
        final PathParamList res = new PathParamList();

        final String[] classPath = StringUtils.splitNoBlank(getJerseyPath(info), "/");
        final String[] url = StringUtils.splitNoBlank(
                request.getRequestURI().substring(request.getContextPath().length()), "/");

        int iClassPath = classPath.length - 1;
        int iUrl = url.length - 1;

        while (iClassPath > 0 && iUrl > 0) {
            if (classPath[iClassPath].startsWith("{") && classPath[iClassPath].endsWith("}")) {
                res.setPathParam(classPath[iClassPath].substring(1, classPath[iClassPath].length() - 1), url[iUrl]);
            }

            iClassPath--;
            iUrl--;
        }

        return res;
    }

    @Override
    public void configure(final ResourceInfo info, final FeatureContext context) {
        if (
                ClassUtils.hasAnnotation(ProtectedBy.class, info.getResourceClass(), info.getResourceMethod()) &&
                        canAdd(info.getResourceClass())) {
            final boolean isGet = ClassUtils.hasAnnotation(GET.class, info.getResourceMethod());
            final boolean isDelete = ClassUtils.hasAnnotation(DELETE.class, info.getResourceMethod());

            if (isGet || isDelete) {
                context.register(ProtectionFilter.class);
            } else {
                final boolean isPut = ClassUtils.hasAnnotation(PUT.class, info.getResourceMethod());
                final boolean isPost = ClassUtils.hasAnnotation(POST.class, info.getResourceMethod());

                if (isPut || isPost) {
                    context.register(ProtectionInterceptor.class);
                }
            }
        }
    }
}
