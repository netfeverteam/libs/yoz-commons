/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2ee.jersey.providers;

import me.yoram.commons.j2ee.jersey.exceptions.JsonException;
import me.yoram.commons.j2se.exceptions.BusinessException;
import org.testng.annotations.Test;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 21/09/18
 */
public class TestExceptionProvider {
    private static class E1 extends Exception {
        public E1(String message) {
            super(message);
        }

        public E1(String message, Throwable cause) {
            super(message, cause);
        }
    }

    private static class E2 extends Exception {
        public E2(String message) {
            super(message);
        }

        public E2(String message, Throwable cause) {
            super(message, cause);
        }
    }

    private static class E3 extends Exception {
        public E3(String message) {
            super(message);
        }

        public E3(String message, Throwable cause) {
            super(message, cause);
        }
    }

    private static void throwExceptions(Class<? extends Throwable>... throwables) throws Throwable {
        if (throwables.length == 1) {
            throw throwables[0].getConstructor(String.class).newInstance(throwables[0].getName());
        } else {
            try {
                Class<? extends Throwable>[] t = new Class[throwables.length - 1];
                System.arraycopy(throwables, 1, t, 0, t.length);
                throwExceptions(t);
            } catch (Throwable t) {
                throw throwables[0]
                        .getConstructor(String.class, Throwable.class)
                        .newInstance(throwables[0].getName(), t);
            }
        }
    }

    @Test
    public void testFindException() {
        try {
            throwExceptions(E1.class, E2.class, E3.class);

            throw new RuntimeException("Test failed, it should have thrown exceptions");
        } catch (Throwable t) {
            assert ExceptionProvider.findException(t, E1.class) != null : "Could not find E1";
            assert ExceptionProvider.findException(t, E2.class) != null : "Could not find E2";
            assert ExceptionProvider.findException(t, E3.class) != null : "Could not find E3";
        }
    }

    @Test
    public void testFindExceptionNull() {
        assert ExceptionProvider.findException(null, TestExceptionProvider.class) == null :
                "Should have returned null";

        assert ExceptionProvider.findException(new Throwable(), null) == null :
                "Should have returned null";
    }
}
