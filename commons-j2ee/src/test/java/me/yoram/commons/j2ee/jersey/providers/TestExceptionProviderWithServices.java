/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2ee.jersey.providers;

import com.google.gson.Gson;
import me.yoram.commons.j2ee.jersey.models.AbstractJsonBaseResponse;
import me.yoram.commons.j2ee.jersey.models.JsonErrorResponse;
import me.yoram.commons.j2ee.jersey.models.JsonRequest;
import me.yoram.commons.tests.jersey.AbstractJerseyTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.UUID;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 21/09/18
 */
public class TestExceptionProviderWithServices extends AbstractJerseyTest {
    @DataProvider
    public Object[][] dpErrorTesting() {
        return new Object[][] {
                {
                    "GET",
                        "/exceptions/system/get",
                        "An error has occurred, we are working on the problem.",
                        null,
                        null,
                        null
                },
                {
                    "GET",
                        "/exceptions/system/get",
                        "Une erreur est survenue, nous travaillons sur le problème",
                        null,
                        "fr",
                        null
                },
                {
                    "POST",
                        "/exceptions/system/post",
                        "An error has occurred, we are working on the problem.",
                        null,
                        null,
                        new JsonRequest()
                },
                {
                    "POST",
                        "/exceptions/system/post",
                        "Une erreur est survenue, nous travaillons sur le problème",
                        null,
                        null,
                        new JsonRequest().locale("fr")
                },
                {
                    "GET",
                        "/exceptions/business/get",
                        "An error has occurred, we are working on the problem.",
                        "error has occurred, we are working on the problem.",
                        null,
                        null
                },
                {
                    "GET",
                        "/exceptions/business/get",
                        "Une erreur est survenue, nous travaillons sur le problème",
                        " erreur est survenue, nous travaillons sur le problème",
                        "fr",
                        null
                },
                {
                    "POST",
                        "/exceptions/business/post",
                        "An error has occurred, we are working on the problem.",
                        "error has occurred, we are working on the problem.",
                        null,
                        new JsonRequest()
                },
                {
                    "POST",
                        "/exceptions/business/post",
                        "Une erreur est survenue, nous travaillons sur le problème",
                        " erreur est survenue, nous travaillons sur le problème",
                        null,
                        new JsonRequest().locale("fr")
                },
                {
                        "GET",
                        "/exceptions/operation_not_allowed/get",
                        "Operation Not Allowed",
                        null,
                        null,
                        null
                },
                {
                        "GET",
                        "/exceptions/operation_not_allowed/get",
                        "L'operation n'est pas permise",
                        null,
                        "fr",
                        null
                },
                {
                        "POST",
                        "/exceptions/operation_not_allowed/post",
                        "Operation Not Allowed",
                        null,
                        null,
                        new JsonRequest()
                },
                {
                        "POST",
                        "/exceptions/operation_not_allowed/post",
                        "L'operation n'est pas permise",
                        null,
                        null,
                        new JsonRequest().locale("fr")
                },
                {
                        "GET",
                        "/exceptions/request_not_null/get",
                        "No request sent to server",
                        null,
                        null,
                        null
                },
                {
                        "GET",
                        "/exceptions/request_not_null/get",
                        "Aucune requête n'a été envoyé au serveur",
                        null,
                        "fr",
                        null
                },
                {
                        "POST",
                        "/exceptions/request_not_null/post",
                        "No request sent to server",
                        null,
                        null,
                        new JsonRequest()
                },
                {
                        "POST",
                        "/exceptions/request_not_null/post",
                        "Aucune requête n'a été envoyé au serveur",
                        null,
                        null,
                        new JsonRequest().locale("fr")
                },
                {
                        "GET",
                        "/exceptions/threat/get",
                        "Operation Not Allowed",
                        null,
                        null,
                        null
                },
                {
                        "GET",
                        "/exceptions/threat/get",
                        "L'operation n'est pas permise",
                        null,
                        "fr",
                        null
                },
                {
                        "POST",
                        "/exceptions/threat/post",
                        "Operation Not Allowed",
                        null,
                        null,
                        new JsonRequest()
                },
                {
                        "POST",
                        "/exceptions/threat/post",
                        "L'operation n'est pas permise",
                        null,
                        null,
                        new JsonRequest().locale("fr")
                },







                {
                        "GET",
                        "/exceptions/property_empty/get",
                        "Property is required",
                        null,
                        null,
                        null
                },
                {
                        "GET",
                        "/exceptions/property_empty/get",
                        "Propriété obligatoire",
                        null,
                        "fr",
                        null
                },
                {
                        "POST",
                        "/exceptions/property_empty/post",
                        "Property is required",
                        null,
                        null,
                        new JsonRequest()
                },
                {
                        "POST",
                        "/exceptions/property_empty/post",
                        "Propriété obligatoire",
                        null,
                        null,
                        new JsonRequest().locale("fr")
                },
        };
    }

    @Test(dataProvider = "dpErrorTesting")
    public void testSystemP(
            String method,
            String url,
            String errorParam,
            String expectedError,
            String locale,
            Object entity) throws Exception {
        WebTarget target = target(url).queryParam("error", errorParam != null ? errorParam : expectedError);

        if (locale != null) {
            target = target.queryParam("locale", locale);
        }

        Map map = null;

        if ("GET".equals(method)) {
            Response rs = target.request().get();
            map = rs.readEntity(Map.class);
        } else {
            boolean error = false;

            try {
                target.request().post(Entity.entity(entity, MediaType.APPLICATION_JSON_TYPE), Map.class);
            } catch (ClientErrorException bre) {
                error = true;

                map = bre.getResponse().readEntity(Map.class);
            }

            if (!error) {
                throw new Exception("Should have failed");
            }
        }

        assert AbstractJsonBaseResponse.EStatus.ERROR.toString().equals(map.get("status")) :
                String.format("Return does not contain an error: %s", map);

        Gson g = new Gson();
        JsonErrorResponse res = g.fromJson(g.toJson(map), JsonErrorResponse.class);

        assert res.getErrors() != null && res.getErrors().length > 0 : "No error in the error object";

        assert AbstractJsonBaseResponse.EStatus.ERROR.toString().equals(map.get("status")) :
                String.format("Return does not contain an error: %s", map);

        assert res.getErrors()[0].getDetail() != null : "No details given for error";


        if (expectedError == null) {
            expectedError = errorParam;
        }

        assert expectedError != null && expectedError.equals(res.getErrors()[0].getDetail().getMessage()) :
                String.format(
                        "Message expected: '%s' but '%s' returned",
                        expectedError,
                        res.getErrors()[0].getDetail().getMessage());
    }

    /*
    @Test
    public void testSystem() {
        String error = "An error has occurred, we are working on the problem.";

        Response rs = target("/exceptions/system")
                .queryParam("error", error)
                .request()
                .get();
        Map map = rs.readEntity(Map.class);

        assert AbstractJsonBaseResponse.EStatus.ERROR.toString().equals(map.get("status")) :
                String.format("Return does not contain an error: %s", map);

        Gson g = new Gson();
        JsonErrorResponse res = g.fromJson(g.toJson(map), JsonErrorResponse.class);

        assert res.getErrors() != null && res.getErrors().length > 0 : "No error in the error object";

        assert AbstractJsonBaseResponse.EStatus.ERROR.toString().equals(map.get("status")) :
                String.format("Return does not contain an error: %s", map);

        assert res.getErrors()[0].getDetail() != null : "No details given for error";

        assert error.equals(res.getErrors()[0].getDetail().getMessage()) :
                String.format(
                        "Message expected: '%s' but '%s' returned", error, res.getErrors()[0].getDetail().getMessage());
    }

    @Test
    public void testSystemLocaleFr() {
        String error = "Aucune requete n'a été envoyé au serveur.";

        Response rs = target("/exceptions/system")
                .queryParam("error", error)
                .queryParam("locale", "fr")
                .request()
                .get();
        Map map = rs.readEntity(Map.class);

        assert AbstractJsonBaseResponse.EStatus.ERROR.toString().equals(map.get("status")) :
                String.format("Return does not contain an error: %s", map);

        Gson g = new Gson();
        JsonErrorResponse res = g.fromJson(g.toJson(map), JsonErrorResponse.class);

        assert res.getErrors() != null && res.getErrors().length > 0 : "No error in the error object";

        assert AbstractJsonBaseResponse.EStatus.ERROR.toString().equals(map.get("status")) :
                String.format("Return does not contain an error: %s", map);

        assert res.getErrors()[0].getDetail() != null : "No details given for error";

        assert error.equals(res.getErrors()[0].getDetail().getMessage()) :
                String.format(
                        "Message expected: '%s' but '%s' returned", error, res.getErrors()[0].getDetail().getMessage());
    }

    @Test
    public void test() {
        String error = String.format("This is an error %s", UUID.randomUUID().toString());

        Response rs = target("/exceptions/system")
                .queryParam("error", error)
                .request()
                .get();
        Map map = rs.readEntity(Map.class);

        assert AbstractJsonBaseResponse.EStatus.ERROR.toString().equals(map.get("status")) :
                String.format("Return does not contain an error: %s", map);

        Gson g = new Gson();
        JsonErrorResponse res = g.fromJson(g.toJson(map), JsonErrorResponse.class);

        assert res.getErrors() != null && res.getErrors().length > 0 : "No error in the error object";

        assert AbstractJsonBaseResponse.EStatus.ERROR.toString().equals(map.get("status")) :
                String.format("Return does not contain an error: %s", map);

        assert res.getErrors()[0].getDetail() != null : "No details given for error";

        assert error.equals(res.getErrors()[0].getDetail().getMessage()) :
                String.format(
                        "Message expected: '%s' but '%s'returned", error, res.getErrors()[0].getDetail().getMessage());
    }

    @Test
    public void testWithCallerData() {
        String error = String.format("This is an error %s", UUID.randomUUID().toString());

        Response rs = target("/exceptions/system")
                .queryParam("callerData", "123")
                .queryParam("error", error)
                .request()
                .get();
        Map map = rs.readEntity(Map.class);

        assert AbstractJsonBaseResponse.EStatus.ERROR.toString().equals(map.get("status")) :
                String.format("Return does not contain an error: %s", map);

        Gson g = new Gson();
        JsonErrorResponse res = g.fromJson(g.toJson(map), JsonErrorResponse.class);

        assert "123".equals(res.getCallerData()) :
                String.format("Expected caller data value: 123 but %s returned.", res.getCallerData());

        assert res.getErrors() != null && res.getErrors().length > 0 : "No error in the error object";

        assert AbstractJsonBaseResponse.EStatus.ERROR.toString().equals(map.get("status")) :
                String.format("Return does not contain an error: %s", map);

        assert res.getErrors()[0].getDetail() != null : "No details given for error";

        assert error.equals(res.getErrors()[0].getDetail().getMessage()) :
                String.format(
                        "Message expected: '%s' but '%s'returned", error, res.getErrors()[0].getDetail().getMessage());
    }
    */
}
