/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.tests.jersey.services;

import me.yoram.commons.j2ee.jersey.exceptions.JsonException;
import me.yoram.commons.j2ee.jersey.exceptions.ThreatDetectedException;
import me.yoram.commons.j2ee.jersey.models.JsonErrorObject;
import me.yoram.commons.j2ee.jersey.models.JsonRequest;
import me.yoram.commons.j2ee.jersey.providers.protection.ProtectedBy;
import me.yoram.commons.j2se.exceptions.BusinessException;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 21/09/18
 */
@Path("/exceptions")
public class ExceptionService {
    @Context
    private HttpServletRequest servletRequest;

    @Path("/system/get")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWithSystemException(@QueryParam("error") String error) throws Exception {
        throw new Exception(error);
    }

    @Path("/system/post")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response postWithSystemException(JsonRequest request, @QueryParam("error") String error) throws Exception {
        return getWithSystemException(error);
    }

    @Path("/business/get")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWithBusinessException(@QueryParam("error") String error) throws Exception {
        throw new BusinessException(error, error.substring(3));
    }

    @Path("/business/post")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response postWithBusinessException(JsonRequest request, @QueryParam("error") String error) throws Exception {
        return getWithBusinessException(error);
    }

    @Path("/operation_not_allowed/get")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOperationNotAllowed(@QueryParam("error") String error) {
        throw new ForbiddenException();
    }

    @Path("/operation_not_allowed/post")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response postOperationNotAllowed(JsonRequest request, @QueryParam("error") String error) {
        return getOperationNotAllowed(error);
    }

    @Path("/threat/get")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getThreat(@QueryParam("error") String error) throws BusinessException {
        throw new ThreatDetectedException("", "", null, servletRequest);
    }

    @Path("/threat/post")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response postgetThreat(JsonRequest request, @QueryParam("error") String error) throws BusinessException {
        return getThreat(error);
    }

    @Path("/request_not_null/get")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRequestNotNull(@QueryParam("error") String error) throws BusinessException {
        throw new JsonException(new JsonErrorObject().detail(JsonErrorObject.StandardMessages.ERR_REQUEST_NULL.copy()));
    }

    @Path("/request_not_null/post")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response postRequestNotNull(JsonRequest request, @QueryParam("error") String error) throws BusinessException {
        return getRequestNotNull(error);
    }

    @Path("/property_empty/get")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPropertyEmpty(@QueryParam("error") String error) throws BusinessException {
        throw new JsonException(new JsonErrorObject().detail(JsonErrorObject.StandardMessages.ERR_PROPERTY_EMPTY.copy()));
    }

    @Path("/property_empty/post")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response postPropertyEmpty(JsonRequest request, @QueryParam("error") String error) throws BusinessException {
        return getPropertyEmpty(error);
    }
}
