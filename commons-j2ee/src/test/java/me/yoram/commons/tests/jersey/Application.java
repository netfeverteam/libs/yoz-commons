/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.tests.jersey;

import me.yoram.commons.j2ee.jersey.providers.ExceptionProvider;
import me.yoram.commons.j2ee.jersey.providers.jsonmodel.ProcessObjectEntryInterceptor;
import me.yoram.commons.j2ee.jersey.providers.jsonmodel.ProcessObjectEntryRequestFilter;
import me.yoram.commons.j2ee.jersey.providers.jsonmodel.ProcessObjectExitInterceptor;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 21/09/18
 */
public class Application extends ResourceConfig {
    public Application() {
        super();

        packages("me.yoram.commons.tests.jersey.services");

        /*
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bindFactory(MyContainerRequestContext.MyContainerRequestContextFactory.class)
                        .to(ContainerRequestContext.class)
                        .in(RequestScoped.class);
            }
        });
        */

        register(ExceptionProvider.class);
        register(ProcessObjectEntryInterceptor.class);
        register(ProcessObjectEntryRequestFilter.class);
        register(ProcessObjectExitInterceptor.class);
    }
}
