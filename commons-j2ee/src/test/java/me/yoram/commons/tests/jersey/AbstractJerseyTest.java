package me.yoram.commons.tests.jersey;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.grizzly2.servlet.GrizzlyWebContainerFactory;
import org.glassfish.jersey.test.DeploymentContext;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.JerseyTestNg;
import org.glassfish.jersey.test.TestProperties;
import org.glassfish.jersey.test.spi.TestContainer;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.glassfish.jersey.test.spi.TestContainerFactory;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Application;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Yoram Halberstam (yoram.halberstam@gmail.com)
 * @since 05/06/2018.
 */
public class AbstractJerseyTest extends JerseyTestNg.ContainerPerClassTest {
    static {
        try {
            new TestSetup().setup();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    protected TestContainerFactory getTestContainerFactory() throws TestContainerException {
        return new TestContainerFactory() {
            @Override
            public TestContainer create(final URI baseUri, final DeploymentContext deploymentContext) throws IllegalArgumentException {
                return new TestContainer() {
                    private HttpServer server;

                    @Override
                    public ClientConfig getClientConfig() {
                        return null;
                    }

                    @Override
                    public URI getBaseUri() {
                        return baseUri;
                    }

                    @Override
                    public void start() {
                        try {
                            Map<String, String> map = new HashMap<>();
                            map.put(
                                    "jersey.config.server.provider.packages",
                                    "me.yoram.commons.tests.jersey.services");
                            map.put(
                                    "javax.ws.rs.Application",
                                    "me.yoram.commons.tests.jersey.Application");
                            this.server = GrizzlyWebContainerFactory.create(baseUri, map);
                        } catch (IOException e) {
                            throw new TestContainerException(e);
                        }
                    }

                    @Override
                    public void stop() {
                        this.server.stop();
                    }
                };

            }
        };
    }

    @Override
    protected Application configure() {
        forceEnable(TestProperties.LOG_TRAFFIC);
        forceEnable(TestProperties.DUMP_ENTITY);

        return TestSetup.newConfig();
    }

}