package me.yoram.commons.tests.jersey;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.ResourceConfig;
import org.testng.annotations.BeforeSuite;

import javax.ws.rs.container.ContainerRequestContext;

/**
 * @author Yoram Halberstam (yoram.halberstam@gmail.com)
 * @since 05/06/2018.
 */
public class TestSetup {
    private static final Logger LOG = LogManager.getLogger(TestSetup.class);

    private static ResourceConfig shareResourceConfig;
    private static boolean hasSetupRan = false;

    public static ResourceConfig getShareResourceConfig() {
        return shareResourceConfig;
    }

    public static void setShareResourceConfig(ResourceConfig shareResourceConfig) {
        TestSetup.shareResourceConfig = shareResourceConfig;
    }

    static ResourceConfig newConfig() {
        Application res = new Application();
        res.register(LoggingFeature.class);
        res.property("jersey.config.server.tracing.type", "ALL");
        res.property("jersey.config.server.tracing.threshold", "VERBOSE");
        res.property("jersey.config.feature.Trace", true);

        return res;
    }

    @BeforeSuite
    public synchronized void setup() throws Exception {
        if (hasSetupRan) {
            return;
        }

        System.out.println("BEFORE SUIT");

        System.setProperty("catalina.home", "/tmp");

        shareResourceConfig = newConfig();

        hasSetupRan = true;
    }
}
