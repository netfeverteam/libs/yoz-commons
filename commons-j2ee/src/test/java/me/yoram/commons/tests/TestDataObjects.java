/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.tests;

import me.yoram.commons.j2ee.jersey.models.*;
import me.yoram.commons.testing.RandomUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.beans.PropertyDescriptor;
import java.util.Objects;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 17/09/18
 */
public class TestDataObjects {
    @DataProvider
    public Object[][] dpClasses() {
        RandomUtils.Controller noStatus = new RandomUtils.Controller() {
            @Override
            public boolean includeProperty(String name) {
                return !name.equals("status");
            }
        };

        return new Object[][] {
                {JsonErrorObject.MessageDetail.class, null},
                {JsonErrorObject.class, null},
                {JsonErrorResponse.class, noStatus},
                {JsonRequest.class, null},
                {JsonSuccessResponse.class, noStatus}
        };
    }

    @Test(dataProvider = "dpClasses")
    public void testEquality(Class<?> clazz, RandomUtils.Controller controller) throws Exception {
        if (controller == null) {
            controller = new RandomUtils.Controller() {
                @Override
                public boolean includeProperty(String name) {
                    return true;
                }
            };
        }

        Object o1 = RandomUtils.randomize(clazz);
        assert o1 != null : "o1 is null";

        Object o2 = RandomUtils.randomize(clazz);
        assert o2 != null : "o2 is null";

        assert !Objects.equals(o1, o2) : "Objects should not be equal";
        assert o1.hashCode() != o2.hashCode() : "Objects hashcode should not be equal";

        Object o1Copy = clazz.newInstance();
        PropertyUtils.copyProperties(o1Copy, o1);

        assert Objects.equals(o1, o1Copy) : "Objects should be equal";
        assert o1.hashCode() == o1Copy.hashCode() : "Objects hashcode should be equal";

        PropertyDescriptor[] descriptors = PropertyUtils.getPropertyDescriptors(o1);
        for (PropertyDescriptor desc: descriptors) {
            if (desc.getWriteMethod() != null && controller.includeProperty(desc.getName())) {
                Object saved = PropertyUtils.getProperty(o1Copy, desc.getName());

                Object newValue;

                do {
                    newValue = RandomUtils.randomize(desc.getPropertyType());
                } while (Objects.equals(saved, newValue));

                PropertyUtils.setProperty(o1Copy, desc.getName(), newValue);

                assert !Objects.equals(o1, o1Copy) :
                        String.format(
                                "Objects should not be equal when %s changes from %s to %s",
                                desc.getName(),
                                saved,
                                newValue);
                assert o1.hashCode() != o1Copy.hashCode() : "Objects hashcode should not be equal";

                PropertyUtils.setProperty(o1Copy, desc.getName(), saved);

                assert Objects.equals(o1, o1Copy) : "Objects should be equal";
                assert o1.hashCode() == o1Copy.hashCode() : "Objects hashcode should be equal";
            }
        }
    }
}
