/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.testing;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 17/09/18
 */
public class TestRandomUtils {
    public static class Test1 {
        private String string;

        public String getString() {
            return string;
        }

        public void setString(String string) {
            this.string = string;
        }
    }
    @Test
    public void test() throws Exception {
        String[] o = RandomUtils.randomize(String[].class);

        for (String s: o) {
            System.out.println("ARRAY: " + s);
        }

        ArrayList<String> x = new ArrayList<>();

        Test1 o2 = RandomUtils.randomize(Test1.class);
        System.out.println("Test1.string: " + o2.string);
    }
}
