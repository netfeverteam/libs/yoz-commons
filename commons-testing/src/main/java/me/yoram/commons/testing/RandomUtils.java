/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.testing;

import org.apache.commons.beanutils.PropertyUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Array;
import java.security.SecureRandom;
import java.util.*;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 17/09/18
 */
public class RandomUtils {
    public interface Controller {
        default boolean includeProperty(String name) {
            return true;
        }
    }

    private static final Random RND = new SecureRandom();

    @SuppressWarnings("unchecked")
    public static <T> T[] arrayZero(Class<T> clazz) {
        T[] res = (T[]) Array.newInstance(clazz, 0);
        return res;
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> createList(Class<T> clazz) {
        return new ArrayList<T>();
    }

    @SuppressWarnings("unchecked")
    public static <T> T randomize(Class<T> clazz) throws Exception {
        if (clazz.isArray()) {
            List list = createList(clazz.getComponentType());

            final int len = RND.nextInt(10) + 1;
            for (int i = 0; i < len; i++) {
                list.add(randomize(clazz.getComponentType()));
            }

            return (T)list.toArray(arrayZero(clazz.getComponentType()));
        } else if (clazz.isEnum()) {
            return clazz.getEnumConstants()[RND.nextInt(clazz.getEnumConstants().length)];
        } else if (clazz == String.class) {
            return (T)UUID.randomUUID().toString();
        } else if (!clazz.isPrimitive()) {
            final PropertyDescriptor[] descriptors = PropertyUtils.getPropertyDescriptors(clazz);
            final T res = clazz.newInstance();

            for (final PropertyDescriptor desc: descriptors) {
                if (desc.getWriteMethod() != null) {
                    PropertyUtils.setProperty(res, desc.getName(), randomize(desc.getPropertyType()));
                }
            }

            return res;
        }

        throw new UnsupportedOperationException(String.format("%s not supported.", clazz.getName()));
    }
}
