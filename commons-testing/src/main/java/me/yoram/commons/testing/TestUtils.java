package me.yoram.commons.testing;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.security.SecureRandom;
import java.util.*;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 03/06/18
 */
public class TestUtils {
    private static final Random RANDOM = new SecureRandom();

    public interface Controller {
        default boolean includeProperty(String name) {
            return true;
        }
    }

    static Object randomizeSingle(PropertyDescriptor desc) throws Exception {
        final Class<?> type = desc.getPropertyType().isArray() ?
                desc.getPropertyType().getComponentType() :
                desc.getPropertyType();

        if (type.equals(String.class)) {
            return UUID.randomUUID().toString();
        } else if (type.equals(Integer.class) || desc.getPropertyType().equals(int.class)) {
            return RANDOM.nextInt();
        } else if (type.equals(Boolean.class) || desc.getPropertyType().equals(boolean.class)) {
            return RANDOM.nextInt() % 2 == 0;
        } else if (type.equals(Long.class) || desc.getPropertyType().equals(long.class)) {
            return RANDOM.nextLong();
        } else if (type.isEnum()) {
            return type.getEnumConstants()[RANDOM.nextInt(type.getEnumConstants().length)];
        } else if (!type.isPrimitive()) {
            return randomize(type, null);
        } else {
            throw new Exception("Unhandled class " + desc.getPropertyType().getName());
        }
    }

    static Object randomize(PropertyDescriptor desc) throws Exception {
        Object res;

        if (desc.getPropertyType().isArray()) {
            List subA = new ArrayList();

            for (int i = 0; i < 3; i++) {
                subA.add(randomizeSingle(desc));
            }

            res = subA.toArray((Object[])(Array.newInstance(desc.getPropertyType().getComponentType(), 0)));
        } else if (desc.getPropertyType().equals(Collection.class)) {
            Type genericType = ((ParameterizedType) desc
                    .getWriteMethod().getGenericParameterTypes()[0]).getActualTypeArguments()[0];

            List subA = new ArrayList();

            for (int i = 0; i < 3; i++) {
                subA.add(randomize((Class<?>) genericType, null));
            }

            res = subA;
        } else {
            res = randomizeSingle(desc);
        }

        return res;
    }

    public static <T> T randomize(final Class<T> clazz, final Controller controller) throws Exception {
        try {
            if (clazz == null) {
                return null;
            }

            final Object o;

            o = clazz.newInstance();

            final PropertyDescriptor[] descs = PropertyUtils.getPropertyDescriptors(o.getClass());

            for (final PropertyDescriptor desc : descs) {
                boolean go = desc.getReadMethod() != null && desc.getWriteMethod() != null;

                if (go && controller != null && !controller.includeProperty(desc.getName())) {
                    go = false;
                }

                if (go) {
                    BeanUtils.setProperty(o, desc.getName(), randomize(desc));
                }
            }

            return (T)o;
        } catch (Throwable t) {
            throw new Exception(t.getMessage(), t);
        }
    }

    public static String getRandomEmail() {
        return String.format("%s@test.com", RANDOM.nextInt() + "." + System.currentTimeMillis());
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] toTypedArray(Collection col, Class<T> o) {
        T[] res = (T[])Array.newInstance(o, col.size());

        int ndx = 0;
        for (Object o2: col) {
            res[ndx++] = (T)o2;
        }

        return res;
    }}
