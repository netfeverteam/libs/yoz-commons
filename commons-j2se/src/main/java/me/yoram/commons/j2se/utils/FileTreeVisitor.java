package me.yoram.commons.j2se.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by yoram on 30/05/16.
 */
public class FileTreeVisitor extends SimpleFileVisitor<Path> {
    private final Set<File> fileTree = new TreeSet<>();

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        fileTree.add(file.toFile());
        return FileVisitResult.CONTINUE;
    }

    public Set<File> getFileTree() {
        return this.fileTree;
    }
}
