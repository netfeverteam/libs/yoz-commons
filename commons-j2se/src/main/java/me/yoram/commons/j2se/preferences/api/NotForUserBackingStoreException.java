package me.yoram.commons.j2se.preferences.api;

import me.yoram.commons.j2se.exceptions.IBusinessException;
import me.yoram.commons.j2se.exceptions.NotForUserException;

import java.util.prefs.BackingStoreException;

/**
 * Created by yoram on 30/03/16.
 */
public class NotForUserBackingStoreException extends BackingStoreException implements IBusinessException {
    private static final long serialVersionUID = 5480835037131442364L;

    public NotForUserBackingStoreException(String s) {
        super(s);
    }

    public NotForUserBackingStoreException(Throwable cause) {
        super(cause);
    }

    /**
     * @return the user message. If this has not bee set via a relevant
     *         constructor, this is the same as {@link #getMessage()}.
     */
    @Override
    public String getUserMessage() {
        return NotForUserException.SYSTEM_ERROR;
    }
}
