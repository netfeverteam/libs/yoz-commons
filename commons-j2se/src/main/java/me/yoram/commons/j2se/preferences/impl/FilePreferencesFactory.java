package me.yoram.commons.j2se.preferences.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

import me.yoram.commons.j2se.exceptions.NotForUserException;
import me.yoram.commons.j2se.preferences.api.NotForUserBackingStoreException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import me.yoram.commons.j2se.preferences.api.WtPreferencesFactory;
import me.yoram.commons.j2se.preferences.api.PreferencesUtils;

/**
 * Implement a {@link PreferencesFactory} base on Java Preference factory files.
 * Java preferences DTD is described in
 * http://docs.oracle.com/javase/6/docs/api/java/util/prefs/Preferences.html.
 * <br><br>
 * This class allow default configuration and environment specific.
 * To understand how the files are loaded (see {@link PreferencesUtils#getFilesPath()}.
 * 
 * @author Yoram Halberstam
 */
public final class FilePreferencesFactory extends WtPreferencesFactory {
    static {
        System.out.println("launched FilePreferencesFactory ");
    }
    private static final Logger LOG = LogManager.getLogger(FilePreferencesFactory.class);

    /**
     * Set this Java System Property to set the Java Preferences root path
     */
    public static final String ENV_PROP_PREFS_PATH = "file.preferences.path";

    @Override
    protected void populate() throws NotForUserBackingStoreException {
        final File root = PreferencesUtils.getFilesPath();
        LOG.info("Preferences root directory: " + root);

        final List<File> files = new ArrayList<>();
        PreferencesUtils.getPreferencesFiles(root, files);

        InputStream in = null;

        for (final File f : files) {
            try {
                in = new FileInputStream(f);
            } catch (IOException e) {
                LOG.error("Could not open " + f.getAbsolutePath(), e);

                continue;
            }

            try {
                try {
                    Preferences.importPreferences(in);
                } catch (InvalidPreferencesFormatException e) {
                    LOG.error(
                            "Invalid preferences file format at "
                                    + f.getAbsolutePath(), e);
                } catch (IOException e) {
                    LOG.error(
                            "Error reading preferences file at "
                                    + f.getAbsolutePath(), e);
                }

                LOG.info("Preferences file " + f.getAbsolutePath() + " loaded.");
            } finally {
                try {
                    in.close();
                } catch (Exception e) {
                    LOG.error("Could not close " + f.getAbsolutePath(), e);
                }
            }
        }
    }
}
