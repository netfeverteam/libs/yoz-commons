package me.yoram.commons.j2se.utils.hostname;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class LinuxHostnameProvider implements IHostnameProvider {
    private static final Logger LOG = LogManager.getLogger(LinuxHostnameProvider.class);
    @Override
    public String getHostname() {
        try {
            Process p = Runtime.getRuntime().exec("hostname");
            InputStream in = p.getInputStream();
            String s = IOUtils.toString(in).replace("\n", "");
            
            if (s != null) {
                LOG.info("hostname command on linux returned " + s);                
            }
            
            return s;
        } catch (IOException e) {
            LOG.warn(String.format("Error getting host name with hostname command under linux: %s", e.getMessage()), e);
            return null;
        }
    }
}
