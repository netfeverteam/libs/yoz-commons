package me.yoram.commons.j2se.utils;

public class ThreadUtils {
	public static void sleep(long ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// ignore exception
		}
	}
}
