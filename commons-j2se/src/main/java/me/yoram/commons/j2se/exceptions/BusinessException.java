package me.yoram.commons.j2se.exceptions;

/**
 * This exception support dual message, for logging (system) and user.
 * 
 * @author Yoram Halberstam
 */
public class BusinessException extends Exception implements IBusinessException {
    private static final long serialVersionUID = 5669525252949333394L;

    private final String userMessage;

    /**
     * Initialize the exception with a message for both the system and the user.
     * 
     * @param message
     *            for both the system and the user.
     */
    public BusinessException(String message) {
        this(message, message);
    }

    /**
     * Initialize the exception with a message for the system and a different
     * one for the user.
     * 
     * @param message
     *            system message.
     * @param userMessage
     *            user message
     */
    public BusinessException(String message, String userMessage) {
        super(message);

        this.userMessage = userMessage;
    }

    /**
     * Initialize the exception with a message for both the system and the user
     * and wrap an cause exception.
     * 
     * @param message
     *            for both the system and the user.
     * @param cause
     *            the cause exception
     */
    public BusinessException(String message, Throwable cause) {
        this(message, message, cause);
    }

    /**
     * Initialize the exception with a message for the system and a different
     * one for the user + add a cause.
     * 
     * @param message
     *            system message.
     * @param userMessage
     *            user message
     * @param cause
     *            the cause exception
     */
    public BusinessException(String message, String userMessage, Throwable cause) {
        super(message, cause);

        this.userMessage = userMessage;
    }

    /**
     * @return the user message. If this has not bee set via a relevant
     *         constructor, this is the same as {@link #getMessage()}.
     */
    @Override
    public String getUserMessage() {
        return userMessage;
    }
}
