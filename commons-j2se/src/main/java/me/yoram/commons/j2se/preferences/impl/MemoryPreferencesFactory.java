package me.yoram.commons.j2se.preferences.impl;

import java.util.prefs.BackingStoreException;
import java.util.prefs.PreferencesFactory;

import me.yoram.commons.j2se.preferences.api.NotForUserBackingStoreException;
import me.yoram.commons.j2se.preferences.api.WtPreferencesFactory;

/**
 * This class implements a in-memory version of {@link PreferencesFactory}. It
 * has no backing store and starts empty.
 * 
 * @author Yoram Halberstam
 */
public class MemoryPreferencesFactory extends WtPreferencesFactory {
    static {
        System.out.println("launched MemoryPreferencesFactory ");
    }
    
    @Override
    protected void populate() throws NotForUserBackingStoreException {
        // not need when Preferences created in memory
    }
}
