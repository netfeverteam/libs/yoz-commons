package me.yoram.commons.j2se.io;

import java.lang.reflect.Field;
import java.util.Map;

import me.yoram.commons.j2se.exceptions.BusinessException;

/**
 * System utilities methods. All methods must throw {@link BusinessException}
 * 
 * @author Yoram Halberstam
 *
 */
public final class SystemUtilities {
    private static Map<String, String> environment = null;
    
    private SystemUtilities() {
    }
   
    private static Map<String, String> getEnvironment() throws BusinessException {
        if (environment == null) {
            Class<?> processEnvironmentClass = ClassUtilities.forName("java.lang.ProcessEnvironment");

            Field theUnmodifiableEnvironmentField = ClassUtilities.getDeclaredField(processEnvironmentClass, "theUnmodifiableEnvironment");
            theUnmodifiableEnvironmentField.setAccessible(true);
            Object cienv = ClassUtilities.getField(theUnmodifiableEnvironmentField, null);
            
            Field mField = ClassUtilities.getDeclaredField(cienv.getClass(), "m");
            mField.setAccessible(true);
            @SuppressWarnings("unchecked")
            Map<String, String> res = (Map<String, String>)ClassUtilities.getField(mField, cienv);
            environment = res;
        }
        
        return environment;
    }
        
    /**
     * * Set a new OS environment variable
     * @param key the OS key
     * @param value the value
     * @throws BusinessException if an error occurs
     */
    public static void setEnvironmentVariable(String key, String value) throws BusinessException {
        getEnvironment().put(key, value);                                                                                
    }
}
