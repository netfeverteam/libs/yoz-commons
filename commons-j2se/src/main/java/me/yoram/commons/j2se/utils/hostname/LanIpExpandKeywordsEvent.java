package me.yoram.commons.j2se.utils.hostname;

public class LanIpExpandKeywordsEvent implements IExpandKeywordsEvent {

	@Override
	public String onKeyword(final String key, final String value) {
		if (key.equals("LAN-IP")) {
			return Environment.getIp(value);
		} else {
			return "127.0.0.1";
		}
	}

}
