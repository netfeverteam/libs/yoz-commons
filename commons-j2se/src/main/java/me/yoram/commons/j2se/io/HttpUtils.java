package me.yoram.commons.j2se.io;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import java.util.Map.Entry;

import me.yoram.commons.j2se.exceptions.NotForUserException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

public final class HttpUtils {
	private static final String SYSPROP_VERIFYSSL = "me.yoram.commons.j2se.io.HttpUtils.verifySsl";
	private static boolean verifySsl;

	static {
		verifySsl = System.getProperty(SYSPROP_VERIFYSSL, "true").equalsIgnoreCase("true");
	}

	public static final class HttpResult {
		private int status;
		private byte[] data;

		public int getStatus() {
			return status;
		}

		public void setStatus(final int status) {
			this.status = status;
		}

		public byte[] getData() {
			return data;
		}

		public void setData(final byte[] data) {
			this.data = data;
		}
	}

	private HttpUtils() {
	}

	private static CloseableHttpResponse httpClientExecute(
			final CloseableHttpClient client,
			final HttpUriRequest request,
			final HttpContext context) throws NotForUserException {
		try {
			if (context == null) {
				return client.execute(request);
			} else {
				return client.execute(request, context);
			}
		} catch (ClientProtocolException e) {
			throw new NotForUserException("error with http protocol", e);
		} catch (IOException e) {
			throw new NotForUserException("I/O Exception", e);
		}
	}

	private static InputStream httpEntityGetContent(final HttpEntity entity) throws NotForUserException {
		try {
			return entity.getContent();
		} catch (IOException e) {
			throw new NotForUserException("I/O Exception", e);
		} catch (UnsupportedOperationException e) {
			throw new NotForUserException("The entry cannot be represented as a java.io.InputStream", e);
		}
	}

	private static SSLConnectionSocketFactory createSSLConnectionSocketFactory(
			final SSLContextBuilder builder,
			final HostnameVerifier verifier) throws NotForUserException {
		try {
			if (verifier == null) {
				return new SSLConnectionSocketFactory(builder.build());
			} else {
				return new SSLConnectionSocketFactory(builder.build(), verifier);
			}
		} catch (KeyManagementException e) {
			throw new NotForUserException("KeyManagementException", e);
		} catch (NoSuchAlgorithmException e) {
			throw new NotForUserException("NoSuchAlgorithmException", e);
		}
	}

	private static CloseableHttpClient createCloseableHttpClient() throws NotForUserException {
		if (!verifySsl) {
			final SSLContextBuilder builder = new SSLContextBuilder();

			try {
				builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			} catch (KeyStoreException e) {
				throw new NotForUserException("KeyStoreException", e);
			} catch (NoSuchAlgorithmException e) {
				throw new NotForUserException("NoSuchAlgorithmException", e);
			}

			final SSLConnectionSocketFactory sslsf = createSSLConnectionSocketFactory(builder, new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			});

			return HttpClients.custom().setSSLSocketFactory(sslsf).build();
		} else {
			return HttpClients.createDefault();
		}
	}

	public static Cookie[] getCookies(final String cookieString) {
		Set<Cookie> res = new HashSet<>();

		final String[] parts = cookieString.split(";");

		for (final String part : parts) {
			final String[] splits = part.split("=");

			if (splits.length == 1) {
				res.add(new BasicClientCookie(splits[0].trim(), ""));
			} else {
				res.add(new BasicClientCookie(splits[0].trim(), part.substring(splits[0].length() + 1).trim()));
			}
		}

		return res.toArray(new Cookie[0]);
	}

	public static URL getUrl(final String url) throws NotForUserException {
		try {
			return new URL(url);
		} catch (MalformedURLException e) {
			throw new NotForUserException(
					"if no protocol is specified, or an unknown protocol is found, or spec is null.",
					e);
		}
	}

	public static byte[] post(final String uri, final byte[] data, final String[][] headers, String cookieString)
			throws NotForUserException {
		final Map<String, String> mapHeaders = new HashMap<>();

		if (headers != null) {
			for (final String[] nameValuePairs : headers) {
				if ((nameValuePairs == null) || (nameValuePairs.length == 0)) {
					continue;
				}

				String s = "";

				for (int i = 1; i < nameValuePairs.length; i++) {
					if ((i > 1) && (i != nameValuePairs.length - 1)) {
						s = s + ',';
					}

					s = s + nameValuePairs[i];
				}

				mapHeaders.put(nameValuePairs[0], s);
			}
		}

		return post(uri, data, mapHeaders, cookieString);
	}

	public static byte[] post(
			final String uri,
			final byte[] data,
			final Map<String, String> headers,
			final String cookieString)
			throws NotForUserException {
		final CloseableHttpClient httpclient = createCloseableHttpClient();
		final HttpPost httpPost = new HttpPost(uri);

		HttpClientContext context = null;

		if (cookieString != null) {
			final URL url = getUrl(uri);
			final BasicCookieStore cookieStore = new BasicCookieStore();
			final Cookie[] cookies = getCookies(cookieString);

			for (final Cookie cookie : cookies) {
				((BasicClientCookie) cookie).setDomain(url.getHost());
				((BasicClientCookie) cookie).setPath("/");
				cookieStore.addCookie(cookie);
			}

			context = HttpClientContext.create();
			context.setCookieStore(cookieStore);
		}

		if (headers != null) {
			for (final Entry<String, String> entry : headers.entrySet()) {
				httpPost.addHeader(entry.getKey(), entry.getValue());
			}			
		}

		if (data != null) {
			httpPost.setEntity(new ByteArrayEntity(data));
		}

		final CloseableHttpResponse response = httpClientExecute(httpclient, httpPost, context);

		try {
			final HttpEntity entity = response.getEntity();

			if (entity != null) {
				InputStream in = httpEntityGetContent(entity);

				if (in != null) {
					final byte[] res = IOUtilities.inputStreamToByteArray(in);

					EntityUtils.consumeQuietly(entity);

					return res;
				}
			}

			return null;
		} finally {
			IOUtils.closeQuietly(response);
		}
	}

	public static HttpResult get(
			final String uri,
			final String[][] headers,
			final String cookieString) throws NotForUserException {
		final Map<String, String> mapHeaders = new HashMap<>();

		if (headers != null) {
			for (final String[] nameValuePairs : headers) {
				if ((nameValuePairs == null) || (nameValuePairs.length == 0)) {
					continue;
				}

				String s = "";

				for (int i = 1; i < nameValuePairs.length; i++) {
					if ((i > 1) && (i != nameValuePairs.length - 1)) {
						s = s + ',';
					}

					s = s + nameValuePairs[i];
				}

				mapHeaders.put(nameValuePairs[0], s);
			}
		}

		return get(uri, mapHeaders, cookieString);
	}

	public static HttpResult get(
			final String uri, 
			final Map<String, String> headers, 
			final String cookieString) throws NotForUserException {
		final CloseableHttpClient httpclient = createCloseableHttpClient();

		try {
			final HttpGet httpGet = new HttpGet(uri);

			HttpClientContext context = null;

			if (cookieString != null) {
				final URL url = getUrl(uri);
				final BasicCookieStore cookieStore = new BasicCookieStore();
				final Cookie[] cookies = getCookies(cookieString);

				for (final Cookie cookie : cookies) {
					((BasicClientCookie) cookie).setDomain(url.getHost());
					((BasicClientCookie) cookie).setPath("/");
					cookieStore.addCookie(cookie);
				}

				context = HttpClientContext.create();
				context.setCookieStore(cookieStore);
			}

			for (final Entry<String, String> entry : headers.entrySet()) {
				httpGet.addHeader(entry.getKey(), entry.getValue());
			}

			final CloseableHttpResponse response = httpClientExecute(httpclient, httpGet, context);

			final int statusCode = response.getStatusLine().getStatusCode();
			final HttpResult res = new HttpResult();
			res.setStatus(statusCode);

			try {
				final HttpEntity entity = response.getEntity();

				if (entity != null) {
					InputStream in = httpEntityGetContent(entity);

					if (in != null) {
						res.setData(IOUtilities.inputStreamToByteArray(in));

						EntityUtils.consumeQuietly(entity);
					}
				}

				return res;
			} finally {
				IOUtils.closeQuietly(response);
			}
		} finally {
			IOUtils.closeQuietly(httpclient);
		}
	}

	public static boolean isVerifiySsl() {
		return verifySsl;
	}

	public static void setVerifySsl(final boolean verifySsl) {
		HttpUtils.verifySsl = verifySsl;
	}
}
