package me.yoram.commons.j2se.utils;

import java.security.SecureRandom;

/**
 * @author Yoram Halberstam.
 */
public class RandomUtils {
    private static final String ALLOWED_CHARS = "2346789ABCDEFGHJKLMNPQRTUVWXYZ";
    private static final SecureRandom RND = new SecureRandom();

    public static String generateRandomString(final int length) {
        return generateRandomString(ALLOWED_CHARS, length);
    }

    public static String generateRandomString(final String allowedCharacters, final int length) {
        final byte[] data = new byte[length];
        final int allowedCharLength = allowedCharacters.length();

        for (int i = 0; i < length; i++) {
            final int pos = RND.nextInt(allowedCharLength);

            data[i] = (byte)allowedCharacters.charAt(pos);
        }

        return new String(data);
    }

    private RandomUtils() {

    }
}
