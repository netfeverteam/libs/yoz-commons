package me.yoram.commons.j2se.utils.hostname;

public interface IHostnameProvider {
    String getHostname();
}
