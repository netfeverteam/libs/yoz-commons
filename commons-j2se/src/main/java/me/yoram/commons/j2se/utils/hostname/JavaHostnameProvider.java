package me.yoram.commons.j2se.utils.hostname;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class JavaHostnameProvider implements IHostnameProvider {
    private static final Logger LOG = LogManager.getLogger(JavaHostnameProvider.class);

    @Override
    public String getHostname() {
        try {
            String s = InetAddress.getLocalHost().getHostName();
            
            if (s != null) {
                LOG.info("Java Hostname Provider returned " + s);
            }
            
            return s;
        } catch (UnknownHostException e) {
            LOG.warn(String.format("Error getting host name with java InetAddress class: %s", e.getMessage()), e);
            return null;
        }
    }
}
