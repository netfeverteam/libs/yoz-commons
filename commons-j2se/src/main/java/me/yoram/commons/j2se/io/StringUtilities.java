package me.yoram.commons.j2se.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

import me.yoram.commons.j2se.exceptions.NotForUserException;
import org.apache.commons.io.IOUtils;

import me.yoram.commons.j2se.exceptions.BusinessException;

/**
 * String utilities method. All methods must throw {@link BusinessException}
 * 
 * @author Yoram Halberstam
 */
public final class StringUtilities {
    private StringUtilities() {
    }

    /**
     * Create a new String from a byte array with an encoding
     * @param b the byte array
     * @param encoding the encoding
     * @return a new String from a byte array with an encoding
     * @throws NotForUserException if an error occurs
     */
    public static String newString(final byte[] b, final String encoding)
            throws NotForUserException {
        try {
            return new String(b, encoding);
        } catch (UnsupportedEncodingException e) {
            throw new NotForUserException(
                    String.format("The named charset [%s] is not supported.", encoding), e);
        }
    }
    
    public static String inputStreamToString(final InputStream stream) throws NotForUserException {
        return inputStreamToString(stream, Charset.defaultCharset().name());
    }

    public static String inputStreamToString(
            final InputStream stream,
            final String encoding) throws NotForUserException {
        try {
            return IOUtils.toString(stream, encoding);
        } catch (UnsupportedCharsetException e) {
            throw new NotForUserException(
                    String.format("The named charset [%s] is not supported.", encoding),
                    e);
        } catch (UnsupportedEncodingException e) {
            throw new NotForUserException(
                    String.format("The named charset [%s] is not supported.", encoding),
                    e);
        } catch (IOException e) {
            throw new NotForUserException(String.format("IO Exception: %s.", e.getMessage()), e);
        }
    }

    public static String numbersOnly(final String in) {
        final StringBuilder sb = new StringBuilder();
        final String numbers = "0123456789";

        for (char ch: in.toCharArray()) {
            if (numbers.indexOf(ch) != -1) {
                sb.append(ch);
            }
        }

        return sb.toString();
    }

    public static String getDefault(final String s, final String default_) {
        if (s == null) {
            return default_;
        } else {
            return s;
        }
    }
}
