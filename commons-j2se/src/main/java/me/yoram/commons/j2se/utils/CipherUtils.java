package me.yoram.commons.j2se.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * Created by yoram on 03/06/15.
 */
public final class CipherUtils {
    private static final Logger LOG = LogManager.getLogger(CipherUtils.class);

    private CipherUtils() {}

    public static void copy(InputStream in, OutputStream out) throws IOException {
        final byte[] buf = new byte[2048];
        int nread;

        while ((nread = in.read(buf)) != -1) {
            out.write(buf, 0, nread);
        }
    }

    public static byte[] toByteArray(InputStream in) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        copy(in, baos);
        return baos.toByteArray();
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String generateSymmetricKey(final String alg, final int size) {
        try {
            final KeyGenerator keyGen = KeyGenerator.getInstance(alg);
            keyGen.init(size);

            final SecretKey key = keyGen.generateKey();

            return bytesToHex(key.getEncoded());
        } catch (Throwable t) {
            throw new RuntimeException(t.getMessage(), t);
        }
    }

    public static String generateAES128Key() {
        return generateSymmetricKey("AES", 128);
    }

    public static void symmetricCipher(
            final byte[] key, final String alg, final InputStream in, final OutputStream out) throws IOException {
        try {
            final Cipher cipher = Cipher.getInstance(alg);
            final SecretKey keySpec = new SecretKeySpec(key, alg);

            cipher.init(Cipher.ENCRYPT_MODE, keySpec);

            try (
                    final OutputStream cOut = new CipherOutputStream(out, cipher)
            ) {
                copy(in, cOut);
            }
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            throw new IOException(e.getMessage(), e);
        }

    }

    public static void symmetricCipher(
            final String hexKey, final String alg, final InputStream in, final OutputStream out) throws IOException {
        symmetricCipher(hexStringToByteArray(hexKey), alg, in, out);
    }

    public static String symmetricCipher(
            final byte[] key, final String alg, final InputStream in) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        symmetricCipher(key, alg, in, baos);
        return bytesToHex(baos.toByteArray());
    }

    public static String symmetricCipher(
            final String hexKey, final String alg, final InputStream in) throws IOException {
        return symmetricCipher(hexStringToByteArray(hexKey), alg, in);
    }

    public static void symmetricDecipher(
            final byte[] key, final String alg, final InputStream in, final OutputStream out) throws IOException {
        try {
            final Cipher cipher = Cipher.getInstance(alg);
            final SecretKey keySpec = new SecretKeySpec(key, alg);

            cipher.init(Cipher.DECRYPT_MODE, keySpec);

            try (
                    final OutputStream cOut = new CipherOutputStream(out, cipher)
            ) {
                copy(in, cOut);
            }
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            throw new IOException(e.getMessage(), e);
        }

    }

    public static void symmetricDecipher(
            final String hexKey, final String alg, final InputStream in, final OutputStream out) throws IOException {
        symmetricDecipher(hexStringToByteArray(hexKey), alg, in, out);
    }

    public static String symmetricDecipher(
            final byte[] key, final String alg, final String hexString) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();

        symmetricDecipher(key, alg, new ByteArrayInputStream(hexStringToByteArray(hexString)), baos);

        return new String(baos.toByteArray());
    }

    public static String symmetricDecipher(
            final String hexKey, final String alg, final String hexString) throws IOException {
        return symmetricDecipher(hexStringToByteArray(hexKey), alg, hexString);
    }

    public static KeyPair generateKeyPair(final String alg, final int size) throws NoSuchAlgorithmException {
        final KeyPairGenerator keyGen = KeyPairGenerator.getInstance(alg);
        final SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

        keyGen.initialize(size, random);

        return keyGen.generateKeyPair();
    }

    public static KeyPair generateKeyPair() {
        try {
            return generateKeyPair("RSA", 2048);
        } catch (Throwable t) {
            throw new RuntimeException(t.getMessage(), t);
        }
    }

    public static void sign(final byte[] data, final String alg, final PrivateKey ppk, final OutputStream out) throws
            NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException {
        final Signature sig = Signature.getInstance(alg);
        sig.initSign(ppk);
        sig.update(data);
        out.write(sig.sign());
    }

    public static void sign(final byte[] data, final PrivateKey ppk, final OutputStream out) {
        try {

            sign(data, "SHA1WithRSA", ppk, out);
        } catch (Throwable t) {
            throw new RuntimeException(t.getMessage(), t);
        }
    }

    public static String sign(final byte[] data, final String alg, final PrivateKey ppk) throws
            NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        sign(data, alg, ppk, baos);

        return Base64.getEncoder().encodeToString(baos.toByteArray());
    }

    public static String sign(final byte[] data, final PrivateKey ppk) {
        try {
            return sign(data, "SHA1WithRSA", ppk);
        } catch (Throwable t) {
            throw new RuntimeException(t.getMessage(), t);
        }
    }

    public static boolean verifySignature(
            final byte[] data, final String alg, final PublicKey pub, final InputStream in)
    throws NoSuchAlgorithmException, SignatureException, InvalidKeyException, IOException {
        Signature sig = Signature.getInstance(alg);
        sig.initVerify(pub);
        sig.update(data);

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        copy(in, baos);
        return sig.verify(baos.toByteArray());
    }

    public static boolean verifySignature(final byte[] data, final PublicKey pub, final InputStream in) {
        try {
            return verifySignature(data, "SHA1WithRSA", pub, in);
        } catch (Throwable t) {
            throw new RuntimeException(t.getMessage(), t);
        }
    }

    public static boolean verifySignature(
            final byte[] data, final String alg, final PublicKey pub, final String signature)
            throws NoSuchAlgorithmException, SignatureException, InvalidKeyException, IOException {
        return verifySignature(data, alg, pub, new ByteArrayInputStream(Base64.getDecoder().decode(signature)));
    }

    public static boolean verifySignature(final byte[] data, final PublicKey pub, final String signature) {
        try {
            return verifySignature(data, "SHA1WithRSA", pub, signature);
        } catch (Throwable t) {
            throw new RuntimeException(t.getMessage(), t);
        }
    }

    public static PublicKey loadPublicKey(final InputStream in)
            throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        final X509EncodedKeySpec spec = new X509EncodedKeySpec(toByteArray(in));
        final KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

    public static PrivateKey loadPrivateKey(InputStream in) throws Exception {
        final PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(toByteArray(in));
        final KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }
}
