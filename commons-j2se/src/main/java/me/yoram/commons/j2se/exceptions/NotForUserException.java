package me.yoram.commons.j2se.exceptions;

/**
 * Created by yoram on 30/03/16.
 */
public class NotForUserException extends BusinessException {
    public static final String SYSTEM_ERROR = "System error, the administrator has been notified.";

    public NotForUserException(String message) {
        super(message, SYSTEM_ERROR);
    }

    public NotForUserException(String message, Throwable cause) {
        super(message, SYSTEM_ERROR, cause);
    }
}
