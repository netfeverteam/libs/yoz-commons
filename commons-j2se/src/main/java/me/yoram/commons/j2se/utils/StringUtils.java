package me.yoram.commons.j2se.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 29/05/18
 */
public class StringUtils {
    public static String[] splitNoBlank(final String data, String splitter) {
        if (data == null) {
            return new String[0];
        }

        final String[] parts = data.split(splitter);
        final List<String> res = new ArrayList<>();

        for (String p: parts) {
            p = p.trim();

            if (!p.isEmpty()) {
                res.add(p);
            }
        }

        return res.toArray(new String[0]);
    }
}
