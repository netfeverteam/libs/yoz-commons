/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2se.utils;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 21/09/18
 */
public class I18 {
    private static String defaultMessageBundleBaseName = null;

    public static String getDefaultMessageBundleBaseName() {
        return defaultMessageBundleBaseName;
    }

    public static void setDefaultMessageBundleBaseName(String defaultMessageBundleBaseName) {
        I18.defaultMessageBundleBaseName = defaultMessageBundleBaseName;
    }

    static ResourceBundle getBundle(final String baseName, final Locale locale) {
        try {
            return ResourceBundle.getBundle(baseName, locale);
        } catch (Throwable t) {
            return null;
        }
    }

    public static String getMessage(
            final String messageId, final String defaultMessage) {
        return getMessage(null, messageId, defaultMessage, null);
    }

    public static String getMessage(
            final String baseName, final String messageId, final String defaultMessage, final String language) {
        final String bname = baseName == null ? defaultMessageBundleBaseName : baseName;

        if (bname == null) {
            return defaultMessage;
        }

        if (language != null) {
            final Locale locale = new Locale(language, language);
            final ResourceBundle bundle = getBundle(bname, locale);

            if (bundle != null && bundle.containsKey(messageId)) {
                return new String(bundle.getString(messageId).getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
            }
        }

        final Locale locale = new Locale("en", "US");
        final ResourceBundle bundle = getBundle(bname, locale);

        if (bundle != null && bundle.containsKey(messageId)) {
            return new String(bundle.getString(messageId).getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        }

        return defaultMessage;
    }
}
