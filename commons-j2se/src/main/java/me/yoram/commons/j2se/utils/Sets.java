package me.yoram.commons.j2se.utils;

import java.util.*;

/**
 * Created by yoram on 23/03/16.
 */
public final class Sets {
    public static <T> Set<T> intersection(Set<? extends T> a, Set<? extends T> b) {
        Set<T> union = new HashSet<>(a);
        union.addAll(b);

        Set<T> intersection = new HashSet<>(a);
        intersection.retainAll(b);

        Set<T> difference = new HashSet<>(a);
        difference.removeAll(b);

        return difference;
    }

    public static Set<String> toLowerCase(Set<String> set, boolean treeset) {
        Set<String> res;

        if (treeset) {
            res = new TreeSet<>();
        } else {
            res = new HashSet<>();
        }

        for (String s: set) {
            res.add(s.toLowerCase());
        }

        return res;
    }
}
