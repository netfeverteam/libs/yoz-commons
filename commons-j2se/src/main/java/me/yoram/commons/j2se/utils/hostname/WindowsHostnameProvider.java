package me.yoram.commons.j2se.utils.hostname;

public final class WindowsHostnameProvider implements IHostnameProvider {
    @Override
    public String getHostname() {
        return System.getenv("COMPUTERNAME"); 
    }
}
