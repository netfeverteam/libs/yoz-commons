package me.yoram.commons.j2se.utils.hostname;

public interface IExpandKeywordsEvent {
	String onKeyword(String key, String value);
}
