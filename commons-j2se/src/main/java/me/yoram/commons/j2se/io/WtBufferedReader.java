package me.yoram.commons.j2se.io;

import java.io.BufferedReader;
import java.io.IOException;

import me.yoram.commons.j2se.exceptions.NotForUserException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import me.yoram.commons.j2se.exceptions.BusinessException;

/**
 * BufferedReader wrapper. All methods must throw {@link BusinessException}
 *
 * @author Yoram Halberstam
 */
public final class WtBufferedReader {
    private static final Logger LOG = LogManager.getLogger(WtBufferedReader.class);
    private final BufferedReader reader;

    /**
     * Instantiate the wrapper
     * @param reader the reader
     */
    public WtBufferedReader(final BufferedReader reader) {
        super();
        
        this.reader = reader;
    }
    
    /**
     * Read a line from the reader, essentially calls {@link BufferedReader#readLine()}
     * @return the line read
     * @throws NotForUserException if an error occurs
     */
    public String readLine() throws NotForUserException {
        try {
            return this.reader.readLine();
        } catch (IOException e) {
            throw new NotForUserException("Could not read a line from the reader", e);
        }
    }

    /**
     * Read a line from the reader quietly, essentially calls {@link BufferedReader#readLine()}
     * @return the line read or null if fails
     */
    public String readLineQuietly() {
        try {
            return readLine();
        } catch (BusinessException e) {
            LOG.error(String.format("Error reading line quietly: %s", e.getMessage()), e);
            return null;
        }
    }

    /**
     * Close the internal {@link BufferedReader}
     * @throws NotForUserException if an error occurs
     */
    public void close() throws NotForUserException {
        try {
            this.reader.close();
        } catch (IOException e) {
            throw new NotForUserException("Could not close reader", e);
        }
    }

    /**
     * Close the internal {@link BufferedReader} but do not throw an exception if a error occurs
     */
    public void closeQuietly() {
        try {
            close();
        } catch (BusinessException e) {
            LOG.error(String.format("Error closing quietly: %s", e.getMessage()));            
        }
    }
}
