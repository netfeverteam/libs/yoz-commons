package me.yoram.commons.j2se.nio;

import java.nio.ByteBuffer;
import java.nio.ReadOnlyBufferException;

import me.yoram.commons.j2se.exceptions.BusinessException;
import me.yoram.commons.j2se.exceptions.NotForUserException;

/**
 * ByteBuffer wrapper. All methods must throw {@link BusinessException}
 * 
 * @author Yoram Halberstam
 */
public final class YmeByteBuffer {
    private final ByteBuffer buffer;

   /**
    * Instantiate the wrapper
    * @param buffer ByteBuffer to wrap
    */
    public YmeByteBuffer(final ByteBuffer buffer) {
        super();

        this.buffer = buffer;
    }

    public short getUnsignedByte() {
        return ((short) (this.buffer.get() & 0xff));
    }

    public void putUnsignedByte(int value) {
        this.buffer.put((byte) (value & 0xff));
    }

    public short getUnsignedByte(int position) {
        return ((short) (this.buffer.get(position) & (short) 0xff));
    }

    public void putUnsignedByte(int position, int value) {
        this.buffer.put(position, (byte) (value & 0xff));
    }

    public int getUnsignedShort() {
        return (this.buffer.getShort() & 0xffff);
    }

    public void putUnsignedShort(int value) {
        this.buffer.putShort((short) (value & 0xffff));
    }

    public int getUnsignedShort(int position) {
        return (this.buffer.getShort(position) & 0xffff);
    }

    public void putUnsignedShort(int position, int value) {
        this.buffer.putShort(position, (short) (value & 0xffff));
    }

    public long getUnsignedInt() {
        return ((long) this.buffer.getInt() & 0xffffffffL);
    }

    public void putUnsignedInt(long value) {
        this.buffer.putInt((int) (value & 0xffffffffL));
    }

    public long getUnsignedInt(int position) {
        return ((long) this.buffer.getInt(position) & 0xffffffffL);
    }

    public void putUnsignedInt(int position, long value) {
        this.buffer.putInt(position, (int) (value & 0xffffffffL));
    }
}
