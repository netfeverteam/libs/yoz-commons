package me.yoram.commons.j2se.preferences.api;

import me.yoram.commons.j2se.utils.hostname.ExpandKeywordsRegistry;
import me.yoram.commons.j2se.utils.hostname.IExpandKeywordsEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.prefs.AbstractPreferences;

/**
 * Preferences implementation using {@link Map} for storing information.
 * 
 * @author Yoram Halberstam
 */
public final class WtPreferences extends AbstractPreferences {
	private static final IExpandKeywordsEvent EXPAND_REGISTRY = ExpandKeywordsRegistry.getDefaultInstance();

    private final Map<String, String> node = new HashMap<>();
    private final Map<String, WtPreferences> children = new HashMap<>();

    /**
     * Creates a preference node with the specified parent and the specified
     * name relative to its parent.
     *
     * @param parent
     *            the parent of this preference node, or null if this is the
     *            root.
     * @param name
     *            the name of this preference node, relative to its parent, or
     *            <tt>""</tt> if this is the root.
     * @throws IllegalArgumentException
     *             if <tt>name</tt> contains a slash (<tt>'/'</tt>), or
     *             <tt>parent</tt> is <tt>null</tt> and name isn't <tt>""</tt>.
     */
    protected WtPreferences(AbstractPreferences parent, String name) {
        super(parent, name);
    }

    /**
     * Put the given key-value association into this preference node.
     *
     * <p>
     * This method is invoked with the lock on this node held.
     */
    @Override
    protected void putSpi(String key, String value) {
        node.put(key, value);
    }

    /**
     * Return the value associated with the specified key at this preference
     * node, or <tt>null</tt> if there is no association for this key, or the
     * association cannot be determined at this time.
     * 
     * <p>
     * This method is invoked with the lock on this node held.
     */
    @Override
    protected String getSpi(String key) {
        String unexpanded = node.get(key);
        return ExpandKeywordsRegistry.expandKeywords(unexpanded, EXPAND_REGISTRY);
    }

    /**
     * Remove the association (if any) for the specified key at this preference
     * node.
     *
     * <p>
     * This method is invoked with the lock on this node held.
     */
    @Override
    protected void removeSpi(String key) {
        node.remove(key);
    }

    /**
     * Removes this preference node, invalidating it and any preferences that it
     * contains.
     *
     * <p>
     * This method is invoked with the lock held on this node and its parent.
     *
     * <p>
     * The removal of a node is persistent.
     */
    @Override
    protected void removeNodeSpi() {
        node.clear();
    }

    /**
     * Returns all of the keys that have an associated value in this preference
     * node. (The returned array will be of size zero if this node has no
     * preferences.)
     *
     * <p>
     * This method is invoked with the lock on this node held.
     */
    @Override
    protected String[] keysSpi() {
        return node.keySet().toArray(new String[0]);
    }

    /**
     * Returns the names of the children of this preference node. (The returned
     * array will be of size zero if this node has no children.)
     *
     * <p>
     * This method is invoked with the lock on this node held.
     */
    @Override
    protected String[] childrenNamesSpi() {
        return children.keySet().toArray(new String[0]);
    }

    /**
     * Returns the named child of this preference node, creating it if it does
     * not already exist.
     * 
     * <p>
     * This method is invoked with the lock on this node held.
     */
    @Override
    protected AbstractPreferences childSpi(String name) {
        WtPreferences pref = children.get(name);

        if (pref == null) {
            pref = new WtPreferences(this, name);
            children.put(name, pref);
        }

        return pref;
    }

    /**
     * This method is not implemented since syncing is automatic in memory.
     */
    @Override
    protected void syncSpi() {
        // his method is not implemented since syncing is automatic in memory
    }

    /**
     * This method is not implemented since flushing is automatic in memory.
     */
    @Override
    protected void flushSpi() {
        // This method is not implemented since flushing is automatic in memory.
    }
}
