package me.yoram.commons.j2se.preferences.impl;

import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

/**
 * This class allows switching Preference Factories at run time. This is impossible otherwise because the 
 * {@link PreferencesFactory} load once and stays set until the JVM dies.
 *  
 * @author Yoram Halberstam
 */
public class MultiPreferencesFactory implements PreferencesFactory {
    static {
        System.out.println("launched MultiPreferencesFactory");
    }
    
    private static PreferencesFactory factory = new MemoryPreferencesFactory();

    /**
     * @return the current Preference Factory
     */
    public static PreferencesFactory getFactory() {
        return MultiPreferencesFactory.factory;
    }

    /**
     * Set the preference factory
     * @param factory the preference factory
     */
    public static void setFactory(PreferencesFactory factory) {
        if (factory == null) {
            MultiPreferencesFactory.factory = new MemoryPreferencesFactory();
        } else {
            MultiPreferencesFactory.factory = factory;
        }
    }

    @Override
    public Preferences systemRoot() {
        return factory.systemRoot();
    }

    @Override
    public Preferences userRoot() {
        return factory.userRoot();
    }
}
