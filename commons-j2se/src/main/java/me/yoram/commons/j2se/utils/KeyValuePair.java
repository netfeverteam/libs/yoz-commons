package me.yoram.commons.j2se.utils;

import java.io.Serializable;

public final class KeyValuePair implements Serializable {
	private static final long serialVersionUID = 3100522807552894035L;

	private String key;
	private Object value;
	
	public KeyValuePair() {
		super();
	}

	public KeyValuePair(final String key, final Object value) {
		super();
		
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(final String key) {
		this.key = key;
	}

	public Object getValue() {
		return this.value;
	}

	public void setValue(final Object value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return String.format("%s: %s", key == null ? "null" : key, value == null ? "null" : value.toString());
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return toString().equals(obj.toString());
	}
}
