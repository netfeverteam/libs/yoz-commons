package me.yoram.commons.j2se.utils;

import me.yoram.commons.j2se.exceptions.BusinessException;
import me.yoram.commons.j2se.exceptions.NotForUserException;
import org.apache.commons.io.IOUtils;

import javax.activation.DataSource;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Created by yoram on 30/05/16.
 */
public class ZipUtils {
    public interface OnCopyEvent {
        boolean copy(ZipEntry zip, InputStream in, OutputStream out) throws IOException;
    }

    public static File unzip(String filename) throws BusinessException {
        return unzip(new File(filename));
    }

    public static File unzip(File f) throws BusinessException {
        try {
            if (!f.isFile()) {
                throw new BusinessException(String.format("The file %s does not exists", f.getName()));
            }

            InputStream in = new FileInputStream(f);

            try {
                return unzip(in);
            } finally {
                in.close();
            }
        } catch (IOException e) {
            throw new NotForUserException(e.getMessage(), e);
        }
    }

    public static File unzip(InputStream in) throws BusinessException {
        File tmp = FileSystemUtils.createTemporaryFolder("ZipUtils", ".unzip");

        try {
            unzip(in, tmp);

            return tmp;
        } catch (BusinessException e) {
            throw e;
            // TODO - DELETE FOLDER
        }
    }

    public static void unzip(String filename, File rootFolder) throws BusinessException {
        unzip(new File(filename), rootFolder);
    }

    public static void unzip(File f, File rootFolder) throws BusinessException {
        try {
            if (!f.isFile()) {
                throw new BusinessException(String.format("The file %s does not exists", f.getName()));
            }

            if (!rootFolder.isDirectory()) {
                throw new BusinessException(String.format("The folder %s does not exists", rootFolder.getName()));
            }

            InputStream in = new FileInputStream(f);

            try {
                unzip(in, rootFolder);
            } finally {
                IOUtils.closeQuietly(in);
            }
        } catch (IOException e) {
            throw new NotForUserException(e.getMessage(), e);
        }
    }

    public static void unzip(InputStream in, File rootFolder) throws BusinessException {
        try {
            final ZipInputStream zIn = new ZipInputStream(in);

            try {
                ZipEntry entry;
                while ((entry = zIn.getNextEntry()) != null) {
                    final File f = new File(rootFolder, entry.getName());

                    if (entry.isDirectory()) {
                        f.mkdirs();
                    } else {
                        f.getParentFile().mkdirs();

                        final OutputStream out = new FileOutputStream(f);

                        try {
                            IOUtils.copy(zIn, out);
                        } finally {
                            IOUtils.closeQuietly(out);
                        }
                    }
                }
            } finally {
                IOUtils.closeQuietly(zIn);
            }
        } catch (IOException e) {
            throw new NotForUserException(e.getMessage(), e);
        }
    }

    public static void zipSources(final OutputStream dest, final DataSource... sources) throws BusinessException {
        try {
            final ZipOutputStream zOut = new ZipOutputStream(dest);

            try {
                for (DataSource source: sources) {
                    zOut.putNextEntry(new ZipEntry(source.getName()));

                    try {
                        IOUtils.copy(source.getInputStream(), zOut);
                    } finally {
                        zOut.closeEntry();
                    }
                }
            } finally {
                IOUtils.closeQuietly(zOut);
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    public static void zipFolder(File folder, OutputStream out) throws BusinessException {
        if (!folder.isDirectory()) {
            throw new BusinessException(String.format("%s is not a folder"));
        }

        String[] filenames = folder.list();

        try {
            ZipOutputStream zOut = new ZipOutputStream(out);

            try {
                for (String filename: filenames) {
                    File f = new File(folder, filename);

                    if (f.isFile()) {
                        zOut.putNextEntry(new ZipEntry(filename));

                        try {
                            InputStream in = new FileInputStream(f);

                            try {
                                IOUtils.copy(in, zOut);
                            } finally {
                                IOUtils.closeQuietly(in);
                            }
                        } finally {
                            zOut.closeEntry();
                        }
                    }
                }
            } finally {
                IOUtils.closeQuietly(zOut);
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);

        }
    }

    public static void zipFolder(File folder, File outFile) throws BusinessException {
        try {
            OutputStream out = new FileOutputStream(outFile);

            try {
                zipFolder(folder, out);
            } finally {
                IOUtils.closeQuietly(out);
            }
        } catch (Exception e) {
            throw new NotForUserException(e.getMessage(), e);
        }
    }

    public static File zipFolder(File folder) throws BusinessException {
        try {
            File outFile = File.createTempFile("ZipUtils", ".zip");

            zipFolder(folder, outFile);

            return outFile;
        } catch (Exception e) {
            throw new NotForUserException(e.getMessage(), e);
        }
    }

    public static void copy(final InputStream in, final OutputStream out, OnCopyEvent event) throws BusinessException {
        try {
            try (
                    final ZipInputStream zIn = new ZipInputStream(in);
                    final ZipOutputStream zOut = new ZipOutputStream(out);
            ) {
                ZipEntry entry;
                while ((entry = zIn.getNextEntry()) != null) {
                    final ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    if (event.copy(entry, zIn, baos)) {
                        ZipEntry outEntry = new ZipEntry(entry.getName());

                        zOut.putNextEntry(outEntry);
                        if (baos.size() == 0) {
                            IOUtils.copy(zIn, zOut);
                        } else {
                            IOUtils.copy(new ByteArrayInputStream(baos.toByteArray()), zOut);
                        }
                    }
                }
            }
        } catch (Throwable t) {
            throw new NotForUserException(t.getMessage(), t);
        }
    }

    public static Collection<DataSource> unzipSources(final InputStream in) throws BusinessException {
        try {
            final Collection<DataSource> res = new ArrayList<>();
            final ZipInputStream zIn = new ZipInputStream(in);

            try {
                ZipEntry entry;
                while ((entry = zIn.getNextEntry()) != null) {
                    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    IOUtils.copy(zIn, baos);
                    final String name = entry.getName();
                    res.add(new DataSource() {
                        @Override
                        public InputStream getInputStream() {
                            return new ByteArrayInputStream(baos.toByteArray());
                        }

                        @Override
                        public OutputStream getOutputStream() {
                            return null;
                        }

                        @Override
                        public String getContentType() {
                            return null;
                        }

                        @Override
                        public String getName() {
                            return name;
                        }
                    });
                }

                return res;
            } finally {
                IOUtils.closeQuietly(zIn);
            }
        } catch (Throwable t) {
            throw new BusinessException(t.getMessage(), t);
        }
    }
}
