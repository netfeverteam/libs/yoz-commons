package me.yoram.commons.j2se.utils.hostname;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import org.apache.commons.lang3.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class is a singleton that loads useful system properties and environment
 * variables.
 * 
 * @author Yoram Halberstam
 */
public final class Environment {
    private static final Logger LOG = LogManager.getLogger(Environment.class);

    /**
     * Hostname override property. See {{@link #getHostname()}
     */
    public static final String SYSTEM_PROP_HOSTNAME_OVERRIDE = "com.wt.hostname.override";

    private Environment() {
    }

    public static void main(String[] args) {
    	if (args[0].equals("hostname")) {
    		System.out.println(String.format("Your hostname is [%s]", getHostname()));
    		System.out.println("Please use the name above in the square brackets ^^");
    	} else if (args[0].equals("list-nis")) {
    		listAdapters();
    		System.out.println("Please use the value in the square brackets ^^ when adding configuration such as {LAN-IP:ni}.");
    	}
	}

    /**
     * Return the hostname for the machine the code is running on.
     * 
     * It first look in for the value in the system property
     * {@value #SYSTEM_PROP_HOSTNAME_OVERRIDE}, then it looks to get it via the
     * Java API and finally by the environment variable: <code>HOSTNAME</CODE>
     * on linux or <code>COMPUTERNAME</code> on Windows machine. If none is
     * found it return 'localhost'.
     * 
     * @return the hostname for the machine the code is running on.
     */
    public static String getHostname() {
        final IHostnameProvider[] providers = new IHostnameProvider[] {
                new HostnameOverrideProvider(),
                new JavaHostnameProvider(),
                SystemUtils.IS_OS_LINUX ? new LinuxHostnameProvider() : new WindowsHostnameProvider() };

        for (final IHostnameProvider provider : providers) {
            final String res = provider.getHostname();

            if (res != null) {
				LOG.info("Host name used by preferences is " + res);

                return res;
            }
        }

        LOG.warn("Hostname not found, reverting to localhost");
        return "localhost";
    }
    
    public static String getIp(final String adapter)  {
    	  try {
    		  final Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
    		  
    		  while (interfaces.hasMoreElements()) {
    			  NetworkInterface nic = interfaces.nextElement();
      			  
    			  for (final InterfaceAddress addr: nic.getInterfaceAddresses()) {
    			        final InetAddress inet_addr = addr.getAddress();

      			        if (!( inet_addr instanceof Inet4Address)) {
      			            continue;
      			        }

      			        if (nic.getName().equals(adapter)) {
      			        	return inet_addr.getHostAddress();
      			        }
    			  }
    		  }
    	  } catch(Exception e) {
    		  LOG.error(e.getMessage(), e);
    	  }
    	  
    	  return "127.0.0.1";
    }      

    private static void listAdapters()  {
  	  try {
  		  final Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
  		  
  		  while (interfaces.hasMoreElements()) {
  			  NetworkInterface nic = interfaces.nextElement();
    			  
  			  System.out.println(String.format("Network Interface found: [%s]", nic.getName()));
  		  }
  	  } catch(Exception e) {
  		  LOG.error(e.getMessage(), e);
  	  }
  }      
}
