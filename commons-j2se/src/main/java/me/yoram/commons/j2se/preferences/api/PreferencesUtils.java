package me.yoram.commons.j2se.preferences.api;

import java.io.File;
import java.io.FileFilter;
import java.util.List;
import java.util.prefs.Preferences;

import me.yoram.commons.j2se.preferences.impl.FilePreferencesFactory;
import me.yoram.commons.j2se.utils.hostname.Environment;

/**
 * This is a utility class for {@link Preferences}
 * 
 * @author Yoram Halberstam
 */
public final class PreferencesUtils {
    private PreferencesUtils() {
    }

    /**
     * This method tries to find a system value. A system value is either a Java
     * System Property or an OS environment variable. As input it takes an array
     * of keys and will try to find the first valid one. It will first look into
     * the Java System Properties and then look into the OS environment variable
     * table.
     * 
     * @param systemKeys
     *            the array of keys to search.
     * @return the value of the key if found or null.
     */
    public static String getSystemValue(final String... systemKeys) {
        for (final String key : systemKeys) {
            if (System.getProperties().containsKey(key)) {
                return System.getProperty(key);
            }
        }

        for (final String key : systemKeys) {
            if (System.getenv().containsKey(key)) {
                return System.getenv(key);
            }
        }

        return null;
    }

    /**
     * This method looks for the file path of Java Preferences file. It will try
     * to get the {@link FilePreferencesFactory#ENV_PROP_PREFS_PATH} in the Java
     * System Properties values and if it doesn't find it it will use the
     * <code>user.dir</code> Java System Property
     * 
     * @return the root folder of the Java Preferences file
     * @throws NotForUserBackingStoreException
     *             if a path cannot be found, doesn't exists or isn't a folder
     */
    public static File getFilesPath() throws NotForUserBackingStoreException {
        final String spUserdir = "user.dir";

        final String path = PreferencesUtils.getSystemValue(
                FilePreferencesFactory.ENV_PROP_PREFS_PATH, spUserdir);

        if (path == null) {
            throw new NotForUserBackingStoreException(
                    "No back store found for FilePreferencesApi. Please configure -D"
                            + FilePreferencesFactory.ENV_PROP_PREFS_PATH
                            + " on JVM");
        }

        final File res = new File(path);

        if (!res.exists()) {
            throw new NotForUserBackingStoreException("The path " + res.getAbsolutePath()
                    + " does not exists.");
        }

        if (!res.isDirectory()) {
            throw new NotForUserBackingStoreException("The path " + path
                    + " is not a directory.");
        }

        return res;
    }

    /**
     * This method gets the Preferences file. It will browse for XML files in
     * <code>root</code> and the host sub folder. The host sub folder name is, in order:
     * <ul>
     *   <li>The one specified in {@link Environment#SYSTEM_PROP_HOSTNAME_OVERRIDE}</li>
     *   <li>The one returned by {@link Environment#getHostname()}</li>
     * </ul>
     * 
     * @param root the root folder of Java Preferences files
     * @param list the return list of the files to be loaded in that order
     */
    public static void getPreferencesFiles(
            final File root,
            final List<File> list) {
        final FileFilter xmlFilter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (pathname.isFile()) {
                    final boolean isXml = pathname.getAbsolutePath().endsWith(".xml");

                    if (isXml) {
                        list.add(pathname);

                        return true;
                    }
                }
                return false;
            }
        };

        root.listFiles(xmlFilter);

        final String hostname = Environment.getHostname();
        final File hostRoot = new File(root, hostname);

        if (hostRoot.isDirectory()) {
            hostRoot.listFiles(xmlFilter);
        }
    }
}
