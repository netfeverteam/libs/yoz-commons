package me.yoram.commons.j2se.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by yoram on 27/07/16.
 */
public class Iteratables {
    public interface StringProvider {
        String getString(Object o);
    }

    public static <T> String toString(Collection<T> set, String separator, StringProvider provider) {
        StringBuilder sb = new StringBuilder();

        for (Object o: set) {
            if (sb.length() > 0) {
                sb.append(separator);
            }

            sb.append(provider.getString(o));
        }

        return sb.toString();
    }

    public static <T> String toString(Collection<T> set, String separator) {
        return toString(set, separator, new StringProvider() {
            @Override
            public String getString(Object o) {
                return o.toString();
            }
        });
    }

    public static void split(String s, String separator, boolean allowblank, Collection<String> result) {
        String[] parts = s.split(separator);

        for (String part: parts) {
            part = part.trim();

            if (!allowblank && part.isEmpty()) {
                continue;
            }

            result.add(part);
        }
    }

}
