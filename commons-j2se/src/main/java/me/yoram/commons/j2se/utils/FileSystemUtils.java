package me.yoram.commons.j2se.utils;

import me.yoram.commons.j2se.exceptions.BusinessException;
import me.yoram.commons.j2se.exceptions.NotForUserException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by yoram on 30/05/16.
 */
public final class FileSystemUtils {
    public static class FolderCompareResult {
        private final Set<String> newFiles = new TreeSet<>();
        private final Set<String> deletedFiles = new TreeSet<>();
        private final Set<String> differentFiles = new TreeSet<>();

        FolderCompareResult() {
            super();
        }
        public Set<String> getNewFiles() {
            return this.newFiles;
        }

        public Set<String> getDeletedFiles() {
            return this.deletedFiles;
        }

        public Set<String> getDifferentFiles() {
            return this.differentFiles;
        }

        public boolean isDifferent() {
            return !this.newFiles.isEmpty() || !this.deletedFiles.isEmpty() || !this.differentFiles.isEmpty();
        }
    }

    private FileSystemUtils() {
        super();
    }

    /**
     * Make a map which key is the relative path of the file to <code>rootPath</code>
     *
     * @param rootPath
     * @param files
     * @return
     */
    public static Map<String, File> getRelativePathMap(final String rootPath, final Collection<File> files) {
        final Map<String, File> res = new TreeMap<>();

        final int rootPathLen = rootPath.length();

        for (final File f: files) {
            res.put(f.getAbsolutePath().substring(rootPathLen), f);
        }

        return res;
    }

    public static File createTemporaryFolder(final String prefix, final String suffix) throws BusinessException {
        try {
            final File res  = File.createTempFile(prefix, suffix);
            if (!res.delete()) {
                throw new BusinessException(String.format("Couldn't delete file %s", res.getAbsolutePath()));
            }

            if (!res.mkdir()) {
                throw new BusinessException(String.format("Couldn't create folder %s", res.getAbsolutePath()));
            }

            return res;
        } catch (IOException e) {
            throw new NotForUserException(e.getMessage(), e);
        }
    }

    public static boolean sameFile(String filepath1, String filepath2) throws BusinessException {
        return sameFile(new File(filepath1), new File(filepath2));
    }

    public static boolean sameFile(File file1, File file2) throws BusinessException {
        try {
            InputStream in1 = null;
            InputStream in2 = null;

            try {
                in1 = new FileInputStream(file1);
                in2 = new FileInputStream(file2);

                return equals(in1, in2);
            } finally {
                if (in1 != null) {
                    IOUtils.closeQuietly(in1);
                }

                if (in2 != null) {
                    IOUtils.closeQuietly(in2);
                }
            }
        } catch (IOException e){
            throw new NotForUserException(e.getMessage(), e);
        }
    }

    public static boolean equals(InputStream in1, InputStream in2) throws BusinessException {
        try {
            return DigestUtils.md5Hex(in1).equals(DigestUtils.md5Hex(in2));
        } catch (IOException e) {
            throw new NotForUserException(e.getMessage(), e);
        }
    }

    public static boolean equals(byte[] b1, byte[] b2) {
        return DigestUtils.md5Hex(b1).equals(DigestUtils.md5Hex(b2));
    }

    public static void deleteFolder(String path) throws BusinessException {
        deleteFolder(new File(path));
    }

    public static void deleteFolder(File path) throws BusinessException {
        if (!path.isDirectory()) {
            throw new BusinessException(String.format("%s is not a folder", path.getAbsolutePath()));
        }

        final String[] filenames = path.list();

        for (final String filename: filenames) {
            final File f = new File(path, filename);

            if (f.isFile()) {
                if (!f.delete()) {
                    throw new BusinessException(String.format("could not delete file %s", f.getAbsolutePath()));
                }
            } else if (f.isDirectory()) {
                deleteFolder(f);
            }
        }

        if (!path.delete()) {
            throw new BusinessException(String.format("could not delete folder %s", path.getAbsolutePath()));
        }
    }

    public static FolderCompareResult compareFolder(
            final String folderPath1,
            final String folderPath2) throws BusinessException {
        try {
            final FolderCompareResult res = new FolderCompareResult();

            Path path = Paths.get(folderPath1);
            FileTreeVisitor visitor = new FileTreeVisitor();
            Files.walkFileTree(path, visitor);
            final Set<File> fileset1 = visitor.getFileTree();
            Map<String, File> relativePathMap1 = getRelativePathMap(folderPath1, fileset1);

            path = Paths.get(folderPath2);
            visitor = new FileTreeVisitor();
            Files.walkFileTree(path, visitor);
            final Set<File> fileset2 = visitor.getFileTree();
            Map<String, File> relativePathMap2 = getRelativePathMap(folderPath2, fileset2);

            res.newFiles.addAll(relativePathMap2.keySet());
            res.newFiles.removeAll(relativePathMap1.keySet());

            for (final Map.Entry<String, File> entry: relativePathMap1.entrySet()) {
                if (relativePathMap2.containsKey(entry.getKey())) {
                    File f2 = relativePathMap2.get(entry.getKey());

                    if (!sameFile(entry.getValue(), f2)) {
                        res.differentFiles.add(entry.getKey());
                    }
                } else {
                    res.deletedFiles.add(entry.getKey());
                }
            }

            return res;
        } catch (IOException e) {
            throw new NotForUserException(e.getMessage(), e);
        }
    }

    public static FolderCompareResult compareFolder(
            final File folder1,
            final File folder2) throws BusinessException {
        return compareFolder(folder1.getAbsolutePath(), folder2.getAbsolutePath());
    }
}
