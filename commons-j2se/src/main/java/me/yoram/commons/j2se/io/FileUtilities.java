package me.yoram.commons.j2se.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import me.yoram.commons.j2se.exceptions.NotForUserException;
import org.apache.commons.io.FileUtils;

import me.yoram.commons.j2se.exceptions.BusinessException;

/**
 * Utility class that wraps up File methods. All methods that throws exception
 * must to throw {@link BusinessException}
 * 
 * @author Yoram Halberstam
 */
public final class FileUtilities {
    private FileUtilities() {
    }

    /**
     * Create and returns a new {@link FileInputStream} for a file 
     * @param filename the filename to open for read
     * @return a new {@link FileInputStream}for a file
     * @throws NotForUserException if an error occurs
     */
    public static FileInputStream newFileInputStream(String filename)
            throws NotForUserException {
        return newFileInputStream(new File(filename));
    }

    /**
     * Create and returns a new {@link FileInputStream} for a file 
     * @param f the file to open for read
     * @return a new {@link FileInputStream}for a file
     * @throws NotForUserException if an error occurs
     */
    public static FileInputStream newFileInputStream(File f) throws NotForUserException {
        if (!f.exists()) {
            throw new NotForUserException(
                    String.format("The file [%s] does not exists", f.getAbsolutePath()));
        }

        if (f.isDirectory()) {
            throw new NotForUserException(
                    String.format(
                        "[%s] is a folder and not a file", f.getAbsolutePath()));
        }

        try {
            return new FileInputStream(f);
        } catch (FileNotFoundException e) {
            throw new NotForUserException(
                    String.format("The file [%s] cannot be opened for reading", f.getAbsolutePath()),
                    e);
        } catch (SecurityException e) {
            throw new NotForUserException(
                    String.format(
                            "a security manager exists and its checkRead method denies read access to the file [%s].",
                            f.getAbsolutePath()),
                    e);
        }
    }

    /**
     * Create and returns a new {@link FileOutputStream} for a file 
     * @param filename the filename to open for writing
     * @return a new {@link FileOutputStream}for a file
     * @throws NotForUserException if an error occurs
     */
    public static FileOutputStream newFileOutputStream(String filename)
            throws NotForUserException {
        return newFileOutputStream(new File(filename));
    }

    /**
     * Create and returns a new {@link FileOutputStream} for a file 
     * @param f the file to open for writting
     * @return a new {@link FileOutputStream}for a file
     * @throws NotForUserException if an error occurs
     */
    public static FileOutputStream newFileOutputStream(File f)
            throws NotForUserException {
        if (f.isDirectory()) {
            throw new NotForUserException(
                    String.format("[%s] is a folder and not a file", f.getAbsolutePath()));
        }

        try {
            return new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            throw new NotForUserException(
                    String.format("The file [%s] cannot be opened for writing", f.getAbsolutePath()), e);
        } catch (SecurityException e) {
            throw new NotForUserException(
                    String.format(
                            "a security manager exists and its checkRead method denies read access to the file [%s].",
                            f.getAbsolutePath()),
                    e);
        }
    }
    
    public static File createTempFile(final String prefix, final String suffix) throws NotForUserException {
        try {
            return File.createTempFile(prefix, suffix);
        } catch (IOException e) {
            throw new NotForUserException(String.format("error creating temporary file: %s", e.getMessage()), e);
        }
    }
    
    public static void writeByteArrayToFile(final File file, final byte[] data) throws BusinessException {
        try {
            FileUtils.writeByteArrayToFile(file, data);
        } catch (IOException e) {
            throw new NotForUserException(
                    String.format("Error writting to file [%s]: %s.", file.getAbsolutePath(), e.getMessage()), e);
        }
    }

    public static byte[] readFileToByteArray(final File file) throws NotForUserException {
        try {
            return FileUtils.readFileToByteArray(file);
        } catch (IOException e) {
            throw new NotForUserException(
                    String.format("Error reading to file [%s]: %s.", file.getAbsolutePath(), e.getMessage()), e);
        }
    }
}
