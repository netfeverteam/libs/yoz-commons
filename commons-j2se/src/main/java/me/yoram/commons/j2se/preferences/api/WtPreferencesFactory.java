package me.yoram.commons.j2se.preferences.api;

import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This is the base class for Java {@link PreferencesFactory}
 * 
 * @author Yoram Halberstam
 */
public abstract class WtPreferencesFactory implements PreferencesFactory {
    private static final Logger LOG = LogManager.getLogger(WtPreferencesFactory.class);

    private final Preferences systemPrefs;
    private final Preferences userPrefs;
    private boolean populated;

    /**
     * Instantiate the Preference Factory
     */
    public WtPreferencesFactory() {
        super();

        systemPrefs = new WtPreferences(null, "");
        userPrefs = new WtPreferences(null, "");
        populated = false;
    }

    @Override
    public Preferences systemRoot() {
        doPopulate();
        return systemPrefs;
    }

    @Override
    public Preferences userRoot() {
        doPopulate();
        return userPrefs;
    }

    /**
     * Populate the backing store
     * 
     * @throws NotForUserBackingStoreException if an error occurs
     */
    protected abstract void populate() throws NotForUserBackingStoreException;

    private void doPopulate() {
        if (!populated) {
            populated = true;
            try {
                populate();
            } catch (NotForUserBackingStoreException e) {
                LOG.error("The preferences could not be populated.", e);
            }
        }
    }
}
