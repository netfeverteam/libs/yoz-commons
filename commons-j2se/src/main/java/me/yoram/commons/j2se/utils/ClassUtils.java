package me.yoram.commons.j2se.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 17/05/18
 */
public class ClassUtils {
    private ClassUtils() { }

    public static <T extends Annotation> Collection<T> getAnnotations(Class<T> clazz, AnnotatedElement... elems) {
        final List<T> res= new ArrayList<>();

        for (final AnnotatedElement elem: elems) {
            if (elem != null) {
                T o =  elem.getAnnotation(clazz);

                if (o != null) {
                    res.add(o);
                }
            }
        }

        return res;
    }

    public static boolean hasAnnotation(Class<? extends Annotation> clazz, AnnotatedElement... elems) {
        for (final AnnotatedElement elem: elems) {
            if (elem != null && elem.getAnnotation(clazz) != null) {
                return true;
            }
        }

        return false;
    }

    public static Field geDeclaredField(final Class<?> clazz, final String name, final Class<?> excludeParentClazz) {
        if (clazz == null || clazz == excludeParentClazz || name == null) {
            return null;
        }

        Class<?> currentClazz = clazz;

        do {
            try {
                return currentClazz.getDeclaredField(name);
            } catch (Throwable t) {
                // DO NOTHING
            }

            currentClazz = currentClazz.getSuperclass();
        } while (currentClazz  != null && currentClazz != excludeParentClazz);

        return null;
    }
}
