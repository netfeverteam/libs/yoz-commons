package me.yoram.commons.j2se.utils.hostname;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class HostnameOverrideProvider implements IHostnameProvider {
    private static final Logger LOG = LogManager.getFormatterLogger(HostnameOverrideProvider.class);
    
    @Override
    public String getHostname() {
        String s = System.getProperty(Environment.SYSTEM_PROP_HOSTNAME_OVERRIDE);
        
        if (s != null) {
            LOG.info(Environment.SYSTEM_PROP_HOSTNAME_OVERRIDE + " = " + s);        
        }
        
        return s;
    }
}
