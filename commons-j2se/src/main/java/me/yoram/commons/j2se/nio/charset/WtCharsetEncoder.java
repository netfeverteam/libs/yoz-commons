package me.yoram.commons.j2se.nio.charset;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.MalformedInputException;
import java.nio.charset.UnmappableCharacterException;

import me.yoram.commons.j2se.exceptions.NotForUserException;
import me.yoram.commons.j2se.nio.YmeByteBuffer;
import org.apache.commons.lang3.ArrayUtils;

import me.yoram.commons.j2se.exceptions.BusinessException;

/**
 * This class wraps {@link CharsetEncoder}. All methods must throw {@link BusinessException}
 * 
 * @author Yoram Halberstam
 */
public final class WtCharsetEncoder {
    private final CharsetEncoder encoder;

    /**
     * Instantiate with a {@link CharsetEncoder}
     * 
     * @param encoder the internal encoder 
     */
    public WtCharsetEncoder(final CharsetEncoder encoder) {
        super();

        this.encoder = encoder;        
    }

    /**
     * Use the internal charset and call {@link CharsetEncoder#encode(CharBuffer)}
     * @param in the input buffer
     * @return a {@link ByteBuffer} which is an encoded version of <code>in</code>. 
     * @throws NotForUserException if an error occurs
     */
    public YmeByteBuffer encode(CharBuffer in) throws NotForUserException {
        try {
            ByteBuffer buffer = this.encoder.encode(in);

            return new YmeByteBuffer(buffer);
        } catch (MalformedInputException e) {
            throw new NotForUserException(
                    String.format(
                            "The character sequence for [%s] starting at the input buffer's current position is not a legal sixteen-bit Unicode sequence and the current malformed-input action is CodingErrorAction.REPORT",
                            ArrayUtils.toString(in.array())),
                    e);
        } catch (UnmappableCharacterException e) {
            throw new NotForUserException(
                    String.format(
                            "the character sequence for [%s] starting at the input buffer's current position cannot be mapped to an equivalent byte sequence and the current unmappable-character action is CodingErrorAction.REPORT",
                            ArrayUtils.toString(in.array())),
                    e);
        } catch (CharacterCodingException e) {
            throw new NotForUserException(
                    String.format(
                            "A character encoding exception occurs for array [%s].",
                            ArrayUtils.toString(in.array())),
                    e);
        } catch (IllegalStateException e) {
            throw new NotForUserException("An encoding operation is already in progress.", e);
        }
    }
}
