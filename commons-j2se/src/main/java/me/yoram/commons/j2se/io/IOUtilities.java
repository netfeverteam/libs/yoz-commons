package me.yoram.commons.j2se.io;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import me.yoram.commons.j2se.exceptions.NotForUserException;
import org.apache.commons.io.IOUtils;

public final class IOUtilities {
    private IOUtilities() {
    }

    public static byte[] inputStreamToByteArray(final InputStream stream) throws NotForUserException {
        try {
            return IOUtils.toByteArray(stream);
        } catch (IOException e) {
            throw new NotForUserException(String.format("IO Exception: %s.", e.getMessage()), e);
        }
    }
    
    public static void loadProperties(
            final String errorMessage,
            final Properties props,
            final InputStream in) throws NotForUserException {
    	try {
    		props.load(in);
    	} catch (IOException e) {
    		throw new NotForUserException(errorMessage, e);
    	}    	
    }
}
