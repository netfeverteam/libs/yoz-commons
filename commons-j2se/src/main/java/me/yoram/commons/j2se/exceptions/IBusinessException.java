package me.yoram.commons.j2se.exceptions;

/**
 * Created by yoram on 30/03/16.
 */
public interface IBusinessException {
    String getUserMessage();
}
