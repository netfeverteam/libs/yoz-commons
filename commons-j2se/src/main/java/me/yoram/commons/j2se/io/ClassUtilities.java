package me.yoram.commons.j2se.io;

import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import me.yoram.commons.j2se.exceptions.BusinessException;
import me.yoram.commons.j2se.exceptions.NotForUserException;

/**
 * Class utilities. All method must return {@link BusinessException}
 * 
 * @author Yoram Halberstam
 */
public final class ClassUtilities {
    private ClassUtilities() {
    }

    private static final String[] GETTER_METHOD_PREFIXES = {"get", "is", "has"};

    /**
     * Load a class
     * @param className the classname
     * @return a {@link Class}
     * @throws NotForUserException if an error occurs
     */
    public static Class<?> forName(final String className) throws NotForUserException {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new NotForUserException(
                    String.format("The class %s was not found: %s", className, e.getMessage()), e);
        } catch (ExceptionInInitializerError e) {
            throw new NotForUserException(
                    String.format(
                            "The intialization (static block) of class %s as a failed: %s",
                            className,
                            e.getMessage()),
                    e);
        } catch (LinkageError e) {
            throw new NotForUserException(
                    String.format("The class %s as a linkage failure: %s", className, e.getMessage()), e);
        }
    }

    /**
     * Get a field on a class
     * @param clazz the class
     * @param fieldName the field name
     * @return a {@link Field} on a class
     * @throws NotForUserException if an error occurs
     */
    public static Field getDeclaredField(final Class<?> clazz,
            final String fieldName) throws NotForUserException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            throw new NotForUserException(
                    String.format( "No field named %s in class %s: %s", fieldName, clazz.getName(), e.getMessage()),
                    e);
        } catch (NullPointerException e) {
            throw new NotForUserException(
                    String.format(
                            "The fieldname was null => %s for class %s: %s",
                            fieldName,
                            clazz.getName(),
                            e.getMessage()),
                    e);
        } 
    }

    /**
     * get a field instance on a class {@link Field}
     * @param field the field
     * @param instance the instance if <code>field</code> is not static, <code>null</code> otherwise.
     * @return the field instance
     * @throws NotForUserException if an error occurs
     */
    public static Object getField(final Field field, final Object instance)
            throws NotForUserException {
        try {
            return field.get(instance);
        } catch (IllegalAccessException e) {
            throw new NotForUserException(
                    String.format(
                            "The field %s is inacessible on class %s: %s",
                            field.getName(),
                            field.getDeclaringClass().getName(),
                            e.getMessage()),
                    e);
        } catch (IllegalArgumentException e) {
            throw new NotForUserException(
                    String.format(
                            "The field %s is inacessible on class %s. It is probably the wrong object: %s",
                            field.getName(), field.getDeclaringClass().getName(),
                            e.getMessage()),
                    e);
        } catch (NullPointerException e) {
            throw new NotForUserException(
                    String.format(
                            "The field %s is inacessible on class %s. It probably need an instance: %s",
                            field.getName(),
                            field.getDeclaringClass().getName(),
                            e.getMessage()),
                    e);
        }
    }
    
    public static Object newInstance(Class<?> clazz) throws NotForUserException {
        try {
            return clazz.newInstance();
        } catch (IllegalAccessException e) {
            throw new NotForUserException(
                    String.format(
                            "the class or its nullary constructor is not accessible: %s",
                            e.getMessage()),
                    e);
        } catch (InstantiationException e) {
            throw new NotForUserException(
                    String.format(
                            "this Class represents an abstract class, an interface, an array class, a primitive type, or void; or if the class has no nullary constructor; or if the instantiation fails for some other reason.: %s",
                            e.getMessage()),
                    e);
        } catch (ExceptionInInitializerError e) {
            throw new NotForUserException(
                    String.format(
                            "the initialization provoked by this method fails: %s",
                            e.getMessage()),
                    e);
        }
    }

    public static String getFieldNameForGetter(final String methodName) {
        for (final String prefix: GETTER_METHOD_PREFIXES) {
            if (methodName.startsWith(prefix) && !methodName.equals(prefix)) {
                String res = methodName.substring(prefix.length());
                return Character.toLowerCase(res.charAt(0)) + res.substring(1) ;
            }
        }

        return null;
    }

    public static List<String> findYME() {
        final List<String> res = new ArrayList<>();
        ClassLoader c = Thread.currentThread().getContextClassLoader();

        while (c != null) {
            if (c instanceof URLClassLoader) {
                final URLClassLoader uc = (URLClassLoader)c;

                for (URL url: uc.getURLs()) {
                    res.add(url.toString());
                }
            }

            c = c.getParent();
        }

        return res;
    }
}
