package me.yoram.commons.j2se.nio.charset;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;

import me.yoram.commons.j2se.exceptions.BusinessException;
import me.yoram.commons.j2se.exceptions.NotForUserException;

/**
 * Utility class to wrap {@link Charset}. Methods in this class must throw {@link BusinessException}
 * 
 * @author Yoram Halberstam
 */
public final class WtCharset {
    private final Charset charset;

    /**
     * Instantiate the WtCharset
     * @param charset the internal charset
     */
    public WtCharset(final Charset charset) {
        super();

        this.charset = charset;
    }

    /**
     * Same as {@link Charset#forName(String)} but returns a WtCharset
     * @param charsetName the charset name
     * @return An instance of WtCharset that wraps the java 
     * @throws NotForUserException if an error occurs
     */
    public static WtCharset forName(final String charsetName) throws NotForUserException {
        try {
            Charset charset = Charset.forName(charsetName);
            return new WtCharset(charset);
        } catch (IllegalCharsetNameException e) {
            throw new NotForUserException(
                    String.format("The charset named [%s] is illegal.", charsetName), e);
        } catch (UnsupportedCharsetException e) {
            throw new NotForUserException(
                    "The named charset [%s] is not supported in this instance of the Java virtual machine",
                    e);
        } catch (IllegalArgumentException e) {
            throw new NotForUserException("The charset named is null.", e);
        }
    }

    /**
     * Calls {@link Charset#newDecoder()} on the internal charset and returns it wrapped up in a WtCharsetEncoder
     * @return  a new {@link CharsetEncoder} wrapped up in a WtCharsetEncoder
     * @throws NotForUserException if an error occurs
     */
    public WtCharsetEncoder newEncoder() throws NotForUserException {
        try {
            CharsetEncoder encoder = charset.newEncoder();

            return new WtCharsetEncoder(encoder);
        } catch (UnsupportedOperationException e) {
            throw new NotForUserException(
                    String.format("Charset [%s] does not support encoding", charset.name()),
                    e);
        }
    }
}
