package me.yoram.commons.j2se.utils.hostname;

import java.util.HashMap;
import java.util.Map;

public class ExpandKeywordsRegistry implements IExpandKeywordsEvent {
	private static final Map<String, IExpandKeywordsEvent> REGISTRY = new HashMap<>();
	private static final IExpandKeywordsEvent INSTANCE = new ExpandKeywordsRegistry();
	
	public static void register(final String key, final IExpandKeywordsEvent event) {
		REGISTRY.put(key, event);
	}
	
	public static IExpandKeywordsEvent getDefaultInstance() {
		return INSTANCE;
	}

    public static String expandKeywords(String data, final IExpandKeywordsEvent event) {
    	int pos = data.indexOf("${");
    	
    	while (pos != -1) {
    		pos = pos + 2;
    		int pos2 = data.indexOf("}", pos);
    		
    		if (pos2 == -1) {
    			break;
    		}
    		
    		String part = data.substring(pos, pos2);
    		String[] elem  = part.split(":");
    		String key = elem[0].trim();
    		
    		if (key.length() > 0) {
        		String value = elem.length > 1 ? elem[1].trim() : "";
        		
        		String expanded = event.onKeyword(key, value);
        		data = data.replace("${" + part + "}", expanded); 
    		}
    		
    		pos = data.indexOf("${", pos + 1);
    	}
    	
    	return data;
    }

    @Override
	public String onKeyword(String key, String value) {
		if (REGISTRY.containsKey(key)) {
			return REGISTRY.get(key).onKeyword(key, value);
		} else {
			return "[UNDEFINED KEYWORD " + key + "]";
		} 
	}
	
	
	
	static {
		register("LAN-IP", new LanIpExpandKeywordsEvent());
	}
}
