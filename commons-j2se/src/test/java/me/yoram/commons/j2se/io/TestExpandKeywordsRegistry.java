package me.yoram.commons.j2se.io;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import me.yoram.commons.j2se.utils.hostname.ExpandKeywordsRegistry;
import me.yoram.commons.j2se.utils.hostname.IExpandKeywordsEvent;

public class TestExpandKeywordsRegistry {
    @DataProvider
    public Object[][] dpExpandKeywords() {
    	return new Object[][] {
    		{"aa-${aab:ggt}-ee", "aa-ggt-ee"},
    		{"${aab:ggt}-ee", "ggt-ee"},
    		{"aa-${aab:ggt}", "aa-ggt"},
    		{"aa-${:ggt}-ee", "aa-${:ggt}-ee"},
    		{"aa-${aab:}-ee", "aa--ee"},
    	};
    }
    		
    @Test(dataProvider = "dpExpandKeywords")
    public void testExpandKeywords(String input, String expected) {
    	String res = ExpandKeywordsRegistry.expandKeywords(input, new IExpandKeywordsEvent() {
			@Override
			public String onKeyword(String key, String value) {
				if (key.equals("aab") && value.equals("ggt")) {
					return value;
				} else {
					return "";
				}
			}
		});
    	
    	assert res.equals(expected) : String.format("[%s] returned but [%s] expected", res, expected);
    }

}
