package me.yoram.commons.j2se.utils;

import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.KeyPair;
import java.util.UUID;

/**
 * Created by yoram on 03/06/15.
 */
public class TestCipherUtils {
    @Test
    public void testCipherDecipher() throws Exception {
        String text = UUID.randomUUID().toString();
        String key = CipherUtils.generateAES128Key();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        CipherUtils.symmetricCipher(key, "AES", new ByteArrayInputStream(text.getBytes()), baos);

        assert !new String(baos.toByteArray()).equals(text) : "Cipher failed";

        byte[] data = baos.toByteArray();
        baos = new ByteArrayOutputStream();
        CipherUtils.symmetricDecipher(key, "AES", new ByteArrayInputStream(data), baos);

        assert new String(baos.toByteArray()).equals(text) : "Decipher failed";
    }

    @Test
    public void testCipherDecipher2() throws Exception {
        String text = UUID.randomUUID().toString();
        String key = CipherUtils.generateAES128Key();

        String ciphered = CipherUtils.symmetricCipher(key, "AES", new ByteArrayInputStream(text.getBytes()));

        assert !ciphered.equals(text) : "Cipher failed";

        String deciphered = CipherUtils.symmetricDecipher(key, "AES", ciphered);

        assert deciphered.equals(text) : "Decipher failed";
    }

    @Test
    public void testSign() {
        String data = "1234567";

        KeyPair keyPair = CipherUtils.generateKeyPair();
        String sig = CipherUtils.sign(data.getBytes(), keyPair.getPrivate());
        assert CipherUtils.verifySignature(data.getBytes(), keyPair.getPublic(), sig) : "Signature should  verify";
        assert !CipherUtils.verifySignature("1234568".getBytes(), keyPair.getPublic(), sig) :
                "Signature should not verify";
    }
}
