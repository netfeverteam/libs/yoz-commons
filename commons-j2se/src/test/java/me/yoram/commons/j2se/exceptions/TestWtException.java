package me.yoram.commons.j2se.exceptions;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestWtException {
    @DataProvider
    public Object[][] dpCreate() {
        String userMessage = "message";
        String message = "log message";
        Throwable t = new Throwable();
        
        return new Object[][] {
                {new BusinessException(userMessage), userMessage, userMessage, null},
                {new BusinessException(message, userMessage), message, userMessage, null},
                {new BusinessException(message, t), message, message, t},
                {new BusinessException(message, userMessage, t), message, userMessage, t}
        };
    }
    
    @Test(dataProvider = "dpCreate")
    public void testCreate(BusinessException e, String message, String userMessage, Throwable cause) {
        assert e.getMessage().equals(message) : String.format("[%s] expected in message property but [%s] returned", message, e.getMessage());
        assert e.getUserMessage().equals(userMessage) : String.format("[%s] expected in userMessage property but [%s] returned", userMessage, e.getUserMessage());
        
        if (cause == null) {
            assert e.getCause() == null : "The cause property should be null";
        } else { 
            assert e.getCause() != null : "The cause property should not be null";
        }
        
        assert  e.getCause() == cause : "The cause property differs from expected";
    }
}
