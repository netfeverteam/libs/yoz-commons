package me.yoram.commons.j2se.utils;

import me.yoram.commons.j2se.io.IOUtilities;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.activation.DataSource;
import java.io.*;
import java.net.URL;
import java.util.Collection;
import java.util.zip.ZipEntry;

/**
 * Created by yoram on 30/05/16.
 */
public class TestZipUtils {
    @Test
    public void testUnzip() throws Exception {
        URL url = TestZipUtils.class.getResource("/zip/tests");
        File root = new File(url.toURI());
        String[] zipFilenames = root.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                File f = new File(dir, name);
                return f.isFile() && f.getName().endsWith(".zip");
            }
        });

        for (String zipFilename: zipFilenames) {
            File tmp = ZipUtils.unzip(new File(root, zipFilename));

            try {
                File resourceFolder = new File(root, zipFilename.substring(0, zipFilename.length() - 4));

                FileSystemUtils.FolderCompareResult res = FileSystemUtils.compareFolder(
                        resourceFolder.getAbsolutePath(), tmp.getAbsolutePath());

                assert res.getNewFiles().isEmpty(): "new files: " + res.getNewFiles().toString();
                assert res.getDifferentFiles().isEmpty(): "changed files: " + res.getNewFiles().toString();
                assert res.getDeletedFiles().isEmpty(): "deleted files: " + res.getNewFiles().toString();
            } finally {
                FileSystemUtils.deleteFolder(tmp);
            }

        }

        //File folder = ZipUtils.unzip("/home/yoram/dev/git/NetfeverTeam/Commons/yoz-commons/commons-j2se/src/test/resources/zip/tests/test-01.zip");
        //System.out.println(folder);
        //FileSystemUtils.compareFolder("/home/yoram/tmp/test-zip", "/home/yoram/tmp/test-zip");
    }

    @Test
    public void testZip() throws Exception {
        URL url = TestZipUtils.class.getResource("/zip/tests/test-01/a");

        File tempZip = ZipUtils.zipFolder(new File(url.toURI()));

        try {
            File tempDir = ZipUtils.unzip(tempZip);

            try {
                FileSystemUtils.FolderCompareResult res = FileSystemUtils.compareFolder(new File(url.toURI()), tempDir);

                assert !res.isDifferent();
            } finally {
                FileSystemUtils.deleteFolder(tempDir);
            }
        } finally {
            tempZip.delete();
        }
    }

    @DataProvider
    public Object[][] dpCopy() {
        return new Object[][] {
                {
                    "/zip/tests2/test-01.zip",
                    "/zip/tests2/test-01-a",
                    new ZipUtils.OnCopyEvent() {
                        @Override
                        public boolean copy(ZipEntry zip, InputStream in, OutputStream out) {
                            return !zip.getName().equals("a/01.file");
                        }
                    }
                },
                {
                    "/zip/tests2/test-01.zip",
                    "/zip/tests2/test-01-b",
                    new ZipUtils.OnCopyEvent() {
                        @Override
                        public boolean copy(ZipEntry zip, InputStream in, OutputStream out) throws IOException {
                            if (zip.getName().equals("01.file")) {
                                out.write("this is a change".getBytes());
                            }

                            return true;
                        }
                    }
                }
        };
    }

    @Test(dataProvider = "dpCopy")
    public void testCopy(String zipResource, String compFolderResource, ZipUtils.OnCopyEvent event) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try (
                InputStream in = TestZipUtils.class.getResourceAsStream(zipResource);
        ) {
            ZipUtils.copy(in, baos, event);
        }

        File f = ZipUtils.unzip(new ByteArrayInputStream(baos.toByteArray()));

        try {
            FileSystemUtils.FolderCompareResult res = FileSystemUtils.compareFolder(
                    new File(TestZipUtils.class.getResource(compFolderResource).toURI()),
                    f);

            assert !res.isDifferent();
        } finally {
            FileSystemUtils.deleteFolder(f);
        }
    }

    @Test
    public void testSources() throws Exception {
        final byte[] data1 = "hello world".getBytes();
        DataSource ds1 = new DataSource() {
            @Override
            public InputStream getInputStream()  {
                return new ByteArrayInputStream(data1);
            }

            @Override
            public OutputStream getOutputStream() throws IOException {
                return null;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public String getName() {
                return "name1.txt";
            }
        };

        final byte[] data2 = "goodbye world".getBytes();
        DataSource ds2 = new DataSource() {
            @Override
            public InputStream getInputStream() throws IOException {
                return new ByteArrayInputStream(data2);
            }

            @Override
            public OutputStream getOutputStream() throws IOException {
                return null;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public String getName() {
                return "name2.txt";
            }
        };

        final ByteArrayOutputStream zipOut = new ByteArrayOutputStream();
        ZipUtils.zipSources(zipOut, ds1, ds2);
        Collection<DataSource> data = ZipUtils.unzipSources(new ByteArrayInputStream(zipOut.toByteArray()));
        assert data.size() == 2 : data.toString();
        DataSource o = data.stream()
                .filter(dataSource -> "name1.txt".equals(dataSource.getName()))
                .findFirst()
                .orElse(null);
        assert o != null : String.format("Data source name1.txt not found: %s", data);
        assert "hello world".equals(new String(IOUtilities.inputStreamToByteArray(o.getInputStream()))) :
                String.format("Data source name1.txt do not contains expected text: %s", data);

        o = data.stream()
                .filter(dataSource -> "name2.txt".equals(dataSource.getName()))
                .findFirst()
                .orElse(null);
        assert o != null : String.format("Data source name2.txt not found: %s", data);
        assert "goodbye world".equals(new String(IOUtilities.inputStreamToByteArray(o.getInputStream()))) :
                String.format("Data source name2.txt do not contains expected text: %s", data);
    }
}
