package me.yoram.commons.j2se.utils;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;

/**
 * Created by yoram on 27/07/16.
 */
public class TestIterables {
    @DataProvider
    public Object[][] dpToString() {
        return new Object[][] {
                {new TreeSet<>(Arrays.asList("a", "c", "b")), ",", "a,b,c"},
                {new TreeSet<>(Arrays.asList("a", " ", "b")), ",", " ,a,b"},
                {new TreeSet<>(Arrays.asList("a", "", "b")), ",", "a,b"},
                {new ArrayList<>(Arrays.asList("a", "c", "b")), ",", "a,c,b"},
                {new ArrayList<>(Arrays.asList("a", " ", "b")), ",", "a, ,b"},
                {new ArrayList<>(Arrays.asList("a", "", "b")), ",", "a,,b"}
        };
    }

    @Test(dataProvider = "dpToString")
    public void testToString(
            Collection<String> collection, String separator, String expectedResult) {
        String res = Iteratables.toString(collection, separator);

        assert res.equals(expectedResult) : String.format("Expected result [%s] but [%s] returned", expectedResult, res);
    }

    @DataProvider
    public Object[][] dpSplit() {
        return new Object[][] {
                {"a,b,c", ",", false, new HashSet<>(Arrays.asList("a","b","c"))},
                {"a, ,c", ",", false, new HashSet<>(Arrays.asList("a","c"))},
                {"a,,,,,c,d", ",", false, new HashSet<>(Arrays.asList("a","c", "d"))},
                {"a,b,c", ",", true, new HashSet<>(Arrays.asList("a","b","c"))},
                {"a, ,c", ",", true, new HashSet<>(Arrays.asList("a","c", ""))},
                {"a,,,,,c,d", ",", true, new HashSet<>(Arrays.asList("a","c", "d", ""))}
        };
    }

    @Test(dataProvider = "dpSplit")
    public void testSplit(
            String s, String separator, boolean allowblank, Set<String> expectedResult) {
        Set<String> res = new HashSet<>();
        Iteratables.split(s, separator, allowblank, res);

        Set<String> diff = Sets.intersection(expectedResult, res);

        assert diff.isEmpty() : String.format("Diff [%s] returned", expectedResult, res);
    }
}
