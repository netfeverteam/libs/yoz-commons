package me.yoram.commons.j2se.io;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import org.testng.annotations.Test;

import me.yoram.commons.j2se.exceptions.BusinessException;

public class TestWtBufferedReader {
    private static class ReaderException extends Reader {
        @Override
        public int read(char[] cbuf, int off, int len) throws IOException {
            throw new IOException("aaa");
        }

        @Override
        public void close() throws IOException {
            throw new IOException("aaa");
        }        
    }

    @Test
    public void testReadLineWithClose() throws Exception {
        String s = "aaa\nbbb";
        WtBufferedReader reader = new WtBufferedReader(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(s.getBytes()))));
        assert reader.readLine().equals("aaa") : "should be aaa at this point";
        assert reader.readLineQuietly().equals("bbb") : "should be bbb at this point";
        reader.close();
    }
    
    @Test
    public void testReadLineWithCloseQuietly() throws Exception {
        String s = "aaa\nbbb";
        WtBufferedReader reader = new WtBufferedReader(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(s.getBytes()))));
        assert reader.readLine().equals("aaa") : "should be aaa at this point";
        assert reader.readLineQuietly().equals("bbb") : "should be bbb at this point";
        reader.closeQuietly();
    }
    
    @Test(expectedExceptions = {BusinessException.class})
    public void testReadLineThrowsException() throws Exception {
        WtBufferedReader reader = new WtBufferedReader(new BufferedReader(new ReaderException()));
        reader.readLine();
    }

    @Test
    public void testReadLineQuietlyThrowsException() throws Exception {
        WtBufferedReader reader = new WtBufferedReader(new BufferedReader(new ReaderException()));
        assert reader.readLineQuietly() == null;
    }

    @Test(expectedExceptions = {BusinessException.class})
    public void testCloseThrowsException() throws Exception {
        WtBufferedReader reader = new WtBufferedReader(new BufferedReader(new ReaderException()));
        reader.close();
    }

    @Test
    public void testCloseQuietlyThrowsException() throws Exception {
        WtBufferedReader reader = new WtBufferedReader(new BufferedReader(new ReaderException()));
        reader.closeQuietly();
    }
}
