package me.yoram.commons.j2se.io;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.lang3.SystemUtils;
import org.testng.SkipException;
import org.testng.annotations.Test;

import me.yoram.commons.j2se.exceptions.BusinessException;

public class TestFileUtilities {
    @Test
    public void testNewFileInputStream() throws Exception {
        File f = File.createTempFile("TestFileUtilities", ".tmp");

        try {
            InputStream in = FileUtilities.newFileInputStream(f
                    .getAbsolutePath());

            try {

            } finally {
                in.close();
            }
        } finally {
            f.delete();
        }
    }

    @Test(expectedExceptions = { BusinessException.class })
    public void testNewFileInputStreamFailsNotExists() throws Exception {
        FileUtilities.newFileInputStream("filenotexist");
    }

    @Test(expectedExceptions = { BusinessException.class })
    public void testNewFileInputStreamFailsIsFolder() throws Exception {
        FileUtilities.newFileInputStream(File.listRoots()[0].getAbsolutePath());
    }
    
    @Test(expectedExceptions = {BusinessException.class})
    public void testNewFileInputStreamFileNotFoundException() throws Exception {
        if (SystemUtils.IS_OS_LINUX) {
            FileUtilities.newFileInputStream("/dev/cpu/microcode");            
        } else if(SystemUtils.IS_OS_WINDOWS) {
        	throw new SkipException("TODO");
        } else {
            throw new Exception("add some code for inaccessible file on window :)");
        }
    }

    @Test
    public void testNewFileOutputStream() throws Exception {
        File f = File.createTempFile("TestFileUtilities", ".tmp");

        try {
            OutputStream out = FileUtilities.newFileOutputStream(f
                    .getAbsolutePath());

            try {

            } finally {
                out.close();
            }
        } finally {
            f.delete();
        }
    }

    @Test(expectedExceptions = { BusinessException.class })
    public void testNewFileOutputStreamFailsIsFolder() throws Exception {
        FileUtilities.newFileInputStream(File.listRoots()[0].getAbsolutePath());
    }
    
    @Test(expectedExceptions = {BusinessException.class})
    public void testNewFileOutputStreamFileNotFoundException() throws Exception {
        if (SystemUtils.IS_OS_LINUX) {
            FileUtilities.newFileOutputStream("/dev/cpu/microcode");
        } else if(SystemUtils.IS_OS_WINDOWS) {
        	throw new SkipException("TODO");
        } else {
            throw new Exception("add some code for inaccessible file on window :)");
        }
    }
}
