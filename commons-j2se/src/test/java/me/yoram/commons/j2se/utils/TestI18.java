/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2se.utils;

import org.testng.annotations.Test;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 21/09/18
 */
@Test(singleThreaded = true)
public class TestI18 {
    @Test
    public void testWithNoResourceBundle() {
        I18.setDefaultMessageBundleBaseName(null);

        String res = I18.getMessage("ID1", "Default Message");

        assert "Default Message".equals(res) : String.format("\"Default Message\" expected but \"%s\" returned", res);
    }

    @Test
    public void testWithDefaultMessageBundleBaseName() {
        I18.setDefaultMessageBundleBaseName("bundles.b1");

        String res = I18.getMessage("ID1", "Default Message");
        assert "Hello World".equals(res) : String.format("\"Hello World\" expected but \"%s\" returned", res);

        res = I18.getMessage(null, "ID1", "Default Message", "kr");
        assert "Hello World".equals(res) : String.format("\"Hello World\" expected but \"%s\" returned", res);

        res = I18.getMessage(null, "ID1", "Default Message", "fr");
        assert "Bonjour Monde".equals(res) : String.format("\"Bonjour Monde\" expected but \"%s\" returned", res);
    }

    @Test
    public void testWithBaseName() {
        I18.setDefaultMessageBundleBaseName("bundles.notexists");

        String res = I18.getMessage("bundles.b1", "ID1", "Default Message", null);
        assert "Hello World".equals(res) : String.format("\"Hello World\" expected but \"%s\" returned", res);

        res = I18.getMessage("bundles.b1", "ID1", "Default Message", "kr");
        assert "Hello World".equals(res) : String.format("\"Hello World\" expected but \"%s\" returned", res);

        res = I18.getMessage("bundles.b1", "ID1", "Default Message", "fr");
        assert "Bonjour Monde".equals(res) : String.format("\"Bonjour Monde\" expected but \"%s\" returned", res);
    }
}
