package me.yoram.commons.j2se.preferences.api;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.BackingStoreException;

import org.apache.commons.lang3.SystemUtils;
import org.testng.SkipException;
import org.testng.annotations.Test;

import me.yoram.commons.j2se.io.SystemUtilities;
import me.yoram.commons.j2se.preferences.impl.FilePreferencesFactory;
import me.yoram.commons.j2se.utils.hostname.Environment;

@Test(singleThreaded = true)
public class TestPreferencesUtils {
    @Test
    public void testGetSystemValue() throws Exception {
        String s = PreferencesUtils.getSystemValue("user.dir");
        assert s != null : "user.dir is missing";

        // TODO - Why it doesn't work on Windows
        if(SystemUtils.IS_OS_WINDOWS) {
        	throw new SkipException("TODO WT-23");
        }
        
        SystemUtilities.setEnvironmentVariable("testGetSystemValue", "t");
        s = PreferencesUtils.getSystemValue("testGetSystemValue");
        assert s != null : "testGetSystemValue is missing";

        s = PreferencesUtils.getSystemValue("testGetSystemValue123");
        assert s == null : "testGetSystemValue123 should not exists";
    }
    
    @Test
    public void testGetFilesPath() throws Exception {
        String oldEnvPath = System.getProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH);
        
        try {
            if (oldEnvPath != null) {
                System.getProperties().remove(FilePreferencesFactory.ENV_PROP_PREFS_PATH);
            }
        
            File f = PreferencesUtils.getFilesPath();
            assert f.getAbsolutePath().equals(SystemUtils.getUserDir().getAbsolutePath()) : "should have returned user.dir";
            
            String path = File.listRoots()[0].getAbsolutePath();
            System.setProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH, path);
            f = PreferencesUtils.getFilesPath();
            assert f.getAbsolutePath().equals(path) : "should have returned user.dir";            
        } finally {
            if (oldEnvPath != null) {
                System.setProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH, oldEnvPath);
            }            
        }        
    }

    @Test(expectedExceptions = {BackingStoreException.class})
    public void testGetFilesPathNotExists() throws Exception {
        String oldEnvPath = System.getProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH);
        
        try {        
            String path = "dsadsafsagdsgdsd";
            System.setProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH, path);
            PreferencesUtils.getFilesPath();
        } finally {
            if (oldEnvPath != null) {
                System.setProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH, oldEnvPath);
            }            
        }        
    }

    @Test(expectedExceptions = {BackingStoreException.class})
    public void testGetFilesNotDirectory() throws Exception {
        String oldEnvPath = System.getProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH);
        
        try {        
            String path = new File(TestPreferencesUtils.class.getResource("/img/01.png").toURI()).getAbsolutePath();
            System.setProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH, path);
            PreferencesUtils.getFilesPath();
        } finally {
            if (oldEnvPath != null) {
                System.setProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH, oldEnvPath);
            }            
        }        
    }
    
    @Test(expectedExceptions = {BackingStoreException.class})
    public void testGetFilesIsNull() throws Exception {
        String oldEnvPath = System.getProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH);
        String oldUserDir = System.getProperty("user.dir");
        try {        
            System.getProperties().remove(FilePreferencesFactory.ENV_PROP_PREFS_PATH);
            System.getProperties().remove("user.dir");
            PreferencesUtils.getFilesPath();
        } finally {
            System.setProperty("user.dir", oldUserDir);
            if (oldEnvPath != null) {
                System.setProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH, oldEnvPath);
            }            
        }                
    }
    
    @Test
    public void testGetPreferencesFiles() throws Exception {
        File root = new File(TestPreferencesUtils.class.getResource("/conf").toURI());
        List<File> list = new ArrayList<>();
        
        String oldHostname = System.getProperty(Environment.SYSTEM_PROP_HOSTNAME_OVERRIDE);
        
        try {
            System.setProperty(Environment.SYSTEM_PROP_HOSTNAME_OVERRIDE, "host");
            PreferencesUtils.getPreferencesFiles(root, list);
            
            assert list.size() == 4 : String.format("the list should have 4 items: %s", list);
            File[] files = new File[] {
                    new File(root, "system.localhost.xml"),
                    new File(root, "user.localhost.xml"),
                    new File(root, "host/system.host.xml"),
                    new File(root, "host/user.host.xml")
            };
            
            for (File f: files) {
                assert list.contains(f) : String.format("File [%s] not found in %s", f.getAbsoluteFile(), list);
            }
        } finally {
            if (oldHostname != null) {
                System.setProperty(Environment.SYSTEM_PROP_HOSTNAME_OVERRIDE, oldHostname);
            }
        }
    }
}
