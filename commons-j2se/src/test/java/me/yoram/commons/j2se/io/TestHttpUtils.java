package me.yoram.commons.j2se.io;

import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class TestHttpUtils {
	public static void main(String[] args) throws Exception {
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					ServerSocket server = new ServerSocket(3333);
					Socket socket = server.accept();
					
					InputStream in = socket.getInputStream();
					
					int b;
					while (true) {
						b = in.read();
						System.out.print((char)b);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		t.setDaemon(false);
		t.start();

		//Thread.sleep(1000);
		
		//HttpUtils.post("http://127.0.0.1:3333/fff/ssss", null, new String[][] {{"cookie", "ds"}}, null);
	}
}
