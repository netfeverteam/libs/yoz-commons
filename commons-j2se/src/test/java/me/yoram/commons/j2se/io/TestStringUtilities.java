package me.yoram.commons.j2se.io;

import org.testng.annotations.Test;

import me.yoram.commons.j2se.exceptions.BusinessException;

public class TestStringUtilities {
    @Test
    public void testNewString() throws Exception {
        byte[] b = "0123456789".getBytes();
        
        StringUtilities.newString(b, "UTF-8");
    }

    @Test(expectedExceptions = {BusinessException.class})
    public void testNewStringFails() throws Exception {
        byte[] b = "0123456789".getBytes();
        
        StringUtilities.newString(b, "NOTENCODING");
    }    
}
