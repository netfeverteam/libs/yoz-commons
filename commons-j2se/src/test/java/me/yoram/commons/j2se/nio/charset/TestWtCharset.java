package me.yoram.commons.j2se.nio.charset;

import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

import org.testng.annotations.Test;

import me.yoram.commons.j2se.exceptions.BusinessException;

public class TestWtCharset {
    @Test
    public void testForName() throws Exception {
        WtCharset.forName("UTF-8");
    }

    @Test(expectedExceptions = {BusinessException.class})
    public void testForNameFails() throws Exception {
        WtCharset.forName("NOTENCODING");
    }

    @Test(expectedExceptions = {BusinessException.class})
    public void testForNameFailsNull() throws Exception {
        WtCharset.forName(null);
    }
    
    @Test
    public void testNewEncoder() throws Exception {
        WtCharset ch = WtCharset.forName("UTF-8");
        ch.newEncoder();
    }

    @Test(expectedExceptions = {BusinessException.class})
    public void testNewEncoderFails() throws Exception {
        WtCharset ch = new WtCharset(new Charset("TESTCH", new String[0]) {
            @Override
            public CharsetEncoder newEncoder() {
                throw new UnsupportedOperationException();
            }
            
            @Override
            public CharsetDecoder newDecoder() {
                throw new UnsupportedOperationException();
            }
            
            @Override
            public boolean contains(Charset cs) {
                throw new UnsupportedOperationException();
            }
        });
        
        ch.newEncoder();
    }
}
