package me.yoram.commons.j2se.preferences.impl;

import java.util.prefs.PreferencesFactory;

import org.testng.annotations.Test;

public class TestMultiPreferencesFactory {
    @Test
    public void testFactoryGetSet() {
        PreferencesFactory factory = new MemoryPreferencesFactory();

        MultiPreferencesFactory.setFactory(factory);

        PreferencesFactory factory2 = MultiPreferencesFactory.getFactory();

        assert factory == factory2 : "Factory mismatch between getter and setter";
    }

    @Test
    public void testFactorySetNull() {
        MultiPreferencesFactory.setFactory(null);

        assert (MultiPreferencesFactory.getFactory() instanceof MemoryPreferencesFactory) : String
                .format("Factory must be of class '%s'",
                        MemoryPreferencesFactory.class.getName());
    }
}
