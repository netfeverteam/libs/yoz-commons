 package me.yoram.commons.j2se.utils.hostname;

import org.testng.annotations.Test;

public class TestEnvironment {
	@Test
	public void testEnvironmentHostname() {
		System.clearProperty("com.wt.hostname.override");
		String original = Environment.getHostname();
		
		System.setProperty("com.wt.hostname.override", "test");
		String hostname = Environment.getHostname();
		
		assert hostname.equals("test") : String.format("[test] expected but hotname is [%s]", hostname);
		
		System.getProperties().remove("com.wt.hostname.override");
		hostname = Environment.getHostname();
		assert original.equals(hostname) : String.format("[%s] expected but hostname is [%s]", original, hostname);
	}
	
	@Test
	public void testGetLanIp() {
		System.out.println(Environment.getIp("wlan0"));
	}
}
