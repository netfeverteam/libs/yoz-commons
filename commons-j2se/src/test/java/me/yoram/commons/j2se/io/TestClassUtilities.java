package me.yoram.commons.j2se.io;

import java.lang.reflect.Field;

import org.testng.annotations.Test;

import me.yoram.commons.j2se.exceptions.BusinessException;

public class TestClassUtilities {
    public static class DummyInitError {
        static {
            Integer.parseInt("eeedsa");
        }
    }

    public static class DummyWithField {
        private static String staticPrv ="1234";
        private String prv = "abcd";
        
        public static String getStaticPrv() {
            return staticPrv;
        }
        
        public static void setStaticPrv(String staticPrv) {
            DummyWithField.staticPrv = staticPrv;
        }
        
        public String getPrv() {
            return prv;
        }
        
        public void setPrv(String prv) {
            this.prv = prv;
        }
    }

    @Test
    public void testForName() throws Exception {
        ClassUtilities.forName("java.lang.String");
    }

    @Test(expectedExceptions = {BusinessException.class})
    public void testForNameClassNotFound() throws Exception {
        ClassUtilities.forName("java.lang.NoClass");
    }

    @Test(expectedExceptions = {BusinessException.class})
    public void testForNameInitializationError() throws Exception {
        ClassUtilities.forName(DummyInitError.class.getName());
    }

    @Test
    public void testGetDeclaredField() throws Exception {
        ClassUtilities.getDeclaredField(DummyWithField.class, "prv");
        ClassUtilities.getDeclaredField(DummyWithField.class, "staticPrv");
    }

    @Test(expectedExceptions = {BusinessException.class})
    public void testGetDeclaredFieldNoSuchField() throws Exception {
        ClassUtilities.getDeclaredField(DummyWithField.class, "sdsds");
    }

    @Test(expectedExceptions = {BusinessException.class})
    public void testGetDeclaredFieldNullPointer() throws Exception {
        ClassUtilities.getDeclaredField(DummyWithField.class, null);
    }

    @Test
    public void testGetField() throws Exception {
        Field field = ClassUtilities.getDeclaredField(DummyWithField.class, "staticPrv");
        field.setAccessible(true);
        String s = (String)ClassUtilities.getField(field, null);
        assert s.equals("1234") : String.format("1234 expected but string is %s", s);

        field = ClassUtilities.getDeclaredField(DummyWithField.class, "prv");
        field.setAccessible(true);
        DummyWithField o = new DummyWithField(); 
        s = (String)ClassUtilities.getField(field, o);
        assert s.equals("abcd") : String.format("abcd expected but string is %s", s);
    }

    @Test(expectedExceptions = {BusinessException.class})
    public void testGetFieldIllegalAccess() throws Exception {
        Field field = ClassUtilities.getDeclaredField(DummyWithField.class, "staticPrv");
        ClassUtilities.getField(field, null);
    }

    @Test(expectedExceptions = {BusinessException.class})
    public void testGetFieldNullPointer() throws Exception {
        Field field = ClassUtilities.getDeclaredField(DummyWithField.class, "prv");
        field.setAccessible(true);
        ClassUtilities.getField(field, null);
    }

    @Test(expectedExceptions = {BusinessException.class})
    public void testGetFieldIllegalArgument() throws Exception {
        Field field = ClassUtilities.getDeclaredField(DummyWithField.class, "prv");
        field.setAccessible(true);
        ClassUtilities.getField(field, new Object());
    }
}
