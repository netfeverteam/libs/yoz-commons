package me.yoram.commons.j2se.preferences.impl;

import java.io.File;
import java.net.URISyntaxException;
import java.util.UUID;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

import org.apache.commons.lang3.SystemUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import me.yoram.commons.j2se.utils.hostname.Environment;

@Test(singleThreaded = true)
public class TestPreferencesFactories {
    // MemoryPreferencesFactory
    private Preferences fillTree(boolean isSystemRoot, String nodeName) {
        Preferences res = getRightNode(isSystemRoot, nodeName);

        res.put("test", "value");

        res = getRightNode(isSystemRoot, nodeName);

        return res;
    }

    private Preferences getRightNode(boolean isSystemRoot, String nodeName) {
        Preferences res;

        if (isSystemRoot && !SystemUtils.IS_OS_LINUX) {
            res = Preferences.systemRoot().node(nodeName);
        } else {
            res = Preferences.userRoot().node(nodeName);
        }

        return res;
    }

    @DataProvider
    public Object[][] dpReadAndWrite() {
        return new Object[][] { { true, new MemoryPreferencesFactory() },
                { false, new MemoryPreferencesFactory() },
                { true, new FilePreferencesFactory() },
                { false, new FilePreferencesFactory() } };
    }

    @Test(dataProvider = "dpReadAndWrite")
    public void testPreferencesAddAndRead(boolean isSystemRoot,
            PreferencesFactory factory) {
        PreferencesFactory savedFactory = MultiPreferencesFactory.getFactory();

        try {
            MultiPreferencesFactory.setFactory(factory);
            String nodeName = "this/is/a/test/testPreferencesAddAndRead";

            Preferences pref = fillTree(isSystemRoot, nodeName);

            String s = pref.get("test", null);

            assert s != null : String.format(
                    "Preference test at node '%s' not found", nodeName);
            assert s.equals("value") : String
                    .format("Preference test at node '%s' should be value but is '%s' instead",
                            nodeName, s);
        } finally {
            MultiPreferencesFactory.setFactory(savedFactory);
        }

    }

    @Test(dataProvider = "dpReadAndWrite", expectedExceptions = { IllegalStateException.class })
    public void testPreferencesAddAndRemoveNode(boolean isSystemRoot,
            PreferencesFactory factory) throws Exception {
        PreferencesFactory savedFactory = MultiPreferencesFactory.getFactory();

        String oldPath = null;
        String oldHost = null;
        
        if (factory instanceof FilePreferencesFactory) {
            File f = new File(TestPreferencesFactories.class.getResource("/conf").toURI());
            oldPath = System.getProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH);
            oldHost = System.getProperty(Environment.SYSTEM_PROP_HOSTNAME_OVERRIDE);
            System.setProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH, f.getAbsolutePath());
            System.setProperty(Environment.SYSTEM_PROP_HOSTNAME_OVERRIDE, "host");
        }

        try {            
            MultiPreferencesFactory.setFactory(factory);
            String nodeName = "this/is/a/test/testPreferencesAddAndRemoveNode";

            Preferences pref = fillTree(isSystemRoot, nodeName);
            pref.removeNode();
            pref.flush();
            pref.get("test", null);
        } finally {
            if (oldPath != null) {
                System.setProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH, oldPath);
            }
            
            if (oldHost != null) {
                System.setProperty(Environment.SYSTEM_PROP_HOSTNAME_OVERRIDE, oldHost);
            }
            
            MultiPreferencesFactory.setFactory(savedFactory);
        }
    }

    @Test(dataProvider = "dpReadAndWrite")
    public void testPreferencesAddAndRemoveValue(boolean isSystemRoot,
            PreferencesFactory factory) throws BackingStoreException {
        PreferencesFactory savedFactory = MultiPreferencesFactory.getFactory();

        try {
            MultiPreferencesFactory.setFactory(factory);
            String nodeName = "this/is/a/test/testPreferencesAddAndRemoveValue";

            Preferences pref = fillTree(isSystemRoot, nodeName);

            pref.remove("test");
            String s = pref.get("test", null);
            assert s == null : "Preference test should have been removed at that point";
        } finally {
            MultiPreferencesFactory.setFactory(savedFactory);
        }
    }

    @Test(dataProvider = "dpReadAndWrite")
    public void testListKeys(boolean isSystemRoot, PreferencesFactory factory)
            throws BackingStoreException {
        PreferencesFactory savedFactory = MultiPreferencesFactory.getFactory();

        try {
            MultiPreferencesFactory.setFactory(factory);
            String nodeName = "this/is/a/test/testListKeys";

            Preferences pref = fillTree(isSystemRoot, nodeName);

            String[] keys = pref.keys();
            assert keys.length == 1 : "There should only be one key in this node.";
            assert keys[0].equals("test") : "the key should be named test";
        } finally {
            MultiPreferencesFactory.setFactory(savedFactory);
        }
    }

    @Test(dataProvider = "dpReadAndWrite")
    public void testListNodes(boolean isSystemRoot, PreferencesFactory factory)
            throws BackingStoreException {
        PreferencesFactory savedFactory = MultiPreferencesFactory.getFactory();

        try {
            MultiPreferencesFactory.setFactory(factory);
            String rootNodeName = String.format("this/is/a/test/%s", UUID
                    .randomUUID().toString());
            String nodeName = rootNodeName + "/testListKeys";

            Preferences pref = fillTree(isSystemRoot, nodeName);
            pref = getRightNode(isSystemRoot, rootNodeName);

            String[] keys = pref.childrenNames();
            assert keys.length == 1 : "There should only be one key in this node.";
            assert keys[0].equals("testListKeys") : "the key should be named testListKeys";
        } finally {
            MultiPreferencesFactory.setFactory(savedFactory);
        }
    }

    /**
     * Not sure. why it doesnt work from main maven
     * 
     * @throws BackingStoreException
     * @throws URISyntaxException
     */
    @Test(enabled = true)
    public void testLoadFiles() throws URISyntaxException {
        PreferencesFactory savedFactory = MultiPreferencesFactory.getFactory();

        String oldPath = null;
        String oldHost = null;
        
        File f = new File(TestPreferencesFactories.class.getResource("/conf").toURI());
        oldPath = System.getProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH);
        oldHost = System.getProperty(Environment.SYSTEM_PROP_HOSTNAME_OVERRIDE);
        System.setProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH, f.getAbsolutePath());
        System.setProperty(Environment.SYSTEM_PROP_HOSTNAME_OVERRIDE, "host");

        try {
            MultiPreferencesFactory.setFactory(new FilePreferencesFactory());

            Preferences pref;
            String s;
            if (!SystemUtils.IS_OS_LINUX) {
                pref = Preferences.systemRoot().node(
                        "com/wt/preferences");
                s = pref.get("STRING", null);
                assert s != null : String.format(
                        "the STRING value should not be null but is %s instead", s);
                assert s.equals("SYSTEM_CUSTOM_VALUE") : "the STRING value should be SYSTEM_CUSTOM_VALUE but is "
                        + s + " instead.";

                s = pref.get("INT", null);
                assert s != null : "the INTvalue should not be null";
                assert s.equals("SYSTEM_2013") : "the INT value should be SYSTEM_2013 but is "
                        + s + " instead.";
            }

            pref = Preferences.userRoot().node("com/wt/preferences");

            s = pref.get("STRING", null);
            assert s != null : "the STRING value should not be null";
            assert s.equals("USER_DEFAULT_VALUE") : "the STRING value should be USER_DEFAULT_VALUE but is "
                    + s + " instead.";

            s = pref.get("INT", null);
            assert s != null : "the INT value should not be null";
            assert s.equals("CUSTOM_2013") : "the INT  value should be CUSTOM_2013 but is "
                    + s + " instead.";
        } finally {
            if (oldPath != null) {
                System.setProperty(FilePreferencesFactory.ENV_PROP_PREFS_PATH, oldPath);
            }
            
            if (oldHost != null) {
                System.setProperty(Environment.SYSTEM_PROP_HOSTNAME_OVERRIDE, oldHost);
            }

            MultiPreferencesFactory.setFactory(savedFactory);
        }
    }
}
