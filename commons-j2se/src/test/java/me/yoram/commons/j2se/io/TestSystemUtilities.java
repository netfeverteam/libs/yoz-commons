package me.yoram.commons.j2se.io;

import java.security.SecureRandom;

import org.apache.commons.lang3.SystemUtils;
import org.testng.SkipException;
import org.testng.annotations.Test;

public class TestSystemUtilities {    
    @Test
    public void testSetEnvironmentVariable() throws Exception {
    	if(SystemUtils.IS_OS_WINDOWS) {
    		throw new SkipException("TODO WT-24");
    	}
        SecureRandom rnd = new SecureRandom();
        String key = "TEST" + rnd.nextInt();
        
        String value = System.getenv(key);
        assert value == null : "The key should be null to start with";

        SystemUtilities.setEnvironmentVariable(key, "1234");
        value = System.getenv(key);
        assert value.equals("1234") : String.format("The value should be 1234, it is %s instead", value);
    }
    
}
