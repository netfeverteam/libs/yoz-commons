package me.yoram.commons.j2se.utils.hostname;

import java.net.InetAddress;

import org.apache.commons.lang3.SystemUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import me.yoram.commons.j2se.io.SystemUtilities;

@Test(singleThreaded = true)
public class TestHostnameProviders {
    @DataProvider
    public Object[][] dpHostname() {
        IHostnameProvider windows;
        IHostnameProvider linux;

        if (SystemUtils.IS_OS_LINUX) {
            windows = new IHostnameProvider() {
                @Override
                public String getHostname() {
                    try {
                        SystemUtilities.setEnvironmentVariable("COMPUTERNAME", new JavaHostnameProvider().getHostname());
                        
                        return new WindowsHostnameProvider().getHostname();
                    } catch (Exception e) {
                        return e.getMessage();
                    }
                }
            };

            linux = new LinuxHostnameProvider();
        } else {
            windows = new WindowsHostnameProvider();

            linux = new IHostnameProvider() {
                @Override
                public String getHostname() {
                    return new JavaHostnameProvider().getHostname();
                }
            };
        }

        return new Object[][] { { windows, linux } };
    }

    @Test(dataProvider = "dpHostname")
    public void testHostname(IHostnameProvider windows, IHostnameProvider linux)
            throws Exception {
        String hostname = InetAddress.getLocalHost().getHostName();

        String s = new JavaHostnameProvider().getHostname();
        assert hostname.equalsIgnoreCase(s) : String.format(
                "Hostname is expected to be [%s] but was [%s]", hostname, s);

        s = new LinuxHostnameProvider().getHostname();

        assert hostname.trim().equalsIgnoreCase(s.trim()) : String.format(
                "Hostname is expected to be [%s] but was [%s]", hostname, s);

        s = windows.getHostname();//new WindowsHostnameProvider().getHostname();

        assert hostname.equalsIgnoreCase(s) : String.format(
                "Hostname is expected to be [%s] but was [%s]", hostname, s);

        IHostnameProvider prov = new HostnameOverrideProvider();
        s = prov.getHostname();
        assert s == null : String.format("null expected but [%s] returned", s);
        System.setProperty("com.wt.hostname.override", "test");
        s = prov.getHostname();
        assert s.equals("test") : String.format(
                "[test] expected but [%s] returned", s);
    }
}
