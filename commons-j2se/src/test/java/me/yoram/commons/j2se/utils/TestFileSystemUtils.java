package me.yoram.commons.j2se.utils;

import org.apache.commons.io.IOUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by yoram on 30/05/16.
 */
public class TestFileSystemUtils {
    @Test
    public void testGetRelativePathMap() throws Exception {
        URL url = TestFileSystemUtils.class.getResource("/zip/tests/test-01/01.file");
        File root = new File(url.toURI()).getParentFile();

        Path path = Paths.get(root.getAbsolutePath());
        FileTreeVisitor visitor = new FileTreeVisitor();
        Files.walkFileTree(path, visitor);

        Map<String, File> map = FileSystemUtils.getRelativePathMap(root.getAbsolutePath(), visitor.getFileTree());

        Set<String> expectedSet = new HashSet<>(Arrays.asList("/01.file", "/a/01.file", "/a/02.file"));
        Set<String> diff = Sets.intersection(expectedSet, map.keySet());

        assert diff.isEmpty() : String.format("Returned %s", diff.toString());
    }

    @Test
    public void testCreateTemporaryFolder() throws Exception {
        File f = FileSystemUtils.createTemporaryFolder("TestFileSystemUtils", "dir");

        assert f.isDirectory(): String.format("%s is not a folder");

        FileSystemUtils.deleteFolder(f.getAbsolutePath());
    }

    @DataProvider
    public Object[][] dpSameFile() {
        return new Object[][] {
                {"/zip/tests/test-01/01.file", "/zip/tests/test-01/01.file", true},
                {"/zip/tests/test-01/01.file", "/zip/tests/test-01/a/01.file", false}
        };
    }

    @Test(dataProvider = "dpSameFile")
    public void testSameFile(String filename1, String filename2, boolean expectedResult) throws Exception {
        URL url1 = TestFileSystemUtils.class.getResource(filename1);
        File f1 = new File(url1.toURI());

        URL url2 = TestFileSystemUtils.class.getResource(filename2);
        File f2 = new File(url2.toURI());

        boolean res = FileSystemUtils.sameFile(f1.getAbsolutePath(), f2.getAbsolutePath());

        assert res == expectedResult;
    }

    @Test(dataProvider = "dpSameFile")
    public void testEquals(String filename1, String filename2, boolean expectedResult) throws Exception {
        URL url1 = TestFileSystemUtils.class.getResource(filename1);
        byte[] b1 = IOUtils.toByteArray(url1);

        URL url2 = TestFileSystemUtils.class.getResource(filename2);
        byte[] b2 = IOUtils.toByteArray(url2);

        boolean res = FileSystemUtils.equals(b1, b2);

        assert res == expectedResult;
    }

    @Test
    public void testDeleteFolder() throws Exception {
        File root = FileSystemUtils.createTemporaryFolder(TestFileSystemUtils.class.getSimpleName(), ".dir");

        try {
            OutputStream out = new FileOutputStream(new File(root, "a.txt"));
            out.write("hello world".getBytes());
            out.close();

            new File(root, "a").mkdir();

            out = new FileOutputStream(new File(root, "a/a.txt"));
            out.write("hello world 2".getBytes());
            out.close();
        } finally {
            FileSystemUtils.deleteFolder(root);
        }
    }

    @DataProvider
    public Object[][] dpCompareFolder() {
        return new Object[][] {
                {"/compare/folder1/01", "/compare/folder1/02", new HashSet<>(), new HashSet<>(), new HashSet<>()},
                {
                        "/compare/folder1/01",
                        "/compare/folder2",
                        new HashSet<>(Arrays.asList("/a/03.file")),
                        new HashSet<>(Arrays.asList("/a/01.file")),
                        new HashSet<>(Arrays.asList("/a/02.file"))}
        };
    }

    @Test(dataProvider = "dpCompareFolder")
    public void testCompareFolder(
            String folder1,
            String folder2,
            Set<String> expectedNewFiles,
            Set<String> expectedDifferentFiles,
            Set<String> expectedDeletedFiles) throws Exception {
        URL url = TestFileSystemUtils.class.getResource(folder1);
        File root1 = new File(url.toURI());

        url = TestFileSystemUtils.class.getResource(folder2);
        File root2 = new File(url.toURI());

        FileSystemUtils.FolderCompareResult res = FileSystemUtils.compareFolder(
                root1.getAbsolutePath(), root2.getAbsolutePath());

        assert expectedNewFiles.size() == res.getNewFiles().size(): String.format("size differs : " + expectedNewFiles);
        expectedNewFiles.removeAll(res.getNewFiles());
        assert expectedNewFiles.isEmpty() : String.format("expected new file not empty: " + expectedNewFiles);

        assert expectedDeletedFiles.size() == res.getDeletedFiles().size(): String.format("size differs : " + expectedDeletedFiles);
        expectedDeletedFiles.removeAll(res.getDeletedFiles());
        assert expectedDeletedFiles.isEmpty() : String.format("expected deleted file not empty: " + expectedDeletedFiles);

        assert expectedDifferentFiles.size() == res.getDifferentFiles().size(): String.format("size differs : " + expectedDifferentFiles);
        expectedDifferentFiles.removeAll(res.getDifferentFiles());
        assert expectedDeletedFiles.isEmpty() : String.format("expected deleted file not empty: " + expectedDifferentFiles);
    }
}
